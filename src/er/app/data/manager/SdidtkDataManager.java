/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import er.app.config.AppConfig;
import er.app.config.DatabaseConfig;
import er.app.data.model.Sdidtk;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

/**
 *
 * @author DERI
 */
public class SdidtkDataManager {
    
    private static SdidtkDataManager sInstance;
    private final Dao<Sdidtk, Integer> sdidtkDao;
    private final ObservableList<Sdidtk> sdidtkList;
    
    private SdidtkDataManager() throws SQLException {
        sdidtkDao = DaoManager.createDao(
                DatabaseConfig.getConnection(), 
                Sdidtk.class
        );
        sdidtkList = FXCollections.observableArrayList();
    }
    
    public static void init() {
        try {
            sInstance = new SdidtkDataManager();
            sInstance.loadListSdidtk();
        } catch (SQLException ex) {
            sInstance = null;
            System.err.println("Error while creating Maternal DAO: "+ex);
            DialogUtils.showError("Gagal menyambungkan ke tabel maternal.");
        }
    }    
    public static SdidtkDataManager getInstance() {
        if (sInstance == null) init();
        return sInstance;
    }
    
    public ObservableList<Sdidtk> getSdidtkList() {
        return sdidtkList;
    }    
    public ObservableList<Sdidtk> getSdidtkList(boolean refreshList) {
        if (refreshList) try {
            loadListSdidtk();
        } catch (SQLException ex) {
            System.err.println("Error while reading all Maternal: "+ex);
            DialogUtils.showError("Gagal membaca data dari tabel maternal.");
        }
        return sdidtkList;
    }
    
    public FilteredList<Sdidtk> filterSdidtkList(String query) {
        FilteredList<Sdidtk> filteredList = new FilteredList<>(sdidtkList, p -> true);
        filteredList.setPredicate(sdidtk -> {
            // If filter text is empty, display all persons.
            if (query == null || query.trim().isEmpty()) {
                return true;
            }

            // Compare first name and last name of every person with filter text.
            String lowerCaseFilter = query.toLowerCase();

            if (sdidtk.getKelurahan().toLowerCase().contains(lowerCaseFilter)) {
                return true; 
            } else if (sdidtk.getNomorRM().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches last name.
            } else if (sdidtk.getNamaAnak().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches last name.
            }
            return false; // Does not match.
        });
        return filteredList;
    }
    
    public int addSdidtk(Sdidtk sdidtk) {
        try {
            sdidtkDao.create(sdidtk);
            sdidtkList.add(sdidtk);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while insert Maternal with no rm ("+sdidtk.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int deleteSdidtk(Sdidtk sdidtk) {
        try {
            sdidtkDao.delete(sdidtk);
            sdidtkList.remove(sdidtk);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while delete Maternal with no rm ("+sdidtk.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int updateSdidtk(Sdidtk sdidtk) {
        try {
            sdidtkDao.update(sdidtk);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while update Maternal with no rm ("+sdidtk.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int[][] processSdidtkReport(
            int[][] data, 
            String date,
            int usiaMin,
            int usiaMax
    ) {
        for (Sdidtk s : sdidtkList) {            
            if (!s.getTanggal().contains(date)) continue;
            
            int usia = s.getUsiaAnak();
            if (usia >= usiaMin && usia <= usiaMax) {
                switch (s.getKelurahan()) {
                    case "Bunulrejo":                        
                        data = countSdidtkReport(s, 0, data);
                        break;
                    case "Kesatrian":
                        data = countSdidtkReport(s, 1, data);
                        break;
                    case "Jodipan":
                        data = countSdidtkReport(s, 2, data);
                        break;
                    case "Polehan":
                        data = countSdidtkReport(s, 3, data);
                        break;
                    default:
                        break;
                }
            }
        }        
        
        data = calculateSdidtkKunjunganReport(data, date, usiaMin, usiaMax);
        
        return data;
    }
    
    private int[][] countSdidtkReport(Sdidtk s, int index, int[][] data) {
        String jk = s.getJenisKelamin();
        
        if (s.getGangguanLka().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][4]++;
            else if (jk.equals("Perempuan")) data[index][5]++;
        }
        if (s.getMotorikKasar().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][6]++;
            else if (jk.equals("Perempuan")) data[index][7]++;
        }
        if (s.getMotorikHalus().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][8]++;
            else if (jk.equals("Perempuan")) data[index][9]++;
        }
        if (s.getBicaraBahasa().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][10]++;
            else if (jk.equals("Perempuan")) data[index][11]++;
        }
        if (s.getSosialisasi().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][12]++;
            else if (jk.equals("Perempuan")) data[index][13]++;
        }
        if (s.getGangguanTdl().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][14]++;
            else if (jk.equals("Perempuan")) data[index][15]++;
        }
        if (s.getGangguanTdd().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][16]++;
            else if (jk.equals("Perempuan")) data[index][17]++;
        }
        if (s.getMme().equals("Tidak Normal")) {
            if (jk.equals("Laki - laki")) data[index][18]++;
            else if (jk.equals("Perempuan")) data[index][19]++;
        }
        if (s.getPerluDirujuk().equals("Ya")) {
            if (jk.equals("Laki - laki")) data[index][20]++;
            else if (jk.equals("Perempuan")) data[index][21]++;
        }
        
        return data;
    }
    private int[][] calculateSdidtkKunjunganReport(
            int[][] data, 
            String date,
            int usiaMin,
            int usiaMax
    ) {
        // calculate jumlah kunjungan pasien before tanggal laporan
        HashMap<String, Integer> mapJmlLB = new HashMap<>(),
                mapJmlPB = new HashMap<>(),
                mapJmlLK = new HashMap<>(),
                mapJmlPK = new HashMap<>(),
                mapJmlLJ = new HashMap<>(),
                mapJmlPJ = new HashMap<>(),
                mapJmlLP = new HashMap<>(),
                mapJmlPP = new HashMap<>();
        String fullDate = (date.length() == 4? 
                date+"12-31" : date+"-28");
        LocalDate tglLaporan = DateTimeUtils.parseDateOnly(fullDate);
        for (Sdidtk s : sdidtkList) {
            LocalDate tglKunjungan = DateTimeUtils.parseDateOnly(s.getTanggal());
            if (tglKunjungan.isAfter(tglLaporan)) continue;
             
            String kelurahan = s.getKelurahan(), 
                    jk = s.getJenisKelamin(),
                    noRM = s.getNomorRM();
            int usia = s.getUsiaAnak();
            if (usia >= usiaMin && usia <= usiaMax) {
                switch (kelurahan) {
                    case "Bunulrejo":                        
                        if (jk.equals("Laki - laki")) {
                            if (mapJmlLB.containsKey(noRM)) {
                                // update
                                int value = mapJmlLB.get(noRM);
                                value++;
                                mapJmlLB.replace(noRM, value);
                            } else mapJmlLB.put(noRM, 1);
                        }
                        else if (jk.equals("Perempuan")) {
                            if (mapJmlPB.containsKey(noRM)) {
                                // update
                                int value = mapJmlPB.get(noRM);
                                value++;
                                mapJmlPB.replace(noRM, value);
                            } else mapJmlPB.put(noRM, 1);
                        }
                        break;
                    case "Kesatrian":
                        if (jk.equals("Laki - laki")) {
                            if (mapJmlLK.containsKey(noRM)) {
                                // update
                                int value = mapJmlLK.get(noRM);
                                value++;
                                mapJmlLK.replace(noRM, value);
                            } else mapJmlLK.put(noRM, 1);
                        }
                        else if (jk.equals("Perempuan")) {
                            if (mapJmlPK.containsKey(noRM)) {
                                // update
                                int value = mapJmlPK.get(noRM);
                                value++;
                                mapJmlPK.replace(noRM, value);
                            } else mapJmlPK.put(noRM, 1);
                        }
                        break;
                    case "Jodipan":
                        if (jk.equals("Laki - laki")) {
                            if (mapJmlLJ.containsKey(noRM)) {
                                // update
                                int value = mapJmlLJ.get(noRM);
                                value++;
                                mapJmlLJ.replace(noRM, value);
                            } else mapJmlLJ.put(noRM, 1);
                        }
                        else if (jk.equals("Perempuan")) {
                            if (mapJmlPJ.containsKey(noRM)) {
                                // update
                                int value = mapJmlPJ.get(noRM);
                                value++;
                                mapJmlPJ.replace(noRM, value);
                            } else mapJmlPJ.put(noRM, 1);
                        }
                        break;
                    case "Polehan":
                        if (jk.equals("Laki - laki")) {
                            if (mapJmlLP.containsKey(noRM)) {
                                // update
                                int value = mapJmlLP.get(noRM);
                                value++;
                                mapJmlLP.replace(noRM, value);
                            } else mapJmlLP.put(noRM, 1);
                        }
                        else if (jk.equals("Perempuan")) {
                            if (mapJmlPP.containsKey(noRM)) {
                                // update
                                int value = mapJmlPP.get(noRM);
                                value++;
                                mapJmlPP.replace(noRM, value);
                            } else mapJmlPP.put(noRM, 1);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        
        // Bunulrejo
        for (String noRM : mapJmlLB.keySet()) {
            int jml = mapJmlLB.get(noRM);
            if (jml == 1) data[0][0]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[0][2]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[0][2]++;
                }
            }
        }
        for (String noRM : mapJmlPB.keySet()) {
            int jml = mapJmlPB.get(noRM);
            if (jml == 1) data[0][1]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[0][3]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[0][3]++;
                }
            }
        }
        
        // Kesatrian
        for (String noRM : mapJmlLK.keySet()) {
            int jml = mapJmlLK.get(noRM);
            if (jml == 1) data[1][0]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[1][2]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[1][2]++;
                }
            }
        }
        for (String noRM : mapJmlPK.keySet()) {
            int jml = mapJmlPK.get(noRM);
            if (jml == 1) data[1][1]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[1][3]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[1][3]++;
                }
            }
        }
        
        // Jodipan
        for (String noRM : mapJmlLJ.keySet()) {
            int jml = mapJmlLJ.get(noRM);
            if (jml == 1) data[2][0]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[2][2]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[2][2]++;
                }
            }
        }
        for (String noRM : mapJmlPJ.keySet()) {
            int jml = mapJmlPJ.get(noRM);
            if (jml == 1) data[2][1]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[2][3]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[2][3]++;
                }
            }
        }
        
        // Polehan
        for (String noRM : mapJmlLP.keySet()) {
            int jml = mapJmlLP.get(noRM);
            if (jml == 1) data[3][0]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[3][2]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[3][2]++;
                }
            }
        }
        for (String noRM : mapJmlPP.keySet()) {
            int jml = mapJmlPP.get(noRM);
            if (jml == 1) data[3][1]++;
            else {
                if (usiaMin == 0) {
                    // bayi
                    if (jml >= 4) data[3][3]++;
                } else if (usiaMin == 13) {
                    // balita
                    if (jml >= 2) data[3][3]++;
                }
            }
        }
        
        return data;
    }
    
    private void loadListSdidtk() throws SQLException {
        sdidtkList.clear();
        for (Sdidtk s : sdidtkDao.queryForAll()) {
            sdidtkList.add(s);
        }
    }
}
