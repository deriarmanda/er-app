/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import er.app.config.AppConfig;
import er.app.config.DatabaseConfig;
import er.app.data.model.Pasien;
import er.app.util.DialogUtils;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author DERI
 */
public class PasienDataManager {
    
    private static PasienDataManager sInstance;
    private final Dao<Pasien, Integer> pasienDao;
    private final ObservableList<Pasien> pasienList;
    
    private PasienDataManager() throws SQLException {
        pasienDao = DaoManager.createDao(
                DatabaseConfig.getConnection(), 
                Pasien.class
        );
        pasienList = FXCollections.observableArrayList();
    }
    
    public static void init() {
        try {
            sInstance = new PasienDataManager();
            sInstance.loadListPasien();
        } catch (SQLException ex) {
            sInstance = null;
            System.err.println("Error while creating Pasien DAO: "+ex);
            DialogUtils.showError("Gagal menyambungkan ke tabel pasien.");
        }
    }
    
    public static PasienDataManager getInstance() {
        if (sInstance == null) init();
        return sInstance;
    }

    public ObservableList<Pasien> getPasienList() {
        return pasienList;
    }

    public ObservableList<Pasien> getPasienList(boolean refreshList) {
        if (refreshList) try {
            loadListPasien();
        } catch (SQLException ex) {
            System.err.println("Error while reading all Pasien: "+ex);
            DialogUtils.showError("Gagal membaca data dari tabel pasien.");
        }
        return pasienList;
    }
    
    public int addPasien(Pasien pasien) {
        try {
            pasienDao.create(pasien);
            pasienList.add(pasien);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while insert Pasien with no rm ("+pasien.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int deletePasien(Pasien pasien) {
        try {
            pasienDao.delete(pasien);
            pasienList.remove(pasien);
            MaternalDataManager.getInstance().getMaternalList(true);
            PersalinanDataManager.getInstance().getPersalinanList(true);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while delete Pasien with no rm ("+pasien.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int updatePasien(Pasien pasien) {
        try {
            pasienDao.update(pasien);
            MaternalDataManager.getInstance().getMaternalList(true);
            PersalinanDataManager.getInstance().getPersalinanList(true);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while update Pasien with no rm ("+pasien.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public Pasien searchPasienByNomorRM(String nomorRM) {
        nomorRM = nomorRM.toLowerCase();
        for (Pasien p : pasienList) {
            if (p.getNomorRM().toLowerCase().contains(nomorRM)) return p;
        }
        return null;
    }
    
    public Pasien searchPasienByNamaLengkap(String namaLengkap) {
        namaLengkap = namaLengkap.toLowerCase();
        for (Pasien p : pasienList) {
            if (p.getNamaLengkap().toLowerCase().contains(namaLengkap)) return p;
        }
        return null;
    }
    
    private void loadListPasien () throws SQLException {
        pasienList.clear();
        for (Pasien p : pasienDao.queryForAll()) {
            pasienList.add(p);
        }
    }
}
