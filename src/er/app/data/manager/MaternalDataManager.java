/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import er.app.config.AppConfig;
import er.app.config.DatabaseConfig;
import er.app.data.model.Maternal;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

/**
 *
 * @author DERI
 */
public class MaternalDataManager {
    
    private static MaternalDataManager sInstance;
    private final Dao<Maternal, Integer> maternalDao;
    private final ObservableList<Maternal> maternalList;
    
    private MaternalDataManager() throws SQLException {
        maternalDao = DaoManager.createDao(
                DatabaseConfig.getConnection(), 
                Maternal.class
        );
        maternalList = FXCollections.observableArrayList();
    }
    
    public static void init() {
        try {
            sInstance = new MaternalDataManager();
            sInstance.loadListMaternal();
        } catch (SQLException ex) {
            sInstance = null;
            System.err.println("Error while creating Maternal DAO: "+ex);
            DialogUtils.showError("Gagal menyambungkan ke tabel maternal.");
        }
    }
    
    public static MaternalDataManager getInstance() {
        if (sInstance == null) init();
        return sInstance;
    }
    
    public ObservableList<Maternal> getMaternalList() {
        return maternalList;
    }    
    public ObservableList<Maternal> getMaternalList(boolean refreshList) {
        if (refreshList) try {
            loadListMaternal();
        } catch (SQLException ex) {
            System.err.println("Error while reading all Maternal: "+ex);
            DialogUtils.showError("Gagal membaca data dari tabel maternal.");
        }
        return maternalList;
    }
    
    public FilteredList<Maternal> filterMaternalList(String query) {
        FilteredList<Maternal> filteredList = new FilteredList<>(maternalList, p -> true);
        filteredList.setPredicate(maternal -> {
            // If filter text is empty, display all persons.
            if (query == null || query.trim().isEmpty()) {
                return true;
            }

            // Compare first name and last name of every person with filter text.
            String lowerCaseFilter = query.toLowerCase();

            if (maternal.getKelurahan().toLowerCase().contains(lowerCaseFilter)) {
                return true; 
            } else if (maternal.getNomorRM().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches last name.
            } else if (maternal.getNamaPasien().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches last name.
            }
            return false; // Does not match.
        });
        return filteredList;
    }
    
    public int addMaternal(Maternal maternal) {
        try {
            maternalDao.create(maternal);
            maternalList.add(maternal);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while insert Maternal with no rm ("+maternal.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int deleteMaternal(Maternal maternal) {
        try {
            maternalDao.delete(maternal);
            maternalList.remove(maternal);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while delete Maternal with no rm ("+maternal.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int updateMaternal(Maternal maternal) {
        try {
            maternalDao.update(maternal);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while update Maternal with no rm ("+maternal.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int[][] processMonthlyReport(int[][] data, String date) {
        for (Maternal m : maternalList) {
            if (!m.getTanggalKunjungan().contains(date)) continue;
            
            String[] maternals = new String[] {
                "Hiperemesis", 
                "Abortus", 
                "Pre-eklampsia / Eklampsia", 
                "Perdarahan Kehamilan", 
                "Perdarahan Persalinan", 
                "Perdarahan Nifas", 
                "Partus Lama", 
                "Infeksi", 
                "AIDS", 
                "TB", 
                "Malaria", 
                "Kasus Lain"
            };
            String kelurahan = m.getKelurahan(), alb = m.getAlbumin(),
                    kasus = m.getMaternal();
            double hb = m.getHemoglobin(), lila = m.getLingkarLengan(),
                    red = m.getReduksi();
                    
            switch (kelurahan) {
                case "Bunulrejo":
                    // count total
                    data[0][0]++; data[0][3]++; data[0][5]++;
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[0][1]++;
                    else if (hb < 8) data[0][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[0][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[0][6]++; data[0][7]++;
                        if (red > 140) data[0][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[0][i+9]++;
                    }
                    break;
                case "Kesatrian":
                    // count total
                    data[1][0]++; data[1][3]++; data[1][5]++; 
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[1][1]++;
                    else if (hb < 8) data[1][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[1][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[1][6]++; data[1][7]++;
                        if (red > 140) data[1][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[1][i+9]++;
                    }
                    break;
                case "Jodipan":
                    // count total
                    data[2][0]++; data[2][3]++; data[2][5]++;
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[2][1]++;
                    else if (hb < 8) data[2][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[2][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[2][6]++; data[2][7]++;
                        if (red > 140) data[2][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[2][i+9]++;
                    }
                    break;
                case "Polehan":
                    // count total
                    data[3][0]++; data[3][3]++; data[3][5]++;
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[3][1]++;
                    else if (hb < 8) data[3][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[3][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[3][6]++; data[3][7]++;
                        if (red > 140) data[3][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[3][i+9]++;
                    }
                    break;
                default:
                    break;
            }
        }
        return data;
    }
    
    public int[][][] processAnnualReport(int[][][] data, String date) {
        for (Maternal m : maternalList) {
            if (!m.getTanggalKunjungan().contains(date)) continue;
            
            String[] maternals = new String[] {
                "Hiperemesis", 
                "Abortus", 
                "Pre-eklampsia / Eklampsia", 
                "Perdarahan Kehamilan", 
                "Perdarahan Persalinan", 
                "Perdarahan Nifas", 
                "Partus Lama", 
                "Infeksi", 
                "AIDS", 
                "TB", 
                "Malaria", 
                "Kasus Lain"
            };
            String kelurahan = m.getKelurahan(), alb = m.getAlbumin(),
                    kasus = m.getMaternal();
            double hb = m.getHemoglobin(), lila = m.getLingkarLengan(),
                    red = m.getReduksi();
            int month = DateTimeUtils.getIndexOfMonth(m.getTanggalKunjungan());
                    
            switch (kelurahan) {
                case "Bunulrejo":
                    // count total
                    data[0][month][0]++; data[0][month][3]++; data[0][month][5]++;
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[0][month][1]++;
                    else if (hb < 8) data[0][month][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[0][month][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[0][month][6]++; data[0][month][7]++;
                        if (red > 140) data[0][month][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[0][month][i+9]++;
                    }
                    break;
                case "Kesatrian":
                    // count total
                    data[1][month][0]++; data[1][month][3]++; data[1][month][5]++; 
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[1][month][1]++;
                    else if (hb < 8) data[1][month][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[1][month][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[1][month][6]++; data[1][month][7]++;
                        if (red > 140) data[1][month][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[1][month][i+9]++;
                    }
                    break;
                case "Jodipan":
                    // count total
                    data[2][month][0]++; data[2][month][3]++; data[2][month][5]++;
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[2][month][1]++;
                    else if (hb < 8) data[2][month][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[2][month][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[2][month][6]++; data[2][month][7]++;
                        if (red > 140) data[2][month][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[2][month][i+9]++;
                    }
                    break;
                case "Polehan":
                    // count total
                    data[3][month][0]++; data[3][month][3]++; data[3][month][5]++;
                    
                    // Hemoglobin
                    if (hb >= 8 && hb <= 11) data[3][month][1]++;
                    else if (hb < 8) data[3][month][2]++;
                    
                    // Lingkar lengan
                    if (lila < 23) data[3][month][4]++;
                    
                    // Albumin & Reduksi
                    if (alb.equals("Positif")) {
                        data[3][month][6]++; data[3][month][7]++;
                        if (red > 140) data[3][month][8]++;
                    }
                    
                    // Maternal
                    for (int i=0; i<maternals.length; i++) {
                        if (kasus.contains(maternals[i])) data[3][month][i+9]++;
                    }
                    break;
                default:
//                    // count total
//                    data[4][month][0]++; data[4][month][3]++; data[4][month][5]++;
//                    
//                    // Hemoglobin
//                    if (hb >= 8 && hb <= 11) data[4][month][1]++;
//                    else if (hb < 8) data[4][month][2]++;
//                    
//                    // Lingkar lengan
//                    if (lila < 23) data[4][month][4]++;
//                    
//                    // Albumin & Reduksi
//                    if (alb.equals("Positif")) {
//                        data[4][month][6]++; data[4][month][7]++;
//                        if (red > 140) data[4][month][8]++;
//                    }
//                    
//                    // Maternal
//                    for (int i=0; i<maternals.length; i++) {
//                        if (kasus.contains(maternals[i])) data[4][month][i+9]++;
//                    }
                    break;
            }
        }
        return data;
    }
    
    private void loadListMaternal () throws SQLException {
        maternalList.clear();
        for (Maternal p : maternalDao.queryForAll()) {
            maternalList.add(p);
        }
    }
}
