/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import er.app.config.AppConfig;
import er.app.config.DatabaseConfig;
import er.app.data.model.Persalinan;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

/**
 *
 * @author DERI
 */
public class PersalinanDataManager {
    private static PersalinanDataManager sInstance;
    private final Dao<Persalinan, Integer> persalinanDao;
    private final ObservableList<Persalinan> persalinanList;
    
    private PersalinanDataManager() throws SQLException {
        persalinanDao = DaoManager.createDao(
                DatabaseConfig.getConnection(), 
                Persalinan.class
        );
        persalinanList = FXCollections.observableArrayList();
    }
    
    public static void init() {
        try {
            sInstance = new PersalinanDataManager();
            sInstance.loadListPersalinan();
        } catch (SQLException ex) {
            sInstance = null;
            System.err.println("Error while creating Persalinan DAO: "+ex);
            DialogUtils.showError("Gagal menyambungkan ke tabel persalinan.");
        }
    }
    
    public static PersalinanDataManager getInstance() {
        if (sInstance == null) init();
        return sInstance;
    }

    public ObservableList<Persalinan> getPersalinanList() {
        return persalinanList;
    }

    public ObservableList<Persalinan> getPersalinanList(boolean refreshList) {
        if (refreshList) try {
            loadListPersalinan();
        } catch (SQLException ex) {
            System.err.println("Error while reading all Persalinan: "+ex);
            DialogUtils.showError("Gagal membaca data dari tabel persalinan.");
        }
        return persalinanList;
    }
    
    public FilteredList<Persalinan> filterPersalinanList(String query) {
        FilteredList<Persalinan> filteredList = new FilteredList<>(persalinanList, p -> true);
        filteredList.setPredicate(persalinan -> {
            // If filter text is empty, display all persons.
            if (query == null || query.trim().isEmpty()) {
                return true;
            }

            // Compare first name and last name of every person with filter text.
            String lowerCaseFilter = query.toLowerCase();

            if (persalinan.getKelurahan().toLowerCase().contains(lowerCaseFilter)) {
                return true; 
            } else if (persalinan.getNomorRM().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches last name.
            } else if (persalinan.getNamaPasien().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches last name.
            }
            return false; // Does not match.
        });
        return filteredList;
    }
    
    public int addPersalinan(Persalinan persalinan) {
        try {
            persalinanDao.create(persalinan);
            persalinanList.add(persalinan);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while insert Persalinan with no rm ("+persalinan.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int deletePersalinan(Persalinan persalinan) {
        try {
            persalinanDao.delete(persalinan);
            persalinanList.remove(persalinan);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while delete Persalinan with no rm ("+persalinan.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int updatePersalinan(Persalinan persalinan) {
        try {
            persalinanDao.update(persalinan);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while update Persalinan with no rm ("+persalinan.getNomorRM()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int[][] processMaternalMonthlyReport(int[][] data, String date) {
        for (Persalinan p : persalinanList) {
            if (!p.getTanggalPartus().contains(date)) continue;
                        
            String kelurahan = p.getKelurahan(), 
                    jenis = p.getJenisPartus(),
                    tindakan = p.getTindakan(),
                    jk = p.getJenisKelamin();
                    
            switch (kelurahan) {
                case "Bunulrejo":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[0][21]++;
                            else if (jk.equals("Perempuan")) data[0][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[0][23]++;
                            else if (jk.equals("Perempuan")) data[0][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[0][33]++;
                            else if (jk.equals("Perempuan")) data[0][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[0][25]++;
                            else if (jk.equals("Perempuan")) data[0][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[0][27]++;
                            else if (jk.equals("Perempuan")) data[0][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[0][29]++;
                            else if (jk.equals("Perempuan")) data[0][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[0][31]++;
                            else if (jk.equals("Perempuan")) data[0][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Kesatrian":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[1][21]++;
                            else if (jk.equals("Perempuan")) data[1][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[1][23]++;
                            else if (jk.equals("Perempuan")) data[1][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[1][33]++;
                            else if (jk.equals("Perempuan")) data[1][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[1][25]++;
                            else if (jk.equals("Perempuan")) data[1][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[1][27]++;
                            else if (jk.equals("Perempuan")) data[1][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[1][29]++;
                            else if (jk.equals("Perempuan")) data[1][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[1][31]++;
                            else if (jk.equals("Perempuan")) data[1][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Jodipan":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[2][21]++;
                            else if (jk.equals("Perempuan")) data[2][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[2][23]++;
                            else if (jk.equals("Perempuan")) data[2][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[2][33]++;
                            else if (jk.equals("Perempuan")) data[2][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[2][25]++;
                            else if (jk.equals("Perempuan")) data[2][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[2][27]++;
                            else if (jk.equals("Perempuan")) data[2][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[2][29]++;
                            else if (jk.equals("Perempuan")) data[2][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[2][31]++;
                            else if (jk.equals("Perempuan")) data[2][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Polehan":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[3][21]++;
                            else if (jk.equals("Perempuan")) data[3][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[3][23]++;
                            else if (jk.equals("Perempuan")) data[3][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[3][33]++;
                            else if (jk.equals("Perempuan")) data[3][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[3][25]++;
                            else if (jk.equals("Perempuan")) data[3][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[3][27]++;
                            else if (jk.equals("Perempuan")) data[3][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[3][29]++;
                            else if (jk.equals("Perempuan")) data[3][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[3][31]++;
                            else if (jk.equals("Perempuan")) data[3][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        return data;
    }
    
    public int[][][] processMaternalAnnualReport(int[][][] data, String date) {
        for (Persalinan p : persalinanList) {
            if (!p.getTanggalPartus().contains(date)) continue;
                        
            String kelurahan = p.getKelurahan(), 
                    jenis = p.getJenisPartus(),
                    tindakan = p.getTindakan(),
                    jk = p.getJenisKelamin();
            int month = DateTimeUtils.getIndexOfMonth(p.getTanggalPartus());
            
            switch (kelurahan) {
                case "Bunulrejo":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[0][month][21]++;
                            else if (jk.equals("Perempuan")) data[0][month][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[0][month][23]++;
                            else if (jk.equals("Perempuan")) data[0][month][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[0][month][33]++;
                            else if (jk.equals("Perempuan")) data[0][month][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[0][month][25]++;
                            else if (jk.equals("Perempuan")) data[0][month][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[0][month][27]++;
                            else if (jk.equals("Perempuan")) data[0][month][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[0][month][29]++;
                            else if (jk.equals("Perempuan")) data[0][month][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[0][month][31]++;
                            else if (jk.equals("Perempuan")) data[0][month][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Kesatrian":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[1][month][21]++;
                            else if (jk.equals("Perempuan")) data[1][month][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[1][month][23]++;
                            else if (jk.equals("Perempuan")) data[1][month][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[1][month][33]++;
                            else if (jk.equals("Perempuan")) data[1][month][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[1][month][25]++;
                            else if (jk.equals("Perempuan")) data[1][month][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[1][month][27]++;
                            else if (jk.equals("Perempuan")) data[1][month][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[1][month][29]++;
                            else if (jk.equals("Perempuan")) data[1][month][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[1][month][31]++;
                            else if (jk.equals("Perempuan")) data[1][month][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Jodipan":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[2][month][21]++;
                            else if (jk.equals("Perempuan")) data[2][month][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[2][month][23]++;
                            else if (jk.equals("Perempuan")) data[2][month][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[2][month][33]++;
                            else if (jk.equals("Perempuan")) data[2][month][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[2][month][25]++;
                            else if (jk.equals("Perempuan")) data[2][month][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[2][month][27]++;
                            else if (jk.equals("Perempuan")) data[2][month][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[2][month][29]++;
                            else if (jk.equals("Perempuan")) data[2][month][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[2][month][31]++;
                            else if (jk.equals("Perempuan")) data[2][month][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Polehan":
                    // Jenis Partus
                    switch (jenis) {
                        case "Partus Normal":
                            if (jk.equals("Laki - laki")) data[3][month][21]++;
                            else if (jk.equals("Perempuan")) data[3][month][22]++;
                            break;
                        case "Sectio Caesaria":
                            if (jk.equals("Laki - laki")) data[3][month][23]++;
                            else if (jk.equals("Perempuan")) data[3][month][24]++;
                            break;
                        case "Persalinan Dukun":
                            if (jk.equals("Laki - laki")) data[3][month][33]++;
                            else if (jk.equals("Perempuan")) data[3][month][34]++;
                            break;
                        default:
                            break;
                    }
                    
                    // Tindakan
                    switch (tindakan) {
                        case "Drip":
                            if (jk.equals("Laki - laki")) data[3][month][25]++;
                            else if (jk.equals("Perempuan")) data[3][month][26]++;
                            break;
                        case "Vakum / Forcep":
                            if (jk.equals("Laki - laki")) data[3][month][27]++;
                            else if (jk.equals("Perempuan")) data[3][month][28]++;
                            break;
                        case "Curetage":
                            if (jk.equals("Laki - laki")) data[3][month][29]++;
                            else if (jk.equals("Perempuan")) data[3][month][30]++;
                            break;
                        case "Plasenta Manual":
                            if (jk.equals("Laki - laki")) data[3][month][31]++;
                            else if (jk.equals("Perempuan")) data[3][month][32]++;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        return data;
    }
    
    public int[][] processNeonatalReport(int[][] data, String date) {
        for (Persalinan p : persalinanList) {
            if (!p.getTanggalPartus().contains(date)) continue;
            
            switch (p.getKelurahan()) {
                case "Bunulrejo":
                    data = countNeonatalReport(p, 0, data);
                    break;
                case "Kesatrian":
                    data = countNeonatalReport(p, 1, data);
                    break;
                case "Jodipan":
                    data = countNeonatalReport(p, 2, data);
                    break;
                case "Polehan":
                    data = countNeonatalReport(p, 3, data);
                    break;
                default:
                    data = countNeonatalReport(p, 4, data);
                    break;
            }
        }
        return data;
    }
    
    private int[][] countNeonatalReport(Persalinan p, int index, int[][] data) {
        String[] neonatals = new String[] {
                "Bayi Lahir Prematur", 
                "Trauma Lahir", 
                "Asfiksia", 
                "BBLR < 2500 gr", 
                "Infeksi", 
                "Tetanus Neonatorum", 
                "Kelainan Bawaan",
                "Hipotiroid Kongenital (+)", 
                "Icterus", 
                "Masalah Pemberian ASI"
            };
        String kasus = p.getNeonatal(),
                    jk = p.getJenisKelamin();
        int uk = p.getUsiaKehamilan();
        
        // Bayi lahir prematur
        if (uk >= 28 && uk <=36) {
            if (jk.equals("Laki - laki")) data[index][0]++;
            else if (jk.equals("Perempuan")) data[index][1]++;
        }

        // Kasus neonatal
        if (kasus.contains(neonatals[1])) {
            if (jk.equals("Laki - laki")) data[index][6]++;
            else if (jk.equals("Perempuan")) data[index][7]++;
        }
        if (kasus.contains(neonatals[2])) {
            if (jk.equals("Laki - laki")) data[index][9]++;
            else if (jk.equals("Perempuan")) data[index][10]++;
        }
        if (kasus.contains(neonatals[3])) {
            if (jk.equals("Laki - laki")) data[index][12]++;
            else if (jk.equals("Perempuan")) data[index][13]++;
        }
        if (kasus.contains(neonatals[4])) {
            if (jk.equals("Laki - laki")) data[index][15]++;
            else if (jk.equals("Perempuan")) data[index][16]++;
        }
        if (kasus.contains(neonatals[5])) {
            if (jk.equals("Laki - laki")) data[index][18]++;
            else if (jk.equals("Perempuan")) data[index][19]++;
        }
        if (kasus.contains(neonatals[6])) {
            if (jk.equals("Laki - laki")) data[index][21]++;
            else if (jk.equals("Perempuan")) data[index][22]++;
        }
        
        // Kasus neonatal lainnya
        if (kasus.contains(neonatals[0])) {
            if (jk.equals("Laki - laki")) data[index][27]++;
            else if (jk.equals("Perempuan")) data[index][28]++;
        }
//        if (kasus.contains(neonatals[7])) {
//            if (jk.equals("Laki - laki")) data[index][27]++;
//            else if (jk.equals("Perempuan")) data[index][28]++;
//        }
        if (kasus.contains(neonatals[8])) {
            if (jk.equals("Laki - laki")) data[index][27]++;
            else if (jk.equals("Perempuan")) data[index][28]++;
        }
        if (kasus.contains(neonatals[9])) {
            if (jk.equals("Laki - laki")) data[index][27]++;
            else if (jk.equals("Perempuan")) data[index][28]++;
        }
        return data;
    }
    
    private void loadListPersalinan () throws SQLException {
        persalinanList.clear();
        for (Persalinan p : persalinanDao.queryForAll()) {
            persalinanList.add(p);
        }
    }
}
