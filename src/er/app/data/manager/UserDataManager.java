/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import er.app.config.AppConfig;
import er.app.config.DatabaseConfig;
import er.app.data.model.User;
import er.app.util.DialogUtils;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Deri Armanda
 */
public class UserDataManager {
    
    private static UserDataManager sInstance;
    private final Dao<User, Integer> userDao;
    private User currentUser;
    
    private UserDataManager() throws SQLException {
        userDao = DaoManager.createDao(
                DatabaseConfig.getConnection(), 
                User.class
        );
        currentUser = new User();
    }
    
    public static void init() {
        try {
            sInstance = new UserDataManager();
        } catch (SQLException ex) {
            sInstance = null;
            System.err.println("Error while creating User DAO: "+ex);
            DialogUtils.showError("Gagal menyambungkan ke tabel pengguna.");
        }
    }
    
    public static UserDataManager getInstance() {
        if (sInstance == null) init();
        return sInstance;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public int signingInUser(User user) {
        try {
            List<User> users = userDao.queryForMatching(user);
            if (users.isEmpty()) return AppConfig.ERROR_CODE;
            
            currentUser = users.get(0);
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while signing User with username ("+user.getUsername()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
    
    public int registerUser(User user) {
        try {
            userDao.create(user);
            currentUser = user;
            return AppConfig.SUCCESS_CODE;
        } catch (SQLException ex) {
            System.err.println("Error while registering User with username ("+user.getUsername()+") : "+ex);
            return AppConfig.ERROR_CODE;
        }
    }
}
