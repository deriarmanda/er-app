/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import er.app.data.persister.SimpleStringPropertyPersister;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "users")
public class User {
    
    public static final String USERNAME_FIELD_NAME = "username";
    public static final String PASSWORD_FIELD_NAME = "password";
    
    @DatabaseField(generatedId = true) private int uid;
    @DatabaseField(
            unique = true,
            columnName = USERNAME_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty username;
    
    @DatabaseField(
            columnName = PASSWORD_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty password;
    
    public User() { }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getUsername() {
        return username.get();
    }
    public void setUsername(String username) {
        if (this.username == null) this.username = new SimpleStringProperty(username);
        else this.username.set(username);
    }
    public StringProperty usernameProperty() {
        return username;
    }

    public String getPassword() {
        return password.get();
    }
    public void setPassword(String password) {
        if (this.password == null) this.password = new SimpleStringProperty(password);
        else this.password.set(password);
    }
    public StringProperty passwordProperty() {
        return password;
    }   

}
