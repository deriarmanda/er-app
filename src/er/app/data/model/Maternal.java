/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import er.app.data.persister.SimpleDoublePropertyPersister;
import er.app.data.persister.SimpleIntegerPropertyPersister;
import er.app.data.persister.SimpleStringPropertyPersister;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
@DatabaseTable(tableName = "maternal")
public class Maternal {
    
    public static final String NOMOR_RM_FIELD_NAME = "nomor_rm";
    public static final String NAMA_PASIEN_FIELD_NAME = "nama_pasien";
    public static final String TANGGAL_KUNJUNGAN_FIELD_NAME = "tanggal_kunjungan";
    public static final String KELURAHAN_FIELD_NAME = "kelurahan";
    public static final String KELUHAN_FIELD_NAME = "keluhan";
    public static final String HAMIL_KE_FIELD_NAME = "hamil_ke";
    public static final String RIWAYAT_FIELD_NAME = "riwayat_kehamilan";
    public static final String JARAK_ANAK_FIELD_NAME = "jarak_anak";
    public static final String BERAT_BADAN_FIELD_NAME = "berat_badan";
    public static final String TINGGI_BADAN_FIELD_NAME = "tinggi_badan";
    public static final String TEKANAN_DARAH_FIELD_NAME = "tekanan_darah";
    public static final String LINGKAR_LENGAN_ATAS_FIELD_NAME = "lingkar_lengan_atas";
    public static final String HEMOGLOBIN_FIELD_NAME = "hemoglobin";
    public static final String GOLONGAN_DARAH_FIELD_NAME = "golongan_darah";
    public static final String ALBUMIN_FIELD_NAME = "albumin";
    public static final String REDUKSI_FIELD_NAME = "reduksi";
    public static final String TINGGI_FUNDUS_FIELD_NAME = "tinggi_fundus";
    public static final String DENYUT_JANTUNG_FIELD_NAME = "denyut_jantung_janin";
    public static final String PITC_FIELD_NAME = "pitc";
    public static final String HBSAG_FIELD_NAME = "hbsag";
    public static final String TPHA_FIELD_NAME = "tpha";
    public static final String HPHT_FIELD_NAME = "hpht";
    public static final String TAKSIRAN_FIELD_NAME = "taksiran";
    public static final String LETAK_JANIN_FIELD_NAME = "letak_janin";
    public static final String SPR_FIELD_NAME = "spr";
    public static final String DIAGNOSA_FIELD_NAME = "diagnosa";
    public static final String TERAPI_FIELD_NAME = "terapi";
    public static final String MATERNAL_FIELD_NAME = "maternal";
    
    @DatabaseField(generatedId = true) private int uid;
    @DatabaseField(            
            columnName = NOMOR_RM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorRM;
    
    @DatabaseField(
            columnName = NAMA_PASIEN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaPasien;
    
    @DatabaseField(
            columnName = TANGGAL_KUNJUNGAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalKunjungan;
    
    @DatabaseField(
            columnName = KELURAHAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kelurahan;
    
    @DatabaseField(
            columnName = KELUHAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty keluhan;
    
    @DatabaseField(
            columnName = HAMIL_KE_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty hamilKe;
    
    @DatabaseField(
            columnName = RIWAYAT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty riwayat;
    
    @DatabaseField(
            columnName = JARAK_ANAK_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jarakAnak;
    
    @DatabaseField(
            columnName = BERAT_BADAN_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty beratBadan;
    
    @DatabaseField(
            columnName = TINGGI_BADAN_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty tinggiBadan;
    
    @DatabaseField(
            columnName = TEKANAN_DARAH_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tekananDarah;
    
    @DatabaseField(
            columnName = LINGKAR_LENGAN_ATAS_FIELD_NAME,
            persisterClass = SimpleDoublePropertyPersister.class
    ) private DoubleProperty lingkarLengan;
    
    @DatabaseField(
            columnName = HEMOGLOBIN_FIELD_NAME,
            persisterClass = SimpleDoublePropertyPersister.class
    ) private DoubleProperty hemoglobin;
    
    @DatabaseField(
            columnName = GOLONGAN_DARAH_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty golDarah;
    
    @DatabaseField(
            columnName = ALBUMIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty albumin;
    
    @DatabaseField(
            columnName = REDUKSI_FIELD_NAME,
            persisterClass = SimpleDoublePropertyPersister.class
    ) private DoubleProperty reduksi;
    
    @DatabaseField(
            columnName = TINGGI_FUNDUS_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty tinggiFundus;
    
    @DatabaseField(
            columnName = DENYUT_JANTUNG_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty denyutJantung;
    
    @DatabaseField(
            columnName = PITC_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty pitc;
    
    @DatabaseField(
            columnName = HBSAG_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty hbsAg;
    
    @DatabaseField(
            columnName = TPHA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tpha;
    
    @DatabaseField(
            columnName = HPHT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty hpht;
    
    @DatabaseField(
            columnName = TAKSIRAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty taksiran;
    
    @DatabaseField(
            columnName = LETAK_JANIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty letakJanin;
    
    @DatabaseField(
            columnName = SPR_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty spr;
    
    @DatabaseField(
            columnName = DIAGNOSA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty diagnosa;
    
    @DatabaseField(
            columnName = TERAPI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty terapi;
    
    @DatabaseField(
            columnName = MATERNAL_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty maternal;
    
    public Maternal() { }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getNomorRM() {
        return nomorRM.get();
    }
    public void setNomorRM(String nomorRM) {
        if (this.nomorRM == null) this.nomorRM = new SimpleStringProperty(nomorRM);
        else this.nomorRM.set(nomorRM);
    }
    public StringProperty nomorRMProperty() {
        return nomorRM;
    }

    public String getNamaPasien() {
        return namaPasien.get();
    }
    public void setNamaPasien(String namaPasien) {
        if (this.namaPasien == null) this.namaPasien = new SimpleStringProperty(namaPasien);
        else this.namaPasien.set(namaPasien);
    }
    public StringProperty namaPasienProperty() {
        return namaPasien;
    }   

    public String getTanggalKunjungan() {
        return tanggalKunjungan.get();
    }
    public void setTanggalKunjungan(String tanggalKunjungan) {
        if (this.tanggalKunjungan == null) this.tanggalKunjungan = new SimpleStringProperty(tanggalKunjungan);
        else this.tanggalKunjungan.set(tanggalKunjungan);
    }
    public StringProperty tanggalKunjunganProperty() {
        return tanggalKunjungan;
    }   

    public String getKelurahan() {
        return kelurahan.get();
    }
    public void setKelurahan(String kelurahan) {
        if (this.kelurahan == null) this.kelurahan = new SimpleStringProperty(kelurahan);
        else this.kelurahan.set(kelurahan);
    }
    public StringProperty kelurahanProperty() {
        return kelurahan;
    }   

    public String getKeluhan() {
        return keluhan.get();
    }
    public void setKeluhan(String keluhan) {
        if (this.keluhan == null) this.keluhan = new SimpleStringProperty(keluhan);
        else this.keluhan.set(keluhan);
    }
    public StringProperty keluhanProperty() {
        return keluhan;
    }   

    public String getHamilKe() {
        return hamilKe.get();
    }
    public void setHamilKe(String hamilKe) {
        if (this.hamilKe == null) this.hamilKe = new SimpleStringProperty(hamilKe);
        else this.hamilKe.set(hamilKe);
    }
    public StringProperty hamilKeProperty() {
        return hamilKe;
    }   

    public String getRiwayat() {
        return riwayat.get();
    }
    public void setRiwayat(String riwayat) {
        if (this.riwayat == null) this.riwayat = new SimpleStringProperty(riwayat);
        else this.riwayat.set(riwayat);
    }
    public StringProperty riwayatProperty() {
        return riwayat;
    }   

    public String getJarakAnak() {
        return jarakAnak.get();
    }
    public void setJarakAnak(String jarakAnak) {
        if (this.jarakAnak == null) this.jarakAnak = new SimpleStringProperty(jarakAnak);
        else this.jarakAnak.set(jarakAnak);
    }
    public StringProperty jarakAnakProperty() {
        return jarakAnak;
    }   

    public int getBeratBadan() {
        return beratBadan.get();
    }
    public void setBeratBadan(int beratBadan) {
        if (this.beratBadan == null) this.beratBadan = new SimpleIntegerProperty(beratBadan);
        else this.beratBadan.set(beratBadan);
    }
    public IntegerProperty beratBadanProperty() {
        return beratBadan;
    }   

    public int getTinggiBadan() {
        return tinggiBadan.get();
    }
    public void setTinggiBadan(int tinggiBadan) {
        if (this.tinggiBadan == null) this.tinggiBadan = new SimpleIntegerProperty(tinggiBadan);
        else this.tinggiBadan.set(tinggiBadan);
    }
    public IntegerProperty tinggiBadanProperty() {
        return tinggiBadan;
    }   

    public String getTekananDarah() {
        return tekananDarah.get();
    }
    public void setTekananDarah(String tekananDarah) {
        if (this.tekananDarah == null) this.tekananDarah = new SimpleStringProperty(tekananDarah);
        else this.tekananDarah.set(tekananDarah);
    }
    public StringProperty tekananDarahProperty() {
        return tekananDarah;
    }   

    public double getLingkarLengan() {
        return lingkarLengan.get();
    }
    public void setLingkarLengan(double lingkarLengan) {
        if (this.lingkarLengan == null) this.lingkarLengan = new SimpleDoubleProperty(lingkarLengan);
        else this.lingkarLengan.set(lingkarLengan);
    }
    public DoubleProperty lingkarLenganProperty() {
        return lingkarLengan;
    }   

    public double getHemoglobin() {
        return hemoglobin.get();
    }
    public void setHemoglobin(double hemoglobin) {
        if (this.hemoglobin == null) this.hemoglobin = new SimpleDoubleProperty(hemoglobin);
        else this.hemoglobin.set(hemoglobin);
    }
    public DoubleProperty hemoglobinProperty() {
        return hemoglobin;
    }   

    public String getGolDarah() {
        return golDarah.get();
    }
    public void setGolDarah(String golDarah) {
        if (this.golDarah == null) this.golDarah = new SimpleStringProperty(golDarah);
        else this.golDarah.set(golDarah);
    }
    public StringProperty golDarahProperty() {
        return golDarah;
    }   

    public String getAlbumin() {
        return albumin.get();
    }
    public void setAlbumin(String albumin) {
        if (this.albumin == null) this.albumin = new SimpleStringProperty(albumin);
        else this.albumin.set(albumin);
    }
    public StringProperty albuminProperty() {
        return albumin;
    }   

    public double getReduksi() {
        return reduksi.get();
    }
    public void setReduksi(double reduksi) {
        if (this.reduksi == null) this.reduksi = new SimpleDoubleProperty(reduksi);
        else this.reduksi.set(reduksi);
    }
    public DoubleProperty reduksiProperty() {
        return reduksi;
    }   

    public int getTinggiFundus() {
        return tinggiFundus.get();
    }
    public void setTinggiFundus(int tinggiFundus) {
        if (this.tinggiFundus == null) this.tinggiFundus = new SimpleIntegerProperty(tinggiFundus);
        else this.tinggiFundus.set(tinggiFundus);
    }
    public IntegerProperty tinggiFundusProperty() {
        return tinggiFundus;
    }   

    public int getDenyutJantung() {
        return denyutJantung.get();
    }
    public void setDenyutJantung(int denyutJantung) {
        if (this.denyutJantung == null) this.denyutJantung = new SimpleIntegerProperty(denyutJantung);
        else this.denyutJantung.set(denyutJantung);
    }
    public IntegerProperty denyutJantungProperty() {
        return denyutJantung;
    }   

    public String getPitc() {
        return pitc.get();
    }
    public void setPitc(String pitc) {
        if (this.pitc == null) this.pitc = new SimpleStringProperty(pitc);
        else this.pitc.set(pitc);
    }
    public StringProperty pitcProperty() {
        return pitc;
    }   

    public String getHbsAg() {
        return hbsAg.get();
    }
    public void setHbsAg(String hbsAg) {
        if (this.hbsAg == null) this.hbsAg = new SimpleStringProperty(hbsAg);
        else this.hbsAg.set(hbsAg);
    }
    public StringProperty hbsAgProperty() {
        return hbsAg;
    }   

    public String getTpha() {
        return tpha.get();
    }
    public void setTpha(String tpha) {
        if (this.tpha == null) this.tpha = new SimpleStringProperty(tpha);
        else this.tpha.set(tpha);
    }
    public StringProperty tphaProperty() {
        return tpha;
    }   

    public String getHpht() {
        return hpht.get();
    }
    public void setHpht(String hpht) {
        if (this.hpht == null) this.hpht = new SimpleStringProperty(hpht);
        else this.hpht.set(hpht);
    }
    public StringProperty hphtProperty() {
        return hpht;
    }   

    public String getTaksiran() {
        return taksiran.get();
    }
    public void setTaksiran(String taksiran) {
        if (this.taksiran == null) this.taksiran = new SimpleStringProperty(taksiran);
        else this.taksiran.set(taksiran);
    }
    public StringProperty taksiranProperty() {
        return taksiran;
    }   

    public String getLetakJanin() {
        return letakJanin.get();
    }
    public void setLetakJanin(String letakJanin) {
        if (this.letakJanin == null) this.letakJanin = new SimpleStringProperty(letakJanin);
        else this.letakJanin.set(letakJanin);
    }
    public StringProperty letakJaninProperty() {
        return letakJanin;
    }   

    public String getSpr() {
        return spr.get();
    }
    public void setSpr(String spr) {
        if (this.spr == null) this.spr = new SimpleStringProperty(spr);
        else this.spr.set(spr);
    }
    public StringProperty sprProperty() {
        return spr;
    }   

    public String getDiagnosa() {
        return diagnosa.get();
    }
    public void setDiagnosa(String diagnosa) {
        if (this.diagnosa == null) this.diagnosa = new SimpleStringProperty(diagnosa);
        else this.diagnosa.set(diagnosa);
    }
    public StringProperty diagnosaProperty() {
        return diagnosa;
    }   

    public String getTerapi() {
        return terapi.get();
    }
    public void setTerapi(String terapi) {
        if (this.terapi == null) this.terapi = new SimpleStringProperty(terapi);
        else this.terapi.set(terapi);
    }
    public StringProperty terapiProperty() {
        return terapi;
    }   

    public String getMaternal() {
        return maternal.get();
    }
    public void setMaternal(String maternal) {
        if (this.maternal == null) this.maternal = new SimpleStringProperty(maternal);
        else this.maternal.set(maternal);
    }
    public StringProperty maternalProperty() {
        return maternal;
    }   

}
