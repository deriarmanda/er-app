/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import er.app.data.persister.SimpleIntegerPropertyPersister;
import er.app.data.persister.SimpleStringPropertyPersister;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
@DatabaseTable(tableName = "persalinan")
public class Persalinan {
    
    public static final String NOMOR_RM_FIELD_NAME = "nomor_rm";
    public static final String NAMA_PASIEN_FIELD_NAME = "nama_pasien";
    public static final String TANGGAL_PARTUS_FIELD_NAME = "tanggal_partus";
    public static final String KELURAHAN_FIELD_NAME = "kelurahan";
    public static final String NAMA_SUAMI_FIELD_NAME = "nama_suami";
    public static final String DIAGNOSA_FIELD_NAME = "diagnosa";
    public static final String JENIS_PARTUS_FIELD_NAME = "jenis_partus";
    public static final String USIA_KEHAMILAN_FIELD_NAME = "usia_kehamilan";
    public static final String BERAT_BADAN_FIELD_NAME = "berat_badan";
    public static final String PANJANG_BADAN_FIELD_NAME = "panjang_badan";
    public static final String JENIS_KELAMIN_FIELD_NAME = "jenis_kelamin";
    public static final String KONDISI_IBU_FIELD_NAME = "kondisi_ibu";
    public static final String KONDISI_BAYI_FIELD_NAME = "kondisi_bayi";
    public static final String APGAR_SKOR_FIELD_NAME = "apgar_skor";
    public static final String VITAMIN_FIELD_NAME = "vitamin";
    public static final String VAKSIN_FIELD_NAME = "vaksin";
    public static final String NEONATAL_FIELD_NAME = "neonatal";
    public static final String TINDAKAN_FIELD_NAME = "tindakan";
    public static final String TEMPAT_LAHIR_FIELD_NAME = "tempat_lahir";
    public static final String NAMA_PENOLONG_FIELD_NAME = "nama_penolong";
    
    @DatabaseField(generatedId = true) private int uid;
    @DatabaseField(
            columnName = NOMOR_RM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorRM;
    
    @DatabaseField(
            columnName = NAMA_PASIEN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaPasien;
    
    @DatabaseField(
            columnName = TANGGAL_PARTUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalPartus;
    
    @DatabaseField(
            columnName = KELURAHAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kelurahan;
    
    @DatabaseField(
            columnName = NAMA_SUAMI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaSuami;
    
    @DatabaseField(
            columnName = DIAGNOSA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty diagnosa;
    
    @DatabaseField(
            columnName = JENIS_PARTUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jenisPartus;
    
    @DatabaseField(
            columnName = USIA_KEHAMILAN_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty usiaKehamilan;
    
    @DatabaseField(
            columnName = BERAT_BADAN_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty beratBadanLahir;
    
    @DatabaseField(
            columnName = PANJANG_BADAN_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty panjangBadanLahir;
    
    @DatabaseField(
            columnName = JENIS_KELAMIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jenisKelamin;
    
    @DatabaseField(
            columnName = KONDISI_IBU_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kondisiIbu;
    
    @DatabaseField(
            columnName = KONDISI_BAYI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kondisiBayi;
    
    @DatabaseField(
            columnName = APGAR_SKOR_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty apgarSkor;
    
    @DatabaseField(
            columnName = VITAMIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty vitamin;
    
    @DatabaseField(
            columnName = VAKSIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty vaksin;
    
    @DatabaseField(
            columnName = NEONATAL_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty neonatal;
    
    @DatabaseField(
            columnName = TINDAKAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tindakan;
    
    @DatabaseField(
            columnName = TEMPAT_LAHIR_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tempatLahir;
    
    @DatabaseField(
            columnName = NAMA_PENOLONG_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaPenolong;
    
    public Persalinan() { }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getNomorRM() {
        return nomorRM.get();
    }
    public void setNomorRM(String nomorRM) {
        if (this.nomorRM == null) this.nomorRM = new SimpleStringProperty(nomorRM);
        else this.nomorRM.set(nomorRM);
    }
    public StringProperty nomorRMProperty() {
        return nomorRM;
    }

    public String getNamaPasien() {
        return namaPasien.get();
    }
    public void setNamaPasien(String namaPasien) {
        if (this.namaPasien == null) this.namaPasien = new SimpleStringProperty(namaPasien);
        else this.namaPasien.set(namaPasien);
    }
    public StringProperty namaPasienProperty() {
        return namaPasien;
    }   

    public String getTanggalPartus() {
        return tanggalPartus.get();
    }
    public void setTanggalPartus(String tanggalPartus) {
        if (this.tanggalPartus == null) this.tanggalPartus = new SimpleStringProperty(tanggalPartus);
        else this.tanggalPartus.set(tanggalPartus);
    }
    public StringProperty tanggalPartusProperty() {
        return tanggalPartus;
    }   

    public String getKelurahan() {
        return kelurahan.get();
    }
    public void setKelurahan(String kelurahan) {
        if (this.kelurahan == null) this.kelurahan = new SimpleStringProperty(kelurahan);
        else this.kelurahan.set(kelurahan);
    }
    public StringProperty kelurahanProperty() {
        return kelurahan;
    }   

    public String getNamaSuami() {
        return namaSuami.get();
    }
    public void setNamaSuami(String namaSuami) {
        if (this.namaSuami == null) this.namaSuami = new SimpleStringProperty(namaSuami);
        else this.namaSuami.set(namaSuami);
    }
    public StringProperty namaSuamiProperty() {
        return namaSuami;
    }   

    public String getDiagnosa() {
        return diagnosa.get();
    }
    public void setDiagnosa(String diagnosa) {
        if (this.diagnosa == null) this.diagnosa = new SimpleStringProperty(diagnosa);
        else this.diagnosa.set(diagnosa);
    }
    public StringProperty diagnosaProperty() {
        return diagnosa;
    }   

    public String getJenisPartus() {
        return jenisPartus.get();
    }
    public void setJenisPartus(String jenisPartus) {
        if (this.jenisPartus == null) this.jenisPartus = new SimpleStringProperty(jenisPartus);
        else this.jenisPartus.set(jenisPartus);
    }
    public StringProperty jenisPartusProperty() {
        return jenisPartus;
    }   

    public int getUsiaKehamilan() {
        return usiaKehamilan.get();
    }
    public void setUsiaKehamilan(int usiaKehamilan) {
        if (this.usiaKehamilan == null) this.usiaKehamilan = new SimpleIntegerProperty(usiaKehamilan);
        else this.usiaKehamilan.set(usiaKehamilan);
    }
    public IntegerProperty usiaKehamilanProperty() {
        return usiaKehamilan;
    }   

    public int getBeratBadanLahir() {
        return beratBadanLahir.get();
    }
    public void setBeratBadanLahir(int beratBadanLahir) {
        if (this.beratBadanLahir == null) this.beratBadanLahir = new SimpleIntegerProperty(beratBadanLahir);
        else this.beratBadanLahir.set(beratBadanLahir);
    }
    public IntegerProperty beratBadanLahirProperty() {
        return beratBadanLahir;
    }   

    public int getPanjangBadanLahir() {
        return panjangBadanLahir.get();
    }
    public void setPanjangBadanLahir(int panjangBadanLahir) {
        if (this.panjangBadanLahir == null) this.panjangBadanLahir = new SimpleIntegerProperty(panjangBadanLahir);
        else this.panjangBadanLahir.set(panjangBadanLahir);
    }
    public IntegerProperty panjangBadanLahirProperty() {
        return panjangBadanLahir;
    }   

    public String getJenisKelamin() {
        return jenisKelamin.get();
    }
    public void setJenisKelamin(String jenisKelamin) {
        if (this.jenisKelamin == null) this.jenisKelamin = new SimpleStringProperty(jenisKelamin);
        else this.jenisKelamin.set(jenisKelamin);
    }
    public StringProperty jenisKelaminProperty() {
        return jenisKelamin;
    }   

    public String getKondisiIbu() {
        return kondisiIbu.get();
    }
    public void setKondisiIbu(String kondisiIbu) {
        if (this.kondisiIbu == null) this.kondisiIbu = new SimpleStringProperty(kondisiIbu);
        else this.kondisiIbu.set(kondisiIbu);
    }
    public StringProperty kondisiIbuProperty() {
        return kondisiIbu;
    }   

    public String getKondisiBayi() {
        return kondisiBayi.get();
    }
    public void setKondisiBayi(String kondisiBayi) {
        if (this.kondisiBayi == null) this.kondisiBayi = new SimpleStringProperty(kondisiBayi);
        else this.kondisiBayi.set(kondisiBayi);
    }
    public StringProperty kondisiBayiProperty() {
        return kondisiBayi;
    }   

    public int getApgarSkor() {
        return apgarSkor.get();
    }
    public void setApgarSkor(int apgarSkor) {
        if (this.apgarSkor == null) this.apgarSkor = new SimpleIntegerProperty(apgarSkor);
        else this.apgarSkor.set(apgarSkor);
    }
    public IntegerProperty apgarSkorProperty() {
        return apgarSkor;
    }   

    public String getVitamin() {
        return vitamin.get();
    }
    public void setVitamin(String vitamin) {
        if (this.vitamin == null) this.vitamin = new SimpleStringProperty(vitamin);
        else this.vitamin.set(vitamin);
    }
    public StringProperty vitaminProperty() {
        return vitamin;
    }   

    public String getVaksin() {
        return vaksin.get();
    }
    public void setVaksin(String vaksin) {
        if (this.vaksin == null) this.vaksin = new SimpleStringProperty(vaksin);
        else this.vaksin.set(vaksin);
    }
    public StringProperty vaksinProperty() {
        return vaksin;
    }   

    public String getNeonatal() {
        return neonatal.get();
    }
    public void setNeonatal(String neonatal) {
        if (this.neonatal == null) this.neonatal = new SimpleStringProperty(neonatal);
        else this.neonatal.set(neonatal);
    }
    public StringProperty neonatalProperty() {
        return neonatal;
    }   

    public String getTindakan() {
        return tindakan.get();
    }
    public void setTindakan(String tindakan) {
        if (this.tindakan == null) this.tindakan = new SimpleStringProperty(tindakan);
        else this.tindakan.set(tindakan);
    }
    public StringProperty tindakanProperty() {
        return tindakan;
    }   

    public String getTempatLahir() {
        return tempatLahir.get();
    }
    public void setTempatLahir(String tempatLahir) {
        if (this.tempatLahir == null) this.tempatLahir = new SimpleStringProperty(tempatLahir);
        else this.tempatLahir.set(tempatLahir);
    }
    public StringProperty tempatLahirProperty() {
        return tempatLahir;
    }   

    public String getNamaPenolong() {
        return namaPenolong.get();
    }
    public void setNamaPenolong(String namaPenolong) {
        if (this.namaPenolong == null) this.namaPenolong = new SimpleStringProperty(namaPenolong);
        else this.namaPenolong.set(namaPenolong);
    }
    public StringProperty namaPenolongProperty() {
        return namaPenolong;
    }   

}
