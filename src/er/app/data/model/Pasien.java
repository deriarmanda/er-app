/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.model;

import er.app.data.persister.SimpleStringPropertyPersister;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
@DatabaseTable(tableName = "pasien")
public class Pasien {
    
    public static final String NOMOR_RM_FIELD_NAME = "nomor_rm";
    public static final String NAMA_LENGKAP_FIELD_NAME = "nama_lengkap";
    public static final String JENIS_KELAMIN_FIELD_NAME = "jenis_kelamin";
    public static final String TEMPAT_TANGGAL_LAHIR_FIELD_NAME = "tempat_tanggal_lahir";
    public static final String USIA_FIELD_NAME = "usia";
    public static final String NAMA_KK_FIELD_NAME = "nama_kk";
    public static final String NIK_FIELD_NAME = "nik";
    public static final String NOMOR_TELEPON_FIELD_NAME = "nomor_telepon";
    public static final String ALAMAT_FIELD_NAME = "alamat";
    public static final String KELURAHAN_FIELD_NAME = "kelurahan";
    public static final String PEMBAYARAN_FIELD_NAME = "pembayaran";
    public static final String NOMOR_BPJS_FIELD_NAME = "nomor_bpjs";
    
    @DatabaseField(generatedId = true) private int uid;
    @DatabaseField(
            unique = true,
            columnName = NOMOR_RM_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorRM;
    
    @DatabaseField(
            columnName = NAMA_LENGKAP_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaLengkap;
    
    @DatabaseField(
            columnName = JENIS_KELAMIN_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jenisKelamin;
    
    @DatabaseField(
            columnName = TEMPAT_TANGGAL_LAHIR_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tempatTanggalLahir;
    
    @DatabaseField(
            columnName = USIA_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class 
    ) private StringProperty usia;
    
    @DatabaseField(
            columnName = NAMA_KK_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaKK;
    
    @DatabaseField(
            columnName = NIK_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nik;
    
    @DatabaseField(
            columnName = NOMOR_TELEPON_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorTelepon;
    
    @DatabaseField(
            columnName = ALAMAT_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty alamat;
    
    @DatabaseField(
            columnName = KELURAHAN_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kelurahan;
    
    @DatabaseField(
            columnName = PEMBAYARAN_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty pembayaran;
    
    @DatabaseField(
            columnName = NOMOR_BPJS_FIELD_NAME, 
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorBpjs;
    
    public Pasien() {
        this(
                "-", "-", "Laki - laki",
                "", "", "", "", "", "", "Lainnya",
                "", ""
        );
    }
    public Pasien(
            String nomorRM, String namaLengkap, String jenisKelamin,
            String tempatTanggalLahir, String usia, String namaKK, String nik, 
            String nomorTelepon, String alamat, String kelurahan, String pembayaran, String nomorBpjs
    ) {
        this.nomorRM = new SimpleStringProperty(nomorRM);
        this.namaLengkap = new SimpleStringProperty(namaLengkap);
        this.jenisKelamin = new SimpleStringProperty(jenisKelamin);
        this.tempatTanggalLahir = new SimpleStringProperty(tempatTanggalLahir);
        this.usia = new SimpleStringProperty(usia);
        this.namaKK = new SimpleStringProperty(namaKK);
        this.nik = new SimpleStringProperty(nik);
        this.nomorTelepon = new SimpleStringProperty(nomorTelepon);
        this.alamat = new SimpleStringProperty(alamat);
        this.kelurahan = new SimpleStringProperty(kelurahan);
        this.pembayaran = new SimpleStringProperty(pembayaran);
        this.nomorBpjs = new SimpleStringProperty(nomorBpjs);
    }

    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getNomorRM() {
        return nomorRM.get();
    }
    public void setNomorRM(String nomorRM) {
        if (this.nomorRM == null) this.nomorRM = new SimpleStringProperty(nomorRM);
        else this.nomorRM.set(nomorRM);
    }
    public StringProperty nomorRMProperty() {
        return nomorRM;
    }

    public String getNamaLengkap() {
        return namaLengkap.get();
    }
    public void setNamaLengkap(String namaLengkap) {
        if (this.namaLengkap == null) this.namaLengkap = new SimpleStringProperty(namaLengkap);
        else this.namaLengkap.set(namaLengkap);
    }
    public StringProperty namaLengkapProperty() {
        return namaLengkap;
    }

    public String getJenisKelamin() {
        if (jenisKelamin == null) return "";
        return jenisKelamin.get();
    }
    public void setJenisKelamin(String jenisKelamin) {
        if (this.jenisKelamin == null) this.jenisKelamin = new SimpleStringProperty(jenisKelamin);
        else this.jenisKelamin.set(jenisKelamin);
    }
    public StringProperty jenisKelaminProperty() {
        return jenisKelamin;
    }

    public String getTempatTanggalLahir() {
        if (tempatTanggalLahir == null) return "";
        return tempatTanggalLahir.get();
    }
    public void setTempatTanggalLahir(String tempatTanggalLahir) {
        if (this.tempatTanggalLahir == null) this.tempatTanggalLahir = new SimpleStringProperty(tempatTanggalLahir);
        else this.tempatTanggalLahir.set(tempatTanggalLahir);
    }
    public StringProperty tempatTanggalLahirProperty() {
        return tempatTanggalLahir;
    }

    public String getUsia() {
        if (usia == null) return "";
        return usia.get();
    }
    public void setUsia(String usia) {
        if (this.usia == null) this.usia = new SimpleStringProperty(usia);
        else this.usia.set(usia);
    }
    public StringProperty usiaProperty() {
        return usia;
    }

    public String getNamaKK() {
        if (namaKK == null) return "";
        return namaKK.get();
    }
    public void setNamaKK(String namaKK) {
        if (this.namaKK == null) this.namaKK = new SimpleStringProperty(namaKK);
        else this.namaKK.set(namaKK);
    }
    public StringProperty namaKKProperty() {
        return namaKK;
    }

    public String getNIK() {
        if (nik == null) return "";
        return nik.get();
    }
    public void setNIK(String nik) {
        if (this.nik == null) this.nik = new SimpleStringProperty(nik);
        else this.nik.set(nik);
    }
    public StringProperty nikProperty() {
        return nik;
    }

    public String getNomorTelepon() {
        if (nomorTelepon == null) return "";
        return nomorTelepon.get();
    }
    public void setNomorTelepon(String nomorTelepon) {
        if (this.nomorTelepon == null) this.nomorTelepon = new SimpleStringProperty(nomorTelepon);
        else this.nomorTelepon.set(nomorTelepon);
    }
    public StringProperty nomorTeleponProperty() {
        return nomorTelepon;
    }

    public String getAlamat() {
        if (alamat == null) return "";
        return alamat.get();
    }
    public void setAlamat(String alamat) {
        if (this.alamat == null) this.alamat = new SimpleStringProperty(alamat);
        else this.alamat.set(alamat);
    }
    public StringProperty alamatProperty() {
        return alamat;
    }

    public String getKelurahan() {
        if (kelurahan == null) return "";
        return kelurahan.get();
    }
    public void setKelurahan(String kelurahan) {
        if (this.kelurahan == null) this.kelurahan = new SimpleStringProperty(kelurahan);
        else this.kelurahan.set(kelurahan);
    }
    public StringProperty kelurahanProperty() {
        return kelurahan;
    }

    public String getPembayaran() {
        if (pembayaran == null) return "";
        return pembayaran.get();
    }
    public void setPembayaran(String pembayaran) {
        if (this.pembayaran == null) this.pembayaran = new SimpleStringProperty(pembayaran);
        else this.pembayaran.set(pembayaran);
    }
    public StringProperty pembayaranProperty() {
        return pembayaran;
    }

    public String getNomorBpjs() {
        if (nomorBpjs == null) return "";
        return nomorBpjs.get();
    }
    public void setNomorBpjs(String nomorBpjs) {
        if (this.nomorBpjs == null) this.nomorBpjs = new SimpleStringProperty(nomorBpjs);
        else this.nomorBpjs.set(nomorBpjs);
    }  
    public StringProperty nomorBpjsProperty() {
        return nomorBpjs;
    } 
    
}
