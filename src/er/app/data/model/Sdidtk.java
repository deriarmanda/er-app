/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import er.app.data.persister.SimpleIntegerPropertyPersister;
import er.app.data.persister.SimpleStringPropertyPersister;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
@DatabaseTable(tableName = "sdidtk")
public class Sdidtk {
    
    public static final String NOMOR_RM_FIELD_NAME = "nomor_rm";
    public static final String NAMA_ANAK_FIELD_NAME = "nama_anak";
    public static final String TANGGAL_FIELD_NAME = "tanggal";
    public static final String KELURAHAN_FIELD_NAME = "kelurahan";
    public static final String JENIS_KELAMIN_FIELD_NAME = "jenis_kelamin";
    public static final String NAMA_IBU_FIELD_NAME = "nama_ibu";
    public static final String NAMA_AYAH_FIELD_NAME = "nama_ayah";
    public static final String USIA_ANAK_FIELD_NAME = "usia_anak";
    public static final String BERAT_BADAN_FIELD_NAME = "berat_badan";
    public static final String TINGGI_BADAN_FIELD_NAME = "tinggi_badan";
    public static final String LINGKAR_KEPALA_FIELD_NAME = "lingkar_kepala";
    public static final String DIAGNOSA_FIELD_NAME = "diagnosa";
    public static final String GANGGUAN_LKA_FIELD_NAME = "gangguan_lka";
    public static final String MOTORIK_KASAR_FIELD_NAME = "motorik_kasar";
    public static final String MOTORIK_HALUS_FIELD_NAME = "motorik_halus";
    public static final String BICARA_BAHASA_FIELD_NAME = "bicara_bahasa";
    public static final String SOSIALISASI_FIELD_NAME = "sosialisasi";
    public static final String GANGGUAN_TDL_FIELD_NAME = "gangguan_tdl";
    public static final String GANGGUAN_TDD_FIELD_NAME = "gangguan_tdd";
    public static final String MME_FIELD_NAME = "mme";
    public static final String PERLU_DIRUJUK_FIELD_NAME = "perlu_dirujuk";
    
    @DatabaseField(generatedId = true) private int uid;
    @DatabaseField(
            columnName = NOMOR_RM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorRM;
    
    @DatabaseField(
            columnName = NAMA_ANAK_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaAnak;
    
    @DatabaseField(
            columnName = TANGGAL_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggal;
    
    @DatabaseField(
            columnName = KELURAHAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kelurahan;
    
    @DatabaseField(
            columnName = JENIS_KELAMIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jenisKelamin;
    
    @DatabaseField(
            columnName = NAMA_IBU_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaIbu;
    
    @DatabaseField(
            columnName = NAMA_AYAH_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaAyah;
    
    @DatabaseField(
            columnName = USIA_ANAK_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty usiaAnak;
    
    @DatabaseField(
            columnName = BERAT_BADAN_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty beratBadan;
    
    @DatabaseField(
            columnName = TINGGI_BADAN_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty tinggiBadan;
    
    @DatabaseField(
            columnName = LINGKAR_KEPALA_FIELD_NAME,
            persisterClass = SimpleIntegerPropertyPersister.class
    ) private IntegerProperty lingkarKepala;
    
    @DatabaseField(
            columnName = DIAGNOSA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty diagnosa;
    
    @DatabaseField(
            columnName = GANGGUAN_LKA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty gangguanLka;
    
    @DatabaseField(
            columnName = MOTORIK_KASAR_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty motorikKasar;
    
    @DatabaseField(
            columnName = MOTORIK_HALUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty motorikHalus;
    
    @DatabaseField(
            columnName = BICARA_BAHASA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty bicaraBahasa;
    
    @DatabaseField(
            columnName = SOSIALISASI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty sosialisasi;
    
    @DatabaseField(
            columnName = GANGGUAN_TDL_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty gangguanTdl;
    
    @DatabaseField(
            columnName = GANGGUAN_TDD_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty gangguanTdd;
    
    @DatabaseField(
            columnName = MME_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty mme;
    
    @DatabaseField(
            columnName = PERLU_DIRUJUK_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty perluDirujuk;
    
    public Sdidtk() { }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getNomorRM() {
        return nomorRM.get();
    }
    public void setNomorRM(String nomorRM) {
        if (this.nomorRM == null) this.nomorRM = new SimpleStringProperty(nomorRM);
        else this.nomorRM.set(nomorRM);
    }
    public StringProperty nomorRMProperty() {
        return nomorRM;
    }

    public String getNamaAnak() {
        return namaAnak.get();
    }
    public void setNamaAnak(String namaAnak) {
        if (this.namaAnak == null) this.namaAnak = new SimpleStringProperty(namaAnak);
        else this.namaAnak.set(namaAnak);
    }
    public StringProperty namaAnakProperty() {
        return namaAnak;
    }   

    public String getTanggal() {
        return tanggal.get();
    }
    public void setTanggal(String tanggal) {
        if (this.tanggal == null) this.tanggal = new SimpleStringProperty(tanggal);
        else this.tanggal.set(tanggal);
    }
    public StringProperty tanggalProperty() {
        return tanggal;
    }   

    public String getKelurahan() {
        return kelurahan.get();
    }
    public void setKelurahan(String kelurahan) {
        if (this.kelurahan == null) this.kelurahan = new SimpleStringProperty(kelurahan);
        else this.kelurahan.set(kelurahan);
    }
    public StringProperty kelurahanProperty() {
        return kelurahan;
    }   

    public String getJenisKelamin() {
        return jenisKelamin.get();
    }
    public void setJenisKelamin(String jenisKelamin) {
        if (this.jenisKelamin == null) this.jenisKelamin = new SimpleStringProperty(jenisKelamin);
        else this.jenisKelamin.set(jenisKelamin);
    }
    public StringProperty jenisKelaminProperty() {
        return jenisKelamin;
    }   

    public String getNamaIbu() {
        return namaIbu.get();
    }
    public void setNamaIbu(String namaIbu) {
        if (this.namaIbu == null) this.namaIbu = new SimpleStringProperty(namaIbu);
        else this.namaIbu.set(namaIbu);
    }
    public StringProperty namaIbuProperty() {
        return namaIbu;
    }   

    public String getNamaAyah() {
        return namaAyah.get();
    }
    public void setNamaAyah(String namaAyah) {
        if (this.namaAyah == null) this.namaAyah = new SimpleStringProperty(namaAyah);
        else this.namaAyah.set(namaAyah);
    }
    public StringProperty namaAyahProperty() {
        return namaAyah;
    }   

    public int getUsiaAnak() {
        return usiaAnak.get();
    }
    public void setUsiaAnak(int usiaAnak) {
        if (this.usiaAnak == null) this.usiaAnak = new SimpleIntegerProperty(usiaAnak);
        else this.usiaAnak.set(usiaAnak);
    }
    public IntegerProperty usiaAnakProperty() {
        return usiaAnak;
    }   

    public int getBeratBadan() {
        return beratBadan.get();
    }
    public void setBeratBadan(int beratBadan) {
        if (this.beratBadan == null) this.beratBadan = new SimpleIntegerProperty(beratBadan);
        else this.beratBadan.set(beratBadan);
    }
    public IntegerProperty beratBadanProperty() {
        return beratBadan;
    }   

    public int getTinggiBadan() {
        return tinggiBadan.get();
    }
    public void setTinggiBadan(int tinggiBadan) {
        if (this.tinggiBadan == null) this.tinggiBadan = new SimpleIntegerProperty(tinggiBadan);
        else this.tinggiBadan.set(tinggiBadan);
    }
    public IntegerProperty tinggiBadanProperty() {
        return tinggiBadan;
    }   

    public int getLingkarKepala() {
        return lingkarKepala.get();
    }
    public void setLingkarKepala(int lingkarKepala) {
        if (this.lingkarKepala == null) this.lingkarKepala = new SimpleIntegerProperty(lingkarKepala);
        else this.lingkarKepala.set(lingkarKepala);
    }
    public IntegerProperty lingkarKepalaProperty() {
        return lingkarKepala;
    }   

    public String getDiagnosa() {
        return diagnosa.get();
    }
    public void setDiagnosa(String diagnosa) {
        if (this.diagnosa == null) this.diagnosa = new SimpleStringProperty(diagnosa);
        else this.diagnosa.set(diagnosa);
    }
    public StringProperty diagnosaProperty() {
        return diagnosa;
    }   

    public String getGangguanLka() {
        return gangguanLka.get();
    }
    public void setGangguanLka(String gangguanLka) {
        if (this.gangguanLka == null) this.gangguanLka = new SimpleStringProperty(gangguanLka);
        else this.gangguanLka.set(gangguanLka);
    }
    public StringProperty gangguanLkaProperty() {
        return gangguanLka;
    }   

    public String getMotorikKasar() {
        return motorikKasar.get();
    }
    public void setMotorikKasar(String motorikKasar) {
        if (this.motorikKasar == null) this.motorikKasar = new SimpleStringProperty(motorikKasar);
        else this.motorikKasar.set(motorikKasar);
    }
    public StringProperty motorikKasarProperty() {
        return motorikKasar;
    }   

    public String getMotorikHalus() {
        return motorikHalus.get();
    }
    public void setMotorikHalus(String motorikHalus) {
        if (this.motorikHalus == null) this.motorikHalus = new SimpleStringProperty(motorikHalus);
        else this.motorikHalus.set(motorikHalus);
    }
    public StringProperty motorikHalusProperty() {
        return motorikHalus;
    }   

    public String getBicaraBahasa() {
        return bicaraBahasa.get();
    }
    public void setBicaraBahasa(String bicaraBahasa) {
        if (this.bicaraBahasa == null) this.bicaraBahasa = new SimpleStringProperty(bicaraBahasa);
        else this.bicaraBahasa.set(bicaraBahasa);
    }
    public StringProperty bicaraBahasaProperty() {
        return bicaraBahasa;
    }   

    public String getSosialisasi() {
        return sosialisasi.get();
    }
    public void setSosialisasi(String sosialisasi) {
        if (this.sosialisasi == null) this.sosialisasi = new SimpleStringProperty(sosialisasi);
        else this.sosialisasi.set(sosialisasi);
    }
    public StringProperty sosialisasiProperty() {
        return sosialisasi;
    }   

    public String getGangguanTdl() {
        return gangguanTdl.get();
    }
    public void setGangguanTdl(String gangguanTdl) {
        if (this.gangguanTdl == null) this.gangguanTdl = new SimpleStringProperty(gangguanTdl);
        else this.gangguanTdl.set(gangguanTdl);
    }
    public StringProperty gangguanTdlProperty() {
        return gangguanTdl;
    }   

    public String getGangguanTdd() {
        return gangguanTdd.get();
    }
    public void setGangguanTdd(String gangguanTdd) {
        if (this.gangguanTdd == null) this.gangguanTdd = new SimpleStringProperty(gangguanTdd);
        else this.gangguanTdd.set(gangguanTdd);
    }
    public StringProperty gangguanTddProperty() {
        return gangguanTdd;
    }   

    public String getMme() {
        return mme.get();
    }
    public void setMme(String mme) {
        if (this.mme == null) this.mme = new SimpleStringProperty(mme);
        else this.mme.set(mme);
    }
    public StringProperty mmeProperty() {
        return mme;
    }   

    public String getPerluDirujuk() {
        return perluDirujuk.get();
    }
    public void setPerluDirujuk(String perluDirujuk) {
        if (this.perluDirujuk == null) this.perluDirujuk = new SimpleStringProperty(perluDirujuk);
        else this.perluDirujuk.set(perluDirujuk);
    }
    public StringProperty perluDirujukProperty() {
        return perluDirujuk;
    }   

}
