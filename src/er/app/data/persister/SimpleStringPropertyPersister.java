/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.data.persister;

/**
 * Created by Jonty on 13/09/15.
 * This class tells ORMlite how to store a SimpleStringProperty in a SQL database.
 *
 */

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDataType;
import com.j256.ormlite.support.DatabaseResults;
import javafx.beans.property.SimpleStringProperty;

import java.sql.SQLException;

public class SimpleStringPropertyPersister extends BaseDataType {

    private static final SimpleStringPropertyPersister singleTon = new SimpleStringPropertyPersister();

    private SimpleStringPropertyPersister() {
        super(SqlType.STRING, new Class<?>[]{String.class});
    }

    /**
     * Getter for singleton of class.
     * @return
     */
    public static SimpleStringPropertyPersister getSingleton() {
        return singleTon;
    }

    /**
     * Parse a default String into a SimpleStringProperty.
     * @param fieldType Field Type
     * @param defaultStr Default string to parse
     * @return New SimpleStringProperty
     */
    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) throws SQLException {
        return new SimpleStringProperty(defaultStr);
    }

    /**
     * Converts database results into Sql argument
     * @param fieldType Field Type
     * @param results Results to convert
     * @param columnPos Column position
     * @return SQL argument from input
     * @throws SQLException
     */
    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        String resultsStr = results.getString(columnPos);
        return resultsStr;
    }

    /**
     * Converts SQL argument into a SimpleStringProperty.
     * @param fieldType Field Type
     * @param sqlArg SQL argument to convert
     * @param columnPos Column Position
     * @return SimpleStringProperty from input
     */
    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        return new SimpleStringProperty((String) sqlArg);
    }

    /**
     * Converts a SimpleStringProperty into a string to be store in the database.
     * @param fieldType Field Type
     * @param javaObject SimpleStringProperty to convert
     * @return string from input
     * @throws SQLException
     */
    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        if (javaObject.getClass().equals(String.class)) {
        }
        SimpleStringProperty stringProperty = (SimpleStringProperty) javaObject;
        return stringProperty.getValue();
    }
}