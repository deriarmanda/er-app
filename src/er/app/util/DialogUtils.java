/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.util;

import er.app.config.AppConfig;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author DERI
 */
public class DialogUtils {
    
    public static int RESULT_OK = 1;
    public static int RESULT_CANCELLED = 0;
    
    private static Stage mainStage;
    
    private static Alert splashDialog;
    private static Alert aboutDialog;
    private static Alert infoDialog;
    private static Alert errorDialog;
    private static Alert confirmDialog;
    private static FileChooser fileDialog;
    
    public static void init(Stage mainStage) {
        DialogUtils.mainStage = mainStage;
        Stage stage;
        
        // Splash Dialog
        splashDialog = new Alert(AlertType.NONE);
        splashDialog.setTitle("Mohon Menunggu");
        splashDialog.setHeaderText(
                "ER - App\n"
                + "Aplikasi Pelaporan Elektronik"
        );
        splashDialog.setContentText("Sedang menyambungkan ke database...");
        ImageView splashLogo = new ImageView(AppConfig.appLogo);
        splashLogo.setFitWidth(96);
        splashLogo.setFitHeight(96);
        splashDialog.setGraphic(splashLogo);
        stage = (Stage) splashDialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(AppConfig.appLogo);
        
        // About Dialog
        aboutDialog = new Alert(AlertType.INFORMATION);
        aboutDialog.setTitle("About App");
        aboutDialog.setHeaderText(
                "ER - App\n"
                + "Aplikasi Pelaporan Elektronik"
        );
        aboutDialog.setContentText(AppConfig.appAuthor);
        stage = (Stage) aboutDialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(AppConfig.appLogo);
        
        // Information Dialog
        infoDialog = new Alert(AlertType.INFORMATION);
        infoDialog.setHeaderText("Pesan Informasi");
        stage = (Stage) infoDialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(AppConfig.appLogo);
        
        // Information Dialog
        errorDialog = new Alert(AlertType.ERROR);
        errorDialog.setHeaderText("Terjadi Kesalahan !");
        stage = (Stage) errorDialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(AppConfig.appLogo);
        
        // Information Dialog
        confirmDialog = new Alert(
                AlertType.CONFIRMATION,
                "Dummy content",
                ButtonType.YES,
                ButtonType.NO
        );
        confirmDialog.setHeaderText("Konfirmasi Ulang");
        stage = (Stage) confirmDialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(AppConfig.appLogo);
        
        // File Chooser Dialog
        fileDialog = new FileChooser();
        fileDialog.getExtensionFilters().add(new FileChooser.ExtensionFilter("Excel Files", "*.xlsx"));
    }
    
    public static void showInfo(String message) {
        if (infoDialog == null) return;
        infoDialog.setContentText(message);
        infoDialog.showAndWait();
    }
    
    public static void showError(String message) {
        if (errorDialog == null) return;
        errorDialog.setContentText(message);
        errorDialog.showAndWait();
    }
    
    public static int showConfirm(String message) {
        if (confirmDialog == null) return RESULT_CANCELLED;
        confirmDialog.setContentText(message);
        confirmDialog.showAndWait();
        return confirmDialog.getResult() != ButtonType.YES? 
                RESULT_CANCELLED : 
                RESULT_OK;
    }
    
    public static File showSaveFileDialog(String fileName) {
        fileDialog.setInitialFileName(fileName);
        return fileDialog.showSaveDialog(mainStage);
    }
    
    public static void showAbout() {
        aboutDialog.showAndWait();
    }
    
    public static void showSplash() {
        splashDialog.show();
    }
    public static void closeSplash() {
        splashDialog.setResult(ButtonType.OK);
        splashDialog.close();
    }
}
