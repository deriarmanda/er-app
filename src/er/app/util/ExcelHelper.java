/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.util;

import er.app.config.AppConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author DERI
 */
public class ExcelHelper {
    
    private static void copyFile(File target, String source) throws IOException {
        target.createNewFile();
        FileOutputStream out;
        try (InputStream in = new FileInputStream(source)) {
            out = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            int read;
            while((read = in.read(buffer)) != -1){
                out.write(buffer, 0, read);
            }
        }
        out.close();
    }
    
    public static int exportMaternalMonthlyReport(
            File file, 
            String month, 
            String year, 
            int[][] data
    ) {
        try {
            copyFile(file, System.getProperty("user.dir")
                +File.separator+"laporan_bulanan_maternal.xlsx");
            
            // Preparation
            FileInputStream in = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(in);
            XSSFSheet sheet = workbook.getSheetAt(0);
            String header = "PUSKESMAS :  KENDAL KEREP BULAN "
                    +month.toUpperCase()
                    +" TAHUN "+year;
            
            // Header file
            XSSFRow row = sheet.getRow(1);
            XSSFCell cell = row.getCell(2);
            cell.setCellValue(header);
            cell = row.getCell(11);
            cell.setCellValue(header);
            cell = row.getCell(24);
            cell.setCellValue(header);
            
            // Footer file
            row = sheet.getRow(14);
            cell = row.getCell(30);
            String date = DateTimeUtils.getCurrentHumanReadableDate();
            date = date.substring(date.indexOf(" ")+1);
            cell.setCellValue("Malang, "+date);
            
            // Content
            for (int i=0; i<4; i++) {
                row =sheet.getRow(i+8);
                for (int j=0; j<35; j++) {
                    cell = row.getCell(j+2);
                    cell.setCellValue(data[i][j]);
                }
            }
            
            // Closing
            XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
            try (FileOutputStream out = new FileOutputStream(file)) {
                workbook.write(out);
            }
            
            return AppConfig.SUCCESS_CODE;
        } catch (IOException ex) {
            return AppConfig.ERROR_CODE;            
        }
    }
    
    public static int exportMaternalAnnualReport(
            File file, 
            String year, 
            int[][][] data
    ) {
        try {
            copyFile(file, System.getProperty("user.dir")
                +File.separator+"laporan_tahunan_maternal.xlsx");
            
            // Preparation
            FileInputStream in = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(in);
            String header = "PUSKESMAS :  KENDAL KEREP TAHUN "+year;
            
            // Iteration 4 sheets 
            for (int kelurahan=0; kelurahan<4; kelurahan++) {
                // Header file
                XSSFSheet sheet = workbook.getSheetAt(kelurahan);
                XSSFRow row = sheet.getRow(1);
                XSSFCell cell = row.getCell(2);
                cell.setCellValue(header);
                cell = row.getCell(11);
                cell.setCellValue(header);
                cell = row.getCell(24);
                cell.setCellValue(header);

                // Content
                for (int i=0; i<12; i++) {
                    row =sheet.getRow(i+8);
                    for (int j=0; j<35; j++) {
                        cell = row.getCell(j+2);
                        cell.setCellValue(data[kelurahan][i][j]);
                    }
                }
            }
            
            // Closing
            XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
            try (FileOutputStream out = new FileOutputStream(file)) {
                workbook.write(out);
            }
            
            return AppConfig.SUCCESS_CODE;
        } catch (IOException ex) {
            return AppConfig.ERROR_CODE;            
        }
    }
    
    public static int exportNeonatalReport(
            File file, 
            String header,
            int data[][]
    ) {
        try {
            copyFile(file, System.getProperty("user.dir")
                +File.separator+"laporan_neonatal.xlsx");
            
            // Preparation
            FileInputStream in = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(in);
            XSSFSheet sheet = workbook.getSheetAt(0);
            
            // Header file
            XSSFRow row = sheet.getRow(1);
            XSSFCell cell = row.getCell(0);
            cell.setCellValue(header);
            
            // Footer file
            row = sheet.getRow(16);
            cell = row.getCell(26);
            String date = DateTimeUtils.getCurrentHumanReadableDate();
            date = date.substring(date.indexOf(" ")+1);
            cell.setCellValue("Malang, "+date);
            
            // Content
            for (int i=0; i<5; i++) {
                row =sheet.getRow(i+9);
                for (int j=0; j<30; j++) {
                    int k = j+1;
                    if (k%3 == 0) continue;
                    
                    cell = row.getCell(j+3);
                    cell.setCellValue(data[i][j]);
                }
            }
            
            // Closing
            XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
            try (FileOutputStream out = new FileOutputStream(file)) {
                workbook.write(out);
            }
            
            return AppConfig.SUCCESS_CODE;
        } catch (IOException ex) {
            return AppConfig.ERROR_CODE;            
        }
    }
    
    public static int exportSdidtkReport(
            File file,
            String source,
            String month,
            String year,
            int[][] data
    ) {
        try {
            copyFile(file, System.getProperty("user.dir")
                +File.separator+source);
            
            // Preparation
            FileInputStream in = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(in);
            XSSFSheet sheet = workbook.getSheetAt(0);
            
            // Header file
            XSSFRow row = sheet.getRow(3);
            XSSFCell cell = row.getCell(2);
            cell.setCellValue(": "+month);
            
            row = sheet.getRow(4);
            cell = row.getCell(2);
            cell.setCellValue(": "+year);
            
            // Content
            for (int i=0; i<4; i++) {
                row =sheet.getRow(i+29);
                int k = 6;
                for (int j=0; j<22; j++) {
                    cell = row.getCell(k);
                    cell.setCellValue(data[i][j]);
                    
                    if (k %2 != 0) k+=3;
                    else k++;
                }
            }
            
            // Closing
            XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
            try (FileOutputStream out = new FileOutputStream(file)) {
                workbook.write(out);
            }
            
            return AppConfig.SUCCESS_CODE;
        } catch (IOException ex) {
            return AppConfig.ERROR_CODE;            
        }
    }
}
