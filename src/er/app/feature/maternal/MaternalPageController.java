/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.maternal;

import er.app.config.AppConfig;
import er.app.data.manager.MaternalDataManager;
import er.app.data.manager.PasienDataManager;
import er.app.data.model.Maternal;
import er.app.data.model.Pasien;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.controlsfx.control.CheckComboBox;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class MaternalPageController implements Initializable {

    @FXML private VBox root;
    
    // Formulir Views
    @FXML private TitledPane containerForms;
    @FXML private DatePicker datePickerTanggal;
    @FXML private Label labelKelurahan;
    @FXML private TextField formNomorRM;
    @FXML private TextField formNamaPasien;
    @FXML private TextField formKeluhan;
    @FXML private TextField formHamilKe;
    @FXML private TextField formRiwayat;
    @FXML private TextField formJarakAnak;
    @FXML private TextField spinnerBeratBadan;
    @FXML private TextField spinnerTinggiBadan;
    @FXML private TextField spinnerTekananDarah1;
    @FXML private TextField spinnerTekananDarah2;
    @FXML private TextField spinnerLingkarLengan;
    @FXML private TextField spinnerHemoglobin;
    @FXML private ComboBox comboGolDarah;
    @FXML private ChoiceBox comboAlbumin;
    @FXML private TextField spinnerReduksi;
    @FXML private TextField spinnerTinggiFundus;
    @FXML private TextField spinnerDenyutJantung;
    @FXML private ToggleGroup groupPitc;
    @FXML private RadioButton radioReaktifPitc;
    @FXML private RadioButton radioNonPitc;
    @FXML private ToggleGroup groupHbsAg;
    @FXML private RadioButton radioReaktifHbsAg;
    @FXML private RadioButton radioNonHbsAg;
    @FXML private ToggleGroup groupTpha;
    @FXML private RadioButton radioReaktifTpha;
    @FXML private RadioButton radioNonTpha;
    @FXML private DatePicker datePickerHpht;
    @FXML private DatePicker datePickerTaksiran;
    @FXML private ComboBox comboLetakJanin;
    @FXML private TextField formSpr;
    @FXML private TextField formDiagnosa;
    @FXML private TextField formTerapi;
    @FXML private CheckComboBox checkComboMaternal;
    @FXML private Button buttonSearchNomorRM;
    @FXML private Button buttonSearchNamaPasien;
    @FXML private Button buttonTambah;
    @FXML private Button buttonUpdate;
    @FXML private Button buttonDelete;
    @FXML private Button buttonSimpan;
    @FXML private Button buttonBatal;
    
    // Table Views
    @FXML private TableView<Maternal> tableMaternal;
    @FXML private TableColumn<Maternal, String> colTanggal;
    @FXML private TableColumn<Maternal, String> colKelurahan;
    @FXML private TableColumn<Maternal, String> colNomorRM;
    @FXML private TableColumn<Maternal, String> colNamaPasien;
    @FXML private TableColumn<Maternal, String> colKeluhan;
    @FXML private TableColumn<Maternal, String> colRiwayat;
    @FXML private TableColumn<Maternal, Number> colBeratBadan;
    @FXML private TableColumn<Maternal, Number> colTinggiBadan;
    @FXML private TableColumn<Maternal, String> colTekananDarah;
    @FXML private TableColumn<Maternal, Number> colLingkarLengan;
    @FXML private TableColumn<Maternal, Number> colHemoglobin;
    @FXML private TableColumn<Maternal, String> colGolDarah;
    @FXML private TableColumn<Maternal, String> colAlbumin;
    @FXML private TableColumn<Maternal, Number> colReduksi;
    @FXML private TableColumn<Maternal, Number> colTinggiFundus;
    @FXML private TableColumn<Maternal, Number> colDenyutJantung;
    @FXML private TableColumn<Maternal, String> colDiagnosa;
    @FXML private TableColumn<Maternal, String> colTerapi;
    @FXML private TableColumn<Maternal, String> colMaternal;
    @FXML private TextField formSearchTable;
    @FXML private Button buttonSearchTable;
    @FXML private Button buttonRefreshTable;
        
    private PasienDataManager pasienManager;    
    private MaternalDataManager maternalManager;
    private Maternal selectedMaternal;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pasienManager = PasienDataManager.getInstance();
        maternalManager = MaternalDataManager.getInstance();
        selectedMaternal = null;
        
        initForms();
        initTables();
    }     
    
    public void onPageShown() {
        System.out.println("Maternal Controller shown");
    }
    
    private void initForms() {
        // DatePicker tanggal
        datePickerTanggal.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        // DatePicker hpht
        datePickerHpht.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        // DatePicker taksiran
        datePickerTaksiran.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        
        // Form berat badan
        spinnerBeratBadan.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerBeratBadan.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form tinggi badan
        spinnerTinggiBadan.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerTinggiBadan.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form tekanan darah
        spinnerTekananDarah1.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerTekananDarah1.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        spinnerTekananDarah2.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerTekananDarah2.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form lingkar lengan atas
//        spinnerLingkarLengan.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, 
//                String newValue) {
//                if (!newValue.matches("[-+]?[0-9]*\\.?[0-9]+")) {
//                    spinnerLingkarLengan.setText(newValue.replaceAll("[-+]?[0-9]*\\.?[0-9]+", ""));
//                }
//            }
//        });
        
        // Form hemoglobin
//        spinnerHemoglobin.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, 
//                String newValue) {
//                if (!newValue.matches("[-+]?[0-9]*\\.?[0-9]+")) {
//                    spinnerHemoglobin.setText(newValue.replaceAll("[-+]?[0-9]*\\.?[0-9]+", ""));
//                }
//            }
//        });
        
        // Form golongan darah
        ObservableList<String> listGolonganDarah = 
                FXCollections.observableArrayList(
                        "A", 
                        "B", 
                        "AB", 
                        "O"
                );
        comboGolDarah.setItems(listGolonganDarah);
        comboGolDarah.getSelectionModel().selectFirst();
        
        // Form albumin
        ObservableList<String> listAlbumin = 
                FXCollections.observableArrayList(
                        "Positif", 
                        "Negatif"
                );
        comboAlbumin.setItems(listAlbumin);
        comboAlbumin.getSelectionModel().selectFirst();
        comboAlbumin.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (!buttonSimpan.isDisabled()) {
                spinnerReduksi.setText("0");
                spinnerReduksi.setDisable(comboAlbumin.getSelectionModel().isSelected(1));
            }
        });
        
        // Form reduksi
//        spinnerReduksi.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, 
//                String newValue) {
//                if (!newValue.matches("[-+]?[0-9]*\\.?[0-9]+")) {
//                    spinnerReduksi.setText(newValue.replaceAll("[^\\d]", ""));
//                }
//            }
//        });
        
        // Form tinggi fundus
        spinnerTinggiFundus.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerTinggiFundus.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form denyut jantung janin
        spinnerDenyutJantung.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerDenyutJantung.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form letak janin
        ObservableList<String> listLetakJanin = 
                FXCollections.observableArrayList(
                        "Sungsang", 
                        "Kepala", 
                        "Lintang", 
                        "Ballottement (+)", 
                        "Oblique"
                );
        comboLetakJanin.setItems(listLetakJanin);
        comboLetakJanin.getSelectionModel().selectFirst();
        
        // Form Kasus Maternal
        ObservableList<String> listKasus = 
                FXCollections.observableArrayList(
                        "Hiperemesis", 
                        "Abortus", 
                        "Pre-eklampsia / Eklampsia", 
                        "Perdarahan Kehamilan", 
                        "Perdarahan Persalinan", 
                        "Perdarahan Nifas", 
                        "Partus Lama", 
                        "Infeksi", 
                        "AIDS", 
                        "TB", 
                        "Malaria", 
                        "Kasus Lain"
                );
        checkComboMaternal.getItems().clear();
        checkComboMaternal.getItems().addAll(listKasus);
    }
    private void initTables() {
        colTanggal.setCellValueFactory(value -> value.getValue().tanggalKunjunganProperty());
        colKelurahan.setCellValueFactory(value -> value.getValue().kelurahanProperty());
        colNomorRM.setCellValueFactory(value -> value.getValue().nomorRMProperty());
        colNamaPasien.setCellValueFactory(value -> value.getValue().namaPasienProperty());
        colKeluhan.setCellValueFactory(value -> value.getValue().keluhanProperty());
        colRiwayat.setCellValueFactory(value -> value.getValue().riwayatProperty());
        colBeratBadan.setCellValueFactory(value -> value.getValue().beratBadanProperty());
        colTinggiBadan.setCellValueFactory(value -> value.getValue().tinggiBadanProperty());
        colTekananDarah.setCellValueFactory(value -> value.getValue().tekananDarahProperty());
        colLingkarLengan.setCellValueFactory(value -> value.getValue().lingkarLenganProperty());
        colHemoglobin.setCellValueFactory(value -> value.getValue().hemoglobinProperty());
        colGolDarah.setCellValueFactory(value -> value.getValue().golDarahProperty());
        colAlbumin.setCellValueFactory(value -> value.getValue().albuminProperty());
        colReduksi.setCellValueFactory(value -> value.getValue().reduksiProperty());
        colTinggiFundus.setCellValueFactory(value -> value.getValue().tinggiFundusProperty());
        colDenyutJantung.setCellValueFactory(value -> value.getValue().denyutJantungProperty());
        colDiagnosa.setCellValueFactory(value -> value.getValue().diagnosaProperty());
        colTerapi.setCellValueFactory(value -> value.getValue().terapiProperty());
        colMaternal.setCellValueFactory(value -> value.getValue().maternalProperty());
        
        tableMaternal.setItems(maternalManager.getMaternalList());
        tableMaternal.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Maternal> observable, 
                        Maternal oldValue, 
                        Maternal newValue) -> {
            selectedMaternal = newValue == null? oldValue : newValue;
            fetchSelectedMaternal();
            buttonTambah.setDisable(false);
            buttonBatal.setDisable(false);
            formNomorRM.setDisable(true);
            buttonSearchNomorRM.setDisable(true);
            formNamaPasien.setDisable(true);
            buttonSearchNamaPasien.setDisable(true);
            containerForms.setExpanded(true);
        });
    }
    
    private void fetchSelectedPasien(Pasien pasien) {
        buttonTambah.setDisable(pasien == null);
        buttonBatal.setDisable(pasien == null);
        formNomorRM.setDisable(pasien != null);
        buttonSearchNomorRM.setDisable(pasien != null);
        formNamaPasien.setDisable(pasien != null);
        buttonSearchNamaPasien.setDisable(pasien != null);
        
        if (pasien != null) {
            labelKelurahan.setText("Kelurahan "+pasien.getKelurahan());
            formNomorRM.setText(pasien.getNomorRM());
            formNamaPasien.setText(pasien.getNamaLengkap());
            buttonTambah.requestFocus();
        } else {
            labelKelurahan.setText("Kelurahan -");
            formNomorRM.clear();
            formNamaPasien.clear();
            formNomorRM.requestFocus();
        }            
    }
    private void fetchSelectedMaternal() {
        if (!buttonSimpan.isDisabled()) return;
        
        if (selectedMaternal == null) {
            datePickerTanggal.getEditor().clear();
            formKeluhan.clear();
            formHamilKe.clear();
            formRiwayat.clear();
            formJarakAnak.clear();
            datePickerHpht.getEditor().clear();
            datePickerTaksiran.getEditor().clear();
            formSpr.clear();
            formDiagnosa.clear();
            formTerapi.clear();
            checkComboMaternal.getCheckModel().clearChecks();
        } else {
            datePickerTanggal.setValue(DateTimeUtils.parseDateOnly(selectedMaternal.getTanggalKunjungan()));
            labelKelurahan.setText("Kelurahan "+selectedMaternal.getKelurahan());
            formNomorRM.setText(selectedMaternal.getNomorRM());
            formNamaPasien.setText(selectedMaternal.getNamaPasien());
            formKeluhan.setText(selectedMaternal.getKeluhan());
            formHamilKe.setText(selectedMaternal.getHamilKe());
            formRiwayat.setText(selectedMaternal.getRiwayat());
            formJarakAnak.setText(selectedMaternal.getJarakAnak());
            spinnerBeratBadan.setText(selectedMaternal.getBeratBadan()+"");
            spinnerTinggiBadan.setText(selectedMaternal.getTinggiBadan()+"");
            String darah[] = selectedMaternal.getTekananDarah().split("/");
            spinnerTekananDarah1.setText(Integer.parseInt(darah[0])+"");
            spinnerTekananDarah2.setText(Integer.parseInt(darah[1])+"");
            spinnerLingkarLengan.setText(selectedMaternal.getLingkarLengan()+"");
            spinnerHemoglobin.setText(selectedMaternal.getHemoglobin()+"");
            comboGolDarah.getSelectionModel().select(selectedMaternal.getGolDarah());
            comboAlbumin.getSelectionModel().select(selectedMaternal.getAlbumin());
            spinnerReduksi.setText(selectedMaternal.getReduksi()+"");
            spinnerTinggiFundus.setText(selectedMaternal.getTinggiFundus()+"");
            spinnerDenyutJantung.setText(selectedMaternal.getDenyutJantung()+"");
            groupPitc.selectToggle(
                    selectedMaternal.getPitc().equals("Reaktif")?
                            radioReaktifPitc : radioNonPitc
            );
            groupHbsAg.selectToggle(
                    selectedMaternal.getHbsAg().equals("Reaktif")?
                            radioReaktifHbsAg : radioNonHbsAg
            );
            groupTpha.selectToggle(
                    selectedMaternal.getTpha().equals("Reaktif")?
                            radioReaktifTpha : radioNonTpha
            );
            datePickerHpht.setValue(DateTimeUtils.parseDateOnly(selectedMaternal.getHpht()));
            datePickerTaksiran.setValue(DateTimeUtils.parseDateOnly(selectedMaternal.getTaksiran()));
            comboLetakJanin.getSelectionModel().select(selectedMaternal.getLetakJanin());
            formSpr.setText(selectedMaternal.getSpr());
            formDiagnosa.setText(selectedMaternal.getDiagnosa());
            formTerapi.setText(selectedMaternal.getTerapi());
            
            String temp = selectedMaternal.getMaternal();
            temp = temp.substring(1, temp.length()-1);
            String kasus[] = temp.split(", ");
            checkComboMaternal.getCheckModel().clearChecks();
            for (String k : kasus) checkComboMaternal.getCheckModel().check(k);            
        }
        
        buttonUpdate.setDisable(selectedMaternal == null);
        buttonDelete.setDisable(selectedMaternal == null);
    }
    private void disableForms(boolean isDisabled) {
        datePickerTanggal.setDisable(isDisabled);
        formKeluhan.setDisable(isDisabled);
        formHamilKe.setDisable(isDisabled);
        formRiwayat.setDisable(isDisabled);
        formJarakAnak.setDisable(isDisabled);
        spinnerBeratBadan.setDisable(isDisabled);
        spinnerTinggiBadan.setDisable(isDisabled);
        spinnerTekananDarah1.setDisable(isDisabled);
        spinnerTekananDarah2.setDisable(isDisabled);
        spinnerLingkarLengan.setDisable(isDisabled);
        spinnerHemoglobin.setDisable(isDisabled);
        comboGolDarah.setDisable(isDisabled);
        comboAlbumin.setDisable(isDisabled);
        spinnerReduksi.setDisable(isDisabled);
        spinnerTinggiFundus.setDisable(isDisabled);
        spinnerDenyutJantung.setDisable(isDisabled);
        radioReaktifPitc.setDisable(isDisabled);
        radioNonPitc.setDisable(isDisabled);
        radioReaktifHbsAg.setDisable(isDisabled);
        radioNonHbsAg.setDisable(isDisabled);
        radioReaktifTpha.setDisable(isDisabled);
        radioNonTpha.setDisable(isDisabled);
        datePickerHpht.setDisable(isDisabled);
        datePickerTaksiran.setDisable(isDisabled);
        comboLetakJanin.setDisable(isDisabled);
        formSpr.setDisable(isDisabled);
        formDiagnosa.setDisable(isDisabled);
        formTerapi.setDisable(isDisabled);
        checkComboMaternal.setDisable(isDisabled);
    }
    
    @FXML private void searchPasienByNomorRM() {
        Pasien pasien = pasienManager.searchPasienByNomorRM(formNomorRM.getText().trim());
        if (pasien == null) {            
            DialogUtils.showError("Data pasien tidak ditemukan.");
            labelKelurahan.setText("Kelurahan -");
        } else fetchSelectedPasien(pasien);
    }
    @FXML private void searchPasienByNamaLengkap() {
        Pasien pasien = pasienManager.searchPasienByNamaLengkap(formNamaPasien.getText().trim());
        if (pasien == null) {            
            DialogUtils.showError("Data pasien tidak ditemukan.");
            labelKelurahan.setText("Kelurahan -");
        } else fetchSelectedPasien(pasien);
    }
    @FXML private void onButtonTambahClicked() {
        selectedMaternal = null;
        fetchSelectedMaternal();
        disableForms(false);
        buttonTambah.setDisable(true);        
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);        
        buttonSimpan.setDisable(false);    
        buttonBatal.setDisable(false); 
    }
    @FXML private void onButtonUpdateClicked() {
        disableForms(false);
        buttonTambah.setDisable(true);        
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);        
        buttonSimpan.setDisable(false);    
        buttonBatal.setDisable(false); 
    }
    @FXML private void saveMaternal() {
        if (formNomorRM.getText().trim().isEmpty() || 
                formNamaPasien.getText().trim().isEmpty()) {
            DialogUtils.showError("Data nomor RM dan nama pasien harus diisi.");
            disableForms(true);
            cancelForms();
            return;
        }
        
        root.setDisable(true);
        try {
            if (selectedMaternal == null) {
                // insert new maternal
                if (datePickerTanggal.getValue() == null) {
                    DialogUtils.showError("Data tidak boleh kosong.");
                    root.setDisable(false);
                    return;
                }

                // Collect data maternal
                Maternal maternal = new Maternal();
                maternal.setNomorRM(formNomorRM.getText().trim());
                maternal.setNamaPasien(formNamaPasien.getText().trim());
                maternal.setTanggalKunjungan(datePickerTanggal.getValue().toString());
                maternal.setKelurahan(labelKelurahan.getText().substring(10));
                maternal.setKeluhan(formKeluhan.getText().trim());
                maternal.setHamilKe(formHamilKe.getText().trim());
                maternal.setRiwayat(formRiwayat.getText().trim());
                maternal.setJarakAnak(formJarakAnak.getText().trim());
                maternal.setBeratBadan(Integer.parseInt(spinnerBeratBadan.getText().trim()));
                maternal.setTinggiBadan(Integer.parseInt(spinnerTinggiBadan.getText().trim()));
                maternal.setTekananDarah(spinnerTekananDarah1.getText().trim() + "/" + spinnerTekananDarah2.getText().trim());
                maternal.setLingkarLengan(Double.parseDouble(spinnerLingkarLengan.getText().trim()));
                maternal.setHemoglobin(Double.parseDouble(spinnerHemoglobin.getText().trim()));
                maternal.setGolDarah(comboGolDarah.getSelectionModel().getSelectedItem().toString());
                maternal.setAlbumin(comboAlbumin.getSelectionModel().getSelectedItem().toString());
                maternal.setReduksi(Double.parseDouble(spinnerReduksi.getText().trim()));
                maternal.setTinggiFundus(Integer.parseInt(spinnerTinggiFundus.getText().trim()));
                maternal.setDenyutJantung(Integer.parseInt(spinnerDenyutJantung.getText().trim()));
                maternal.setPitc(
                        groupPitc.selectedToggleProperty()
                                .get() == radioReaktifPitc? "Reaktif" : "Non Reaktif"
                );
                maternal.setHbsAg(
                        groupHbsAg.selectedToggleProperty()
                                .get() == radioReaktifHbsAg? "Reaktif" : "Non Reaktif"
                );
                maternal.setTpha(
                        groupTpha.selectedToggleProperty()
                                .get() == radioReaktifTpha? "Reaktif" : "Non Reaktif"
                );
                maternal.setHpht(datePickerHpht.getValue().toString());
                maternal.setTaksiran(datePickerTaksiran.getValue().toString());
                maternal.setLetakJanin(comboLetakJanin.getSelectionModel().getSelectedItem().toString());
                maternal.setSpr(formSpr.getText().trim());
                maternal.setDiagnosa(formDiagnosa.getText().trim());
                maternal.setTerapi(formTerapi.getText().trim());
                maternal.setMaternal(checkComboMaternal.getCheckModel().getCheckedItems().toString());

                // insert and check result
                if (maternalManager.addMaternal(maternal) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil menambahkan data maternal baru.");
                    selectedMaternal = maternal;
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(false);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal menambahkan data maternal baru, silahkan coba lagi.");
                }
            } else {
                // update selected maternal
                if (datePickerTanggal.getValue() == null) {
                    DialogUtils.showError("Data tidak boleh kosong.");
                    root.setDisable(false);
                    return;
                }

                // Collect data maternal
                selectedMaternal.setTanggalKunjungan(datePickerTanggal.getValue().toString());
                selectedMaternal.setKeluhan(formKeluhan.getText().trim());
                selectedMaternal.setHamilKe(formHamilKe.getText().trim());
                selectedMaternal.setRiwayat(formRiwayat.getText().trim());
                selectedMaternal.setJarakAnak(formJarakAnak.getText().trim());
                selectedMaternal.setBeratBadan(Integer.parseInt(spinnerBeratBadan.getText().trim()));
                selectedMaternal.setTinggiBadan(Integer.parseInt(spinnerTinggiBadan.getText().trim()));
                selectedMaternal.setTekananDarah(spinnerTekananDarah1.getText().trim() + "/" + spinnerTekananDarah2.getText().trim());
                selectedMaternal.setLingkarLengan(Double.parseDouble(spinnerLingkarLengan.getText().trim()));
                selectedMaternal.setHemoglobin(Double.parseDouble(spinnerHemoglobin.getText().trim()));
                selectedMaternal.setGolDarah(comboGolDarah.getSelectionModel().getSelectedItem().toString());
                selectedMaternal.setAlbumin(comboAlbumin.getSelectionModel().getSelectedItem().toString());
                selectedMaternal.setReduksi(Double.parseDouble(spinnerReduksi.getText().trim()));
                selectedMaternal.setTinggiFundus(Integer.parseInt(spinnerTinggiFundus.getText().trim()));
                selectedMaternal.setDenyutJantung(Integer.parseInt(spinnerDenyutJantung.getText().trim()));
                selectedMaternal.setPitc(
                        groupPitc.selectedToggleProperty()
                                .get() == radioReaktifPitc? "Reaktif" : "Non Reaktif"
                );
                selectedMaternal.setHbsAg(
                        groupHbsAg.selectedToggleProperty()
                                .get() == radioReaktifHbsAg? "Reaktif" : "Non Reaktif"
                );
                selectedMaternal.setTpha(
                        groupTpha.selectedToggleProperty()
                                .get() == radioReaktifTpha? "Reaktif" : "Non Reaktif"
                );
                selectedMaternal.setHpht(datePickerHpht.getValue().toString());
                selectedMaternal.setTaksiran(datePickerTaksiran.getValue().toString());
                selectedMaternal.setLetakJanin(comboLetakJanin.getSelectionModel().getSelectedItem().toString());
                selectedMaternal.setSpr(formSpr.getText().trim());
                selectedMaternal.setDiagnosa(formDiagnosa.getText().trim());
                selectedMaternal.setTerapi(formTerapi.getText().trim());
                selectedMaternal.setMaternal(checkComboMaternal.getCheckModel().getCheckedItems().toString());

                // update and check result
                if (maternalManager.updateMaternal(selectedMaternal) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil merubah data maternal lama.");
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(false);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal merubah data maternal lama, silahkan coba lagi.");
                }
            }
        } catch (NumberFormatException ex) {
            DialogUtils.showError("Format data yang diinputkan salah.\n"
                    + "Gunakan titik ( . ) sebagai pengganti koma ( , ) untuk data bilangan desimal.");
        } catch (Exception ex) {
            DialogUtils.showError("Gagal memproses data maternal, silahkan coba lagi.");
        }
        root.setDisable(false);
    }
    @FXML private void deleteMaternal() {
        if (selectedMaternal == null) DialogUtils.showInfo("Silahkan pilih salah satu data Maternal terlebih dahulu di tabel.");
        else if (DialogUtils.showConfirm("Yakin akan menghapus data maternal dari "
                +selectedMaternal.getNamaPasien()+" \npada tanggal "
                +selectedMaternal.getTanggalKunjungan()+" ?") == DialogUtils.RESULT_OK
        ) {
            root.setDisable(true);
            String nama = selectedMaternal.getNamaPasien(),
                    tanggal = selectedMaternal.getTanggalKunjungan();
            if (maternalManager.deleteMaternal(selectedMaternal) == AppConfig.SUCCESS_CODE) {
                DialogUtils.showInfo("Berhasil menghapus data maternal dari "
                        +nama+" \npada tanggal "+tanggal);
                if (tableMaternal.getItems().isEmpty()) {
                    selectedMaternal = null;
                    fetchSelectedMaternal();
                }
                disableForms(true);
                buttonSimpan.setDisable(true);
                buttonBatal.setDisable(false);
                buttonTambah.setDisable(false);
            } else {
                DialogUtils.showError("Gagal menghapus data maternal dari "
                        +nama+" \npada tanggal "+tanggal
                        +", silahkan coba lagi.");
            }
            root.setDisable(false);
        }
    }
    @FXML private void cancelForms() {
        if (datePickerTanggal.isDisabled()) {  
            tableMaternal.getSelectionModel().clearSelection();
            fetchSelectedPasien(null);
            selectedMaternal = null;
            fetchSelectedMaternal();
            disableForms(true);
        } else {            
            if (DialogUtils.showConfirm("Batalkan semua perubahan?")
                    == DialogUtils.RESULT_OK) {
                buttonSimpan.setDisable(true);
                buttonBatal.setDisable(false);
                buttonTambah.setDisable(false);
                fetchSelectedMaternal();
                disableForms(true);
            }
        }
    }
    @FXML private void filterTable() {
        tableMaternal.setItems(maternalManager.filterMaternalList(formSearchTable.getText()));
    }
    @FXML private void refreshTable() {
        formSearchTable.clear();
        tableMaternal.setItems(maternalManager.getMaternalList(true));
    }
    
    private void handleSpin(Spinner spinner, Number oldValue, Object newValue) {
        try {
            if (newValue == null) {
                spinner.getValueFactory().setValue((int)oldValue);
            } else {
                spinner.getValueFactory().setValue((int)newValue);
            }
        } catch (Exception e) {
            System.out.println("spinner exception");
            spinner.getValueFactory().setValue(0);
        }
    }
}
