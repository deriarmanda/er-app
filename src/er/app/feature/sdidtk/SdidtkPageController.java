/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.sdidtk;

import er.app.config.AppConfig;
import er.app.data.manager.PasienDataManager;
import er.app.data.manager.SdidtkDataManager;
import er.app.data.model.Pasien;
import er.app.data.model.Sdidtk;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class SdidtkPageController implements Initializable {

    @FXML private VBox root;
    
    // Formulir Views
    @FXML private TitledPane containerForms;
    @FXML private DatePicker datePickerTanggal;
    @FXML private Label labelKelurahan;
    @FXML private TextField formNomorRM;
    @FXML private TextField formNamaAnak;
    @FXML private TextField formJenisKelamin;
    @FXML private TextField formNamaIbu;
    @FXML private TextField formNamaAyah;
    @FXML private TextField spinnerUsiaAnak;
    @FXML private TextField spinnerBeratBadan;
    @FXML private TextField spinnerTinggiBadan;
    @FXML private TextField spinnerLingkarKepala;
    @FXML private ComboBox comboDiagnosa;
    @FXML private ToggleGroup groupLka;
    @FXML private RadioButton radioLkaNormal;
    @FXML private RadioButton radioLkaTidakNormal;
    @FXML private ToggleGroup groupKasar;
    @FXML private RadioButton radioKasarNormal;
    @FXML private RadioButton radioKasarTidakNormal;
    @FXML private ToggleGroup groupHalus;
    @FXML private RadioButton radioHalusNormal;
    @FXML private RadioButton radioHalusTidakNormal;
    @FXML private ToggleGroup groupBicara;
    @FXML private RadioButton radioBicaraNormal;
    @FXML private RadioButton radioBicaraTidakNormal;
    @FXML private ToggleGroup groupSosial;
    @FXML private RadioButton radioSosialNormal;
    @FXML private RadioButton radioSosialTidakNormal;
    @FXML private ToggleGroup groupTdl;
    @FXML private RadioButton radioTdlNormal;
    @FXML private RadioButton radioTdlTidakNormal;
    @FXML private ToggleGroup groupTdd;
    @FXML private RadioButton radioTddNormal;
    @FXML private RadioButton radioTddTidakNormal;
    @FXML private ToggleGroup groupMme;
    @FXML private RadioButton radioMmeNormal;
    @FXML private RadioButton radioMmeTidakNormal;
    @FXML private ToggleGroup groupDirujuk;
    @FXML private RadioButton radioDirujukYa;
    @FXML private RadioButton radioDirujukTidak;
    @FXML private Button buttonSearchNomorRM;
    @FXML private Button buttonSearchNamaAnak;
    @FXML private Button buttonTambah;
    @FXML private Button buttonUpdate;
    @FXML private Button buttonDelete;
    @FXML private Button buttonSimpan;
    @FXML private Button buttonBatal;
    
    // Table Views
    @FXML private TableView<Sdidtk> tableSdidtk;
    @FXML private TableColumn<Sdidtk, String> colTanggal;
    @FXML private TableColumn<Sdidtk, String> colKelurahan;
    @FXML private TableColumn<Sdidtk, String> colNomorRM;
    @FXML private TableColumn<Sdidtk, String> colNamaAnak;
    @FXML private TableColumn<Sdidtk, String> colNamaIbu;
    @FXML private TableColumn<Sdidtk, String> colNamaAyah;
    @FXML private TableColumn<Sdidtk, Number> colUsiaAnak;
    @FXML private TableColumn<Sdidtk, Number> colBeratBadan;
    @FXML private TableColumn<Sdidtk, Number> colTinggiBadan;
    @FXML private TableColumn<Sdidtk, Number> colLingkarKepala;
    @FXML private TableColumn<Sdidtk, String> colLka;
    @FXML private TableColumn<Sdidtk, String> colKasar;
    @FXML private TableColumn<Sdidtk, String> colHalus;
    @FXML private TableColumn<Sdidtk, String> colSosial;
    @FXML private TableColumn<Sdidtk, String> colBicara;
    @FXML private TableColumn<Sdidtk, String> colTdl;
    @FXML private TableColumn<Sdidtk, String> colTdd;
    @FXML private TableColumn<Sdidtk, String> colMme;
    @FXML private TableColumn<Sdidtk, String> colDirujuk;
    @FXML private TextField formSearchTable;
    @FXML private Button buttonSearchTable;
    @FXML private Button buttonRefreshTable;
    
    private PasienDataManager pasienManager;
    private SdidtkDataManager sdidtkManager;
    private Sdidtk selectedSdidtk;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pasienManager = PasienDataManager.getInstance();
        sdidtkManager = SdidtkDataManager.getInstance();
        selectedSdidtk = null;
        
        initForms();
        initTables();
    }  
    
    public void onPageShown() {
        
    }
    
    private void initForms() {
        // DatePicker tanggal
        datePickerTanggal.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        
        spinnerUsiaAnak.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerUsiaAnak.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        spinnerBeratBadan.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerBeratBadan.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        spinnerTinggiBadan.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerTinggiBadan.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        spinnerLingkarKepala.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerLingkarKepala.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form Diagnosa
        ObservableList<String> listDiagnosa = 
                FXCollections.observableArrayList(
                        "Sesuai", 
                        "Meragukan", 
                        "Penyimpangan"
                );
        comboDiagnosa.setItems(listDiagnosa);
        comboDiagnosa.getSelectionModel().selectFirst();
    }
    private void initTables() {
        colTanggal.setCellValueFactory(value -> value.getValue().tanggalProperty());
        colKelurahan.setCellValueFactory(value -> value.getValue().kelurahanProperty());
        colNomorRM.setCellValueFactory(value -> value.getValue().nomorRMProperty());
        colNamaAnak.setCellValueFactory(value -> value.getValue().namaAnakProperty());
        colNamaIbu.setCellValueFactory(value -> value.getValue().namaIbuProperty());
        colNamaAyah.setCellValueFactory(value -> value.getValue().namaAyahProperty());
        colUsiaAnak.setCellValueFactory(value -> value.getValue().usiaAnakProperty());
        colBeratBadan.setCellValueFactory(value -> value.getValue().beratBadanProperty());
        colTinggiBadan.setCellValueFactory(value -> value.getValue().tinggiBadanProperty());
        colLingkarKepala.setCellValueFactory(value -> value.getValue().lingkarKepalaProperty());
        colLka.setCellValueFactory(value -> value.getValue().gangguanLkaProperty());
        colKasar.setCellValueFactory(value -> value.getValue().motorikKasarProperty());
        colHalus.setCellValueFactory(value -> value.getValue().motorikHalusProperty());
        colSosial.setCellValueFactory(value -> value.getValue().sosialisasiProperty());
        colBicara.setCellValueFactory(value -> value.getValue().bicaraBahasaProperty());
        colTdl.setCellValueFactory(value -> value.getValue().gangguanTdlProperty());
        colTdd.setCellValueFactory(value -> value.getValue().gangguanTddProperty());
        colMme.setCellValueFactory(value -> value.getValue().mmeProperty());
        colDirujuk.setCellValueFactory(value -> value.getValue().perluDirujukProperty());
        
        tableSdidtk.setItems(sdidtkManager.getSdidtkList());
        tableSdidtk.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Sdidtk> observable, 
                        Sdidtk oldValue, 
                        Sdidtk newValue) -> {
            selectedSdidtk = (newValue == null? oldValue : newValue);
            fetchSelectedSdidtk();
            buttonTambah.setDisable(false);
            buttonBatal.setDisable(false);
            formNomorRM.setDisable(true);
            buttonSearchNomorRM.setDisable(true);
            formNamaAnak.setDisable(true);
            buttonSearchNamaAnak.setDisable(true);
            containerForms.setExpanded(true);
        });
    }
    
    private void fetchSelectedPasien(Pasien pasien) {
        buttonTambah.setDisable(pasien == null);
        buttonBatal.setDisable(pasien == null);
        formNomorRM.setDisable(pasien != null);
        buttonSearchNomorRM.setDisable(pasien != null);
        formNamaAnak.setDisable(pasien != null);
        buttonSearchNamaAnak.setDisable(pasien != null);
        
        if (pasien != null) {
            labelKelurahan.setText("Kelurahan "+pasien.getKelurahan());
            formNomorRM.setText(pasien.getNomorRM());
            formNamaAnak.setText(pasien.getNamaLengkap());
            formJenisKelamin.setText(pasien.getJenisKelamin());
            buttonTambah.requestFocus();
        } else {
            labelKelurahan.setText("Kelurahan -");
            formNomorRM.clear();
            formNamaAnak.clear();
            formJenisKelamin.clear();
            formNomorRM.requestFocus();
        }        
    }
    private void fetchSelectedSdidtk() {
        if (!buttonSimpan.isDisabled()) return;
        
        if (selectedSdidtk == null) {
            datePickerTanggal.setValue(null);
            formNamaIbu.clear();
            formNamaAyah.clear();
        } else {
            datePickerTanggal.setValue(DateTimeUtils.parseDateOnly(selectedSdidtk.getTanggal()));
            labelKelurahan.setText("Kelurahan "+selectedSdidtk.getKelurahan());
            formNomorRM.setText(selectedSdidtk.getNomorRM());
            formNamaAnak.setText(selectedSdidtk.getNamaAnak());
            formJenisKelamin.setText(selectedSdidtk.getJenisKelamin());
            formNamaIbu.setText(selectedSdidtk.getNamaIbu());
            formNamaAyah.setText(selectedSdidtk.getNamaAyah());
            spinnerUsiaAnak.setText(selectedSdidtk.getUsiaAnak()+"");
            spinnerBeratBadan.setText(selectedSdidtk.getBeratBadan()+"");
            spinnerTinggiBadan.setText(selectedSdidtk.getTinggiBadan()+"");
            spinnerLingkarKepala.setText(selectedSdidtk.getLingkarKepala()+"");
            comboDiagnosa.getSelectionModel().select(selectedSdidtk.getDiagnosa());
            groupLka.selectToggle(
                    selectedSdidtk.getGangguanLka().equals("Normal")?
                            radioLkaNormal : radioLkaTidakNormal
            );            
            groupKasar.selectToggle(
                    selectedSdidtk.getMotorikKasar().equals("Normal")?
                            radioKasarNormal : radioKasarTidakNormal
            );            
            groupHalus.selectToggle(
                    selectedSdidtk.getMotorikHalus().equals("Normal")?
                            radioHalusNormal : radioHalusTidakNormal
            );            
            groupBicara.selectToggle(
                    selectedSdidtk.getBicaraBahasa().equals("Normal")?
                            radioBicaraNormal : radioBicaraTidakNormal
            );            
            groupSosial.selectToggle(
                    selectedSdidtk.getSosialisasi().equals("Normal")?
                            radioSosialNormal : radioSosialTidakNormal
            );            
            groupTdl.selectToggle(
                    selectedSdidtk.getGangguanTdl().equals("Normal")?
                            radioTdlNormal : radioTdlTidakNormal
            );            
            groupTdd.selectToggle(
                    selectedSdidtk.getGangguanTdd().equals("Normal")?
                            radioTddNormal : radioTddTidakNormal
            );            
            groupMme.selectToggle(
                    selectedSdidtk.getMme().equals("Normal")?
                            radioMmeNormal : radioMmeTidakNormal
            );            
            groupDirujuk.selectToggle(
                    selectedSdidtk.getPerluDirujuk().equals("Ya")?
                            radioDirujukYa : radioDirujukTidak
            );            
        }
        
        buttonUpdate.setDisable(selectedSdidtk == null);
        buttonDelete.setDisable(selectedSdidtk == null);
    }
    private void disableForms(boolean isDisabled) {
        datePickerTanggal.setDisable(isDisabled);
        formNamaIbu.setDisable(isDisabled);    
        formNamaAyah.setDisable(isDisabled);
        spinnerUsiaAnak.setDisable(isDisabled);    
        spinnerBeratBadan.setDisable(isDisabled);
        spinnerTinggiBadan.setDisable(isDisabled);    
        spinnerLingkarKepala.setDisable(isDisabled);
        comboDiagnosa.setDisable(isDisabled);
        radioLkaNormal.setDisable(isDisabled);    
        radioLkaTidakNormal.setDisable(isDisabled);
        radioKasarNormal.setDisable(isDisabled);    
        radioKasarTidakNormal.setDisable(isDisabled);
        radioHalusNormal.setDisable(isDisabled);    
        radioHalusTidakNormal.setDisable(isDisabled);
        radioBicaraNormal.setDisable(isDisabled);        
        radioBicaraTidakNormal.setDisable(isDisabled);
        radioSosialNormal.setDisable(isDisabled);    
        radioSosialTidakNormal.setDisable(isDisabled);
        radioTdlNormal.setDisable(isDisabled);    
        radioTdlTidakNormal.setDisable(isDisabled);
        radioTddNormal.setDisable(isDisabled);    
        radioTddTidakNormal.setDisable(isDisabled);
        radioMmeNormal.setDisable(isDisabled);    
        radioMmeTidakNormal.setDisable(isDisabled);    
        radioDirujukYa.setDisable(isDisabled);    
        radioDirujukTidak.setDisable(isDisabled);   
    }
    
    @FXML private void searchPasienByNomorRM() {
        Pasien pasien = pasienManager.searchPasienByNomorRM(formNomorRM.getText().trim());
        if (pasien == null) {            
            DialogUtils.showError("Data pasien tidak ditemukan.");
            labelKelurahan.setText("Kelurahan -");
            formJenisKelamin.clear();
        } else fetchSelectedPasien(pasien);
    }
    @FXML private void searchPasienByNamaLengkap() {
        Pasien pasien = pasienManager.searchPasienByNamaLengkap(formNamaAnak.getText().trim());
        if (pasien == null) {            
            DialogUtils.showError("Data pasien tidak ditemukan.");
            labelKelurahan.setText("Kelurahan -");
            formJenisKelamin.clear();
        } else fetchSelectedPasien(pasien);
    }
    @FXML private void onButtonTambahClicked() {
        selectedSdidtk = null;
        fetchSelectedSdidtk();
        disableForms(false);
        buttonTambah.setDisable(true);        
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);        
        buttonSimpan.setDisable(false);    
        buttonBatal.setDisable(false);
    }
    @FXML private void onButtonUpdateClicked() {
        disableForms(false);
        buttonTambah.setDisable(true);        
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);        
        buttonSimpan.setDisable(false);    
        buttonBatal.setDisable(false); 
    }
    @FXML private void saveSdidtk() {
        if (formNomorRM.getText().trim().isEmpty() || 
                formNamaAnak.getText().trim().isEmpty()) {
            DialogUtils.showError("Data nomor RM dan nama anak harus diisi.");
            disableForms(true);
            cancelForms();
            return;
        }
        
        root.setDisable(true);
        try {
            if (selectedSdidtk == null) {
                // insert new sdidtk
                if (datePickerTanggal.getValue() == null) {
                    DialogUtils.showError("Data tidak boleh kosong.");
                    root.setDisable(false);
                    return;
                }

                // Collect data sdidtk
                Sdidtk sdidtk = new Sdidtk();
                sdidtk.setNomorRM(formNomorRM.getText().trim());
                sdidtk.setNamaAnak(formNamaAnak.getText().trim());
                sdidtk.setTanggal(datePickerTanggal.getValue().toString());
                sdidtk.setKelurahan(labelKelurahan.getText().substring(10));
                sdidtk.setJenisKelamin(formJenisKelamin.getText().trim());
                sdidtk.setNamaIbu(formNamaIbu.getText().trim());
                sdidtk.setNamaAyah(formNamaAyah.getText().trim());
                sdidtk.setUsiaAnak(Integer.parseInt(spinnerUsiaAnak.getText().trim()));
                sdidtk.setBeratBadan(Integer.parseInt(spinnerBeratBadan.getText().trim()));
                sdidtk.setTinggiBadan(Integer.parseInt(spinnerTinggiBadan.getText().trim()));
                sdidtk.setLingkarKepala(Integer.parseInt(spinnerLingkarKepala.getText().trim()));
                sdidtk.setDiagnosa(comboDiagnosa.getSelectionModel().getSelectedItem().toString());
                sdidtk.setGangguanLka(
                        groupLka.selectedToggleProperty()
                                .get() == radioLkaNormal? "Normal" : "Tidak Normal");
                sdidtk.setMotorikKasar(
                        groupKasar.selectedToggleProperty()
                                .get() == radioKasarNormal? "Normal" : "Tidak Normal");
                sdidtk.setMotorikHalus(
                        groupHalus.selectedToggleProperty()
                                .get() == radioHalusNormal? "Normal" : "Tidak Normal");
                sdidtk.setBicaraBahasa(
                        groupBicara.selectedToggleProperty()
                                .get() == radioBicaraNormal? "Normal" : "Tidak Normal");
                sdidtk.setSosialisasi(
                        groupSosial.selectedToggleProperty()
                                .get() == radioSosialNormal? "Normal" : "Tidak Normal");
                sdidtk.setGangguanTdl(
                        groupTdl.selectedToggleProperty()
                                .get() == radioTdlNormal? "Normal" : "Tidak Normal");
                sdidtk.setGangguanTdd(
                        groupTdd.selectedToggleProperty()
                                .get() == radioTddNormal? "Normal" : "Tidak Normal");
                sdidtk.setMme(
                        groupMme.selectedToggleProperty()
                                .get() == radioMmeNormal? "Normal" : "Tidak Normal");
                sdidtk.setPerluDirujuk(
                        groupDirujuk.selectedToggleProperty()
                                .get() == radioDirujukYa? "Ya" : "Tidak");

                // insert and check result
                if (sdidtkManager.addSdidtk(sdidtk) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil menambahkan data SDIDTK baru.");
                    selectedSdidtk = sdidtk;
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(false);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal menambahkan data SDIDTK baru, silahkan coba lagi.");
                }
            } else {
                // update selected sdidtk
                if (datePickerTanggal.getValue() == null) {
                    DialogUtils.showError("Data tidak boleh kosong.");
                    root.setDisable(false);
                    return;
                }

                // Collect data sdidtk
                selectedSdidtk.setTanggal(datePickerTanggal.getValue().toString());
                selectedSdidtk.setNamaIbu(formNamaIbu.getText().trim());
                selectedSdidtk.setNamaAyah(formNamaAyah.getText().trim());
                selectedSdidtk.setUsiaAnak(Integer.parseInt(spinnerUsiaAnak.getText().trim()));
                selectedSdidtk.setBeratBadan(Integer.parseInt(spinnerBeratBadan.getText().trim()));
                selectedSdidtk.setTinggiBadan(Integer.parseInt(spinnerTinggiBadan.getText().trim()));
                selectedSdidtk.setLingkarKepala(Integer.parseInt(spinnerLingkarKepala.getText().trim()));
                selectedSdidtk.setDiagnosa(comboDiagnosa.getSelectionModel().getSelectedItem().toString());
                selectedSdidtk.setGangguanLka(
                        groupLka.selectedToggleProperty()
                                .get() == radioLkaNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setMotorikKasar(
                        groupKasar.selectedToggleProperty()
                                .get() == radioKasarNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setMotorikHalus(
                        groupHalus.selectedToggleProperty()
                                .get() == radioHalusNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setBicaraBahasa(
                        groupBicara.selectedToggleProperty()
                                .get() == radioBicaraNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setSosialisasi(
                        groupSosial.selectedToggleProperty()
                                .get() == radioSosialNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setGangguanTdl(
                        groupTdl.selectedToggleProperty()
                                .get() == radioTdlNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setGangguanTdd(
                        groupTdd.selectedToggleProperty()
                                .get() == radioTddNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setMme(
                        groupMme.selectedToggleProperty()
                                .get() == radioMmeNormal? "Normal" : "Tidak Normal");
                selectedSdidtk.setPerluDirujuk(
                        groupDirujuk.selectedToggleProperty()
                                .get() == radioDirujukYa? "Ya" : "Tidak");

                // update and check result
                if (sdidtkManager.updateSdidtk(selectedSdidtk) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil merubah data SDIDTK lama.");
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(false);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal merubah data SDIDTK lama, silahkan coba lagi.");
                }
            }
        } catch (NumberFormatException ex) {
            DialogUtils.showError("Format data yang diinputkan salah.\n"
                    + "Gunakan titik ( . ) sebagai pengganti koma ( , ) untuk data bilangan desimal.");
        } catch (Exception ex) {
            DialogUtils.showError("Gagal memproses data SDIDTK, silahkan coba lagi.");
        }
        root.setDisable(false);
    }
    @FXML private void deleteSdidtk() {
        if (selectedSdidtk == null) DialogUtils.showInfo("Silahkan pilih salah satu data SDIDTK terlebih dahulu di tabel.");
        else if (DialogUtils.showConfirm("Yakin akan menghapus data SDIDTK dari "
                +selectedSdidtk.getNamaAnak()+" \npada tanggal "
                +selectedSdidtk.getTanggal()+" ?") == DialogUtils.RESULT_OK
        ) {
            root.setDisable(true);
            String nama = selectedSdidtk.getNamaAnak(),
                    tanggal = selectedSdidtk.getTanggal();
            if (sdidtkManager.deleteSdidtk(selectedSdidtk) == AppConfig.SUCCESS_CODE) {
                DialogUtils.showInfo("Berhasil menghapus data SDIDTK dari "
                        +nama+" \npada tanggal "+tanggal);
                if (tableSdidtk.getItems().isEmpty()) {
                    selectedSdidtk = null;
                    fetchSelectedSdidtk();
                }
                disableForms(true);
                buttonSimpan.setDisable(true);
                buttonBatal.setDisable(false);
                buttonTambah.setDisable(false);
            } else {
                DialogUtils.showError("Gagal menghapus data SDIDTK dari "
                        +nama+" \npada tanggal "+tanggal
                        +", silahkan coba lagi.");
            }
            root.setDisable(false);
        }
    }
    @FXML private void cancelForms() {
        if (datePickerTanggal.isDisabled()) {  
            tableSdidtk.getSelectionModel().clearSelection();
            fetchSelectedPasien(null);
            selectedSdidtk = null;
            fetchSelectedSdidtk();
            disableForms(true);
        } else {            
            if (DialogUtils.showConfirm("Batalkan semua perubahan?")
                    == DialogUtils.RESULT_OK) {
                buttonSimpan.setDisable(true);
                buttonBatal.setDisable(false);
                buttonTambah.setDisable(false);
                fetchSelectedSdidtk();
                disableForms(true);
            }
        }
    }
    
    @FXML private void searchSdidtk() {
        tableSdidtk.setItems(sdidtkManager.filterSdidtkList(formSearchTable.getText()));
    }
    @FXML private void refreshTable() {
        formSearchTable.clear();
        tableSdidtk.setItems(sdidtkManager.getSdidtkList(true));    
    }
    
    private void handleSpin(Spinner spinner, Number oldValue, Object newValue) {
        try {
            if (newValue == null) {
                spinner.getValueFactory().setValue((int)oldValue);
            } else {
                spinner.getValueFactory().setValue((int)newValue);
            }
        } catch (Exception e) {
            System.out.println("spinner exception");
            spinner.getValueFactory().setValue(0);
        }
    }
}
