/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.home;

import er.app.config.AppConfig;
import er.app.feature.login.LoginPageController;
import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author DERI
 */
public class ERApp extends Application {
    
    public static final int LOGIN_PAGE = 110,
            HOME_PAGE = 111;
    
    private Stage mainStage;
    private BorderPane root;
    private Node loginView, homeView;
    private HomeController homeController;
    
    @Override
    public void start(Stage primaryStage) {
        mainStage = primaryStage;
        AppConfig.init(mainStage);
        
        initContentView();
        initRootView();
        showPage(LOGIN_PAGE);
        
        Scene scene = new Scene(root);
        
        mainStage.setTitle(AppConfig.appName);
        mainStage.getIcons().add(AppConfig.appLogo);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public void showPage(int page) {
        switch (page) {
            case LOGIN_PAGE:
                root.setCenter(loginView);
                break;
            case HOME_PAGE:
                root.setCenter(homeView);
                homeController.onPageShown();
                break;
            default:
                break;
        }
    }
    
    private void initRootView() {        
        FXMLLoader loader = new FXMLLoader(ERApp.class.getResource("Root.fxml"));
        try {
            root = loader.load();
        } catch (IOException ex) {
            System.err.println(ex);
            root = new BorderPane();
        }
    }
    private void initContentView() {
        FXMLLoader loader = new FXMLLoader(
                ERApp.class.getResource("/er/app/feature/login/LoginPage.fxml")
        );
        
        // Login
        try {
            loginView = loader.load();
            ((LoginPageController) loader.getController()).setMainApp(this);
        } catch (IOException ex) {
            System.err.println(ex);
            loginView = new BorderPane();
        }
        
        // Home
        loader = new FXMLLoader(
                ERApp.class.getResource("Home.fxml")
        );
        try {
            homeView = loader.load();
            homeController = (HomeController) loader.getController();
            homeController.setMainApp(this);
        } catch (IOException ex) {
            System.err.println(ex);
            homeView = new BorderPane();
        }
    }
}
