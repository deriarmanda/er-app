/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.home;

import er.app.data.manager.UserDataManager;
import er.app.data.model.User;
import er.app.feature.laporan.LaporanPageController;
import er.app.feature.maternal.MaternalPageController;
import er.app.feature.neonatal.NeonatalPageController;
import er.app.feature.pasien.PasienPageController;
import er.app.feature.persalinan.PersalinanPageController;
import er.app.feature.sdidtk.SdidtkPageController;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class HomeController implements Initializable {

    @FXML private Label textAdmin;
    @FXML private Label textTanggal;
    @FXML private BorderPane pagePasien;
    @FXML private BorderPane pageMaternal;
//    @FXML private BorderPane pageNeonatal;
    @FXML private BorderPane pagePersalinan;
    @FXML private BorderPane pageSdidtk;
    @FXML private BorderPane pageLaporan;
    
    private ERApp mainApp;
    private UserDataManager userManager;
    private PasienPageController pasienController;
    private MaternalPageController maternalController;
//    private NeonatalPageController neonatalController;
    private PersalinanPageController persalinanController;
    private SdidtkPageController sdidtkController;
    private LaporanPageController laporanController;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserDataManager.getInstance();
        initPages();
        textTanggal.setText(DateTimeUtils.getCurrentHumanReadableDate());
    }    
    
    public void setMainApp(ERApp mainApp) {
        this.mainApp = mainApp;
        laporanController.setMainApp(mainApp);
    }
    
    public void onPageShown() {
        User user = userManager.getCurrentUser();
        String admin = user == null? "Administrator" : "Administrator | " + user.getUsername();
        textAdmin.setText(admin);
    }
    
    private void initPages() {
        // Pasien Page
        FXMLLoader loader = new FXMLLoader(HomeController.class.getResource("/er/app/feature/pasien/PasienPage.fxml"));
        try {
            Node pasien = loader.load();
            pagePasien.setCenter(pasien);
            pasienController = (PasienPageController) loader.getController();
        } catch (IOException ex) {
            System.err.println(ex);
        }
        
        // Maternal Page
        loader = new FXMLLoader(HomeController.class.getResource("/er/app/feature/maternal/MaternalPage.fxml"));
        try {
            Node maternal = loader.load();
            pageMaternal.setCenter(maternal);
            maternalController = (MaternalPageController) loader.getController();
        } catch (IOException ex) {
            System.err.println(ex);
        }
        
        
        // @Deprecated Neonatal Page
        /*loader = new FXMLLoader(HomeController.class.getResource("/er/app/feature/neonatal/NeonatalPage.fxml"));
        try {
            Node neonatal = loader.load();
            pageNeonatal.setCenter(neonatal);
            neonatalController = (NeonatalPageController) loader.getController();
        } catch (IOException ex) {
            System.err.println(ex);
        }*/
        
        // Persalinan Page
        loader = new FXMLLoader(HomeController.class.getResource("/er/app/feature/persalinan/PersalinanPage.fxml"));
        try {
            Node persalinan = loader.load();
            pagePersalinan.setCenter(persalinan);
            persalinanController = (PersalinanPageController) loader.getController();
        } catch (IOException ex) {
            System.err.println(ex);
        }
        
        // SDIDTK Page
        loader = new FXMLLoader(HomeController.class.getResource("/er/app/feature/sdidtk/SdidtkPage.fxml"));
        try {
            Node sdidtk = loader.load();
            pageSdidtk.setCenter(sdidtk);
            sdidtkController = (SdidtkPageController) loader.getController();
        } catch (IOException ex) {
            System.err.println(ex);
        }
        
        // Laporan Page
        loader = new FXMLLoader(HomeController.class.getResource("/er/app/feature/laporan/LaporanPage.fxml"));
        try {
            Node laporan = loader.load();
            pageLaporan.setCenter(laporan);
            laporanController = (LaporanPageController) loader.getController();
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
    
    @FXML private void doLogout() { if (mainApp != null) mainApp.showPage(ERApp.LOGIN_PAGE); }
    @FXML private void openPasienPage() { if (pasienController != null) pasienController.onPageShown(); }
    @FXML private void openMaternalPage() { if (maternalController != null) maternalController.onPageShown(); }
    /* @Deprecated @FXML private void openNeonatalPage() { } */
    @FXML private void openPersalinanPage() { if (persalinanController != null) persalinanController.onPageShown(); }
    @FXML private void openSdidtkPage() { if (sdidtkController != null) sdidtkController.onPageShown(); }
    @FXML private void openLaporanPage() { if (laporanController != null) laporanController.onPageShown(); }
    @FXML private void showAboutDialog() {
        DialogUtils.showAbout();
    }
}
