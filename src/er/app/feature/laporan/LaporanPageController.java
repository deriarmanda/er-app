/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.laporan;

import er.app.config.AppConfig;
import er.app.data.manager.MaternalDataManager;
import er.app.data.manager.PersalinanDataManager;
import er.app.data.manager.SdidtkDataManager;
import er.app.feature.home.ERApp;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import er.app.util.ExcelHelper;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class LaporanPageController implements Initializable {
    
    private final int MATERNAL = 51;
    private final int NEONATAL = 52;
    private final int SDIDTK_BAYI = 53;
    private final int SDIDTK_BALITA = 54;
    
    // Maternal
    @FXML private ComboBox comboJenisMaternal;
    @FXML private ComboBox comboBulanMaternal;
    @FXML private ComboBox comboTahunMaternal;
    @FXML private Label labelPathMaternal;
    @FXML private Button buttonBrowseMaternal;
    @FXML private Button buttonExportMaternal;
    @FXML private ProgressBar progressMaternal;
    // Neonatal
    @FXML private ComboBox comboJenisNeonatal;
    @FXML private ComboBox comboBulanNeonatal;
    @FXML private ComboBox comboTahunNeonatal;
    @FXML private Label labelPathNeonatal;
    @FXML private Button buttonBrowseNeonatal;
    @FXML private Button buttonExportNeonatal;
    @FXML private ProgressBar progressNeonatal;
    // SDIDTK Bayi
    @FXML private ComboBox comboJenisBayi;
    @FXML private ComboBox comboBulanBayi;
    @FXML private ComboBox comboTahunBayi;
    @FXML private Label labelPathBayi;
    @FXML private Button buttonBrowseBayi;
    @FXML private Button buttonExportBayi;
    @FXML private ProgressBar progressBayi;
    // SDIDTK Balita
    @FXML private ComboBox comboJenisBalita;
    @FXML private ComboBox comboBulanBalita;
    @FXML private ComboBox comboTahunBalita;
    @FXML private Label labelPathBalita;
    @FXML private Button buttonBrowseBalita;
    @FXML private Button buttonExportBalita;
    @FXML private ProgressBar progressBalita;
    
    private ERApp mainApp;
    private MaternalDataManager maternalManager;
    private PersalinanDataManager persalinanManager;
    private SdidtkDataManager sdidtkManager;
    private File fileMaternal;
    private File fileNeonatal;
    private File fileSdidtkBayi;
    private File fileSdidtkBalita;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        maternalManager = MaternalDataManager.getInstance();
        persalinanManager = PersalinanDataManager.getInstance();
        sdidtkManager = SdidtkDataManager.getInstance();
        
        initViews();
    }    
    
    public void setMainApp(ERApp mainApp) {
        this.mainApp = mainApp;
    }
    
    public void onPageShown() {
        System.out.println("Laporan Controller shown");
    }
    
    private void initViews() {
        ObservableList<String> jenisLaporan = 
                FXCollections.observableArrayList(
                        "Laporan Bulanan", 
                        "Laporan Tahunan"
                );
        ObservableList<String> bulanLaporan = 
                FXCollections.observableArrayList(
                        "Januari", 
                        "Februari", 
                        "Maret", 
                        "April", 
                        "Mei", 
                        "Juni", 
                        "Juli", 
                        "Agustus", 
                        "September", 
                        "Oktober", 
                        "November", 
                        "Desember"
                );
        ObservableList<String> tahunLaporan = FXCollections.observableArrayList();
        for (int i=2000; i<=2100; i++) tahunLaporan.add(String.valueOf(i));
        
        // Maternal
        comboJenisMaternal.setItems(jenisLaporan);
        comboJenisMaternal.getSelectionModel().selectFirst();
        comboJenisMaternal.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            comboBulanMaternal.setDisable(comboJenisMaternal.getSelectionModel().isSelected(1));
        });
        comboBulanMaternal.setItems(bulanLaporan);
        comboBulanMaternal.getSelectionModel().selectFirst();
        comboTahunMaternal.setItems(tahunLaporan);
        comboTahunMaternal.getSelectionModel().select(DateTimeUtils.getCurrentYear());
        
        // Neonatal
        comboJenisNeonatal.setItems(jenisLaporan);
        comboJenisNeonatal.getSelectionModel().selectFirst();
        comboJenisNeonatal.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            comboBulanNeonatal.setDisable(comboJenisNeonatal.getSelectionModel().isSelected(1));
        });
        comboBulanNeonatal.setItems(bulanLaporan);
        comboBulanNeonatal.getSelectionModel().selectFirst();
        comboTahunNeonatal.setItems(tahunLaporan);
        comboTahunNeonatal.getSelectionModel().select(DateTimeUtils.getCurrentYear());
        
        // SDIDTK Bayi
        comboJenisBayi.setItems(jenisLaporan);
        comboJenisBayi.getSelectionModel().selectFirst();
        comboJenisBayi.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            comboBulanBayi.setDisable(comboJenisBayi.getSelectionModel().isSelected(1));
        });
        comboBulanBayi.setItems(bulanLaporan);
        comboBulanBayi.getSelectionModel().selectFirst();
        comboTahunBayi.setItems(tahunLaporan);
        comboTahunBayi.getSelectionModel().select(DateTimeUtils.getCurrentYear());
        
        // SDIDTK Anak Balita
        comboJenisBalita.setItems(jenisLaporan);
        comboJenisBalita.getSelectionModel().selectFirst();
        comboJenisBalita.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            comboBulanBalita.setDisable(comboJenisBalita.getSelectionModel().isSelected(1));
        });
        comboBulanBalita.setItems(bulanLaporan);
        comboBulanBalita.getSelectionModel().selectFirst();
        comboTahunBalita.setItems(tahunLaporan);
        comboTahunBalita.getSelectionModel().select(DateTimeUtils.getCurrentYear());
    }
    
    private void onReportingFinished(int whichReport, int resultCode) {
        switch (whichReport) {
            case MATERNAL: 
                if (resultCode == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil membuat laporan maternal pada\n"+fileMaternal.getAbsolutePath());
                    try { 
                        Desktop.getDesktop().browse(fileMaternal.getParentFile().toURI());
                    } catch(IOException ex) { }
                    
                    fileMaternal = null;
                    labelPathMaternal.setText("-");
                } else {                
                    DialogUtils.showError("Gagal membuat laporan maternal, silahkan coba kembali.");
                }
                comboJenisMaternal.setDisable(false);
                comboBulanMaternal.setDisable(comboJenisMaternal.getSelectionModel().isSelected(1));
                comboTahunMaternal.setDisable(false);
                buttonBrowseMaternal.setDisable(false);
                buttonExportMaternal.setDisable(false);
                progressMaternal.setVisible(false);
                break;
            case NEONATAL:
                if (resultCode == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil membuat laporan neonatal pada\n"+fileNeonatal.getAbsolutePath());
                    try { 
                        Desktop.getDesktop().browse(fileNeonatal.getParentFile().toURI());
                    } catch(IOException ex) { }
                    
                    fileNeonatal = null;
                    labelPathNeonatal.setText("-");
                } else {                
                    DialogUtils.showError("Gagal membuat laporan neonatal, silahkan coba kembali.");
                }
                comboJenisNeonatal.setDisable(false);
                comboBulanNeonatal.setDisable(comboJenisNeonatal.getSelectionModel().isSelected(1));
                comboTahunNeonatal.setDisable(false);
                buttonBrowseNeonatal.setDisable(false);
                buttonExportNeonatal.setDisable(false);
                progressNeonatal.setVisible(false);
                break;
            case SDIDTK_BAYI: 
                if (resultCode == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil membuat laporan SDIDTK bayi pada\n"+fileSdidtkBayi.getAbsolutePath());
                    try { 
                        Desktop.getDesktop().browse(fileSdidtkBayi.getParentFile().toURI());
                    } catch(IOException ex) { }
                    
                    fileSdidtkBayi = null;
                    labelPathBayi.setText("-");
                } else {                
                    DialogUtils.showError("Gagal membuat laporan SDIDTK bayi, silahkan coba kembali.");
                }
                comboJenisBayi.setDisable(false);
                comboBulanBayi.setDisable(comboJenisBayi.getSelectionModel().isSelected(1));
                comboTahunBayi.setDisable(false);
                buttonBrowseBayi.setDisable(false);
                buttonExportBayi.setDisable(false);
                progressBayi.setVisible(false);
                break;
            case SDIDTK_BALITA:
                if (resultCode == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil membuat laporan SDIDTK balita pada\n"+fileSdidtkBalita.getAbsolutePath());
                    try { 
                        Desktop.getDesktop().browse(fileSdidtkBalita.getParentFile().toURI());
                    } catch(IOException ex) { }
                    
                    fileSdidtkBalita = null;
                    labelPathBalita.setText("-");
                } else {                
                    DialogUtils.showError("Gagal membuat laporan SDIDTK balita, silahkan coba kembali.");
                }
                comboJenisBalita.setDisable(false);
                comboBulanBalita.setDisable(comboJenisBalita.getSelectionModel().isSelected(1));
                comboTahunBalita.setDisable(false);
                buttonBrowseBalita.setDisable(false);
                buttonExportBalita.setDisable(false);
                progressBalita.setVisible(false);
                break;
            default:
                break;
        }
    }
    
    @FXML private void openFileChooser(ActionEvent event) {
        if (event.getSource() == buttonBrowseMaternal) {
            String defaultName = comboJenisMaternal.getSelectionModel().getSelectedItem()
                    +" Maternal "+DateTimeUtils.format(LocalDateTime.now());
            defaultName = defaultName.replaceAll(":", "-");
            fileMaternal = DialogUtils.showSaveFileDialog(defaultName);
            if (fileMaternal != null) labelPathMaternal.setText(fileMaternal.getAbsolutePath());
        
        } else if (event.getSource() == buttonBrowseNeonatal) {
            String defaultName = comboJenisNeonatal.getSelectionModel().getSelectedItem()
                    +" Neonatal "+DateTimeUtils.format(LocalDateTime.now());
            defaultName = defaultName.replaceAll(":", "-");
            fileNeonatal = DialogUtils.showSaveFileDialog(defaultName);
            if (fileNeonatal != null) labelPathNeonatal.setText(fileNeonatal.getAbsolutePath());
        
        } else if (event.getSource() == buttonBrowseBayi) {
            String defaultName = comboJenisBayi.getSelectionModel().getSelectedItem()
                    +" SDIDTK Bayi "+DateTimeUtils.format(LocalDateTime.now());
            defaultName = defaultName.replaceAll(":", "-");
            fileSdidtkBayi = DialogUtils.showSaveFileDialog(defaultName);
            if (fileSdidtkBayi != null) labelPathBayi.setText(fileSdidtkBayi.getAbsolutePath());
        
        } else if (event.getSource() == buttonBrowseBalita) {
            String defaultName = comboJenisBalita.getSelectionModel().getSelectedItem()
                    +" SDIDTK Anak Balita "+DateTimeUtils.format(LocalDateTime.now());
            defaultName = defaultName.replaceAll(":", "-");
            fileSdidtkBalita = DialogUtils.showSaveFileDialog(defaultName);
            if (fileSdidtkBalita != null) labelPathBalita.setText(fileSdidtkBalita.getAbsolutePath());
        }
    }
    @FXML private void exportMaternalReport() {
        if (fileMaternal == null) {
            DialogUtils.showError("Silahkan menentukan lokasi penyimpanan terlebih dahulu.");
            return;
        }
        
        comboJenisMaternal.setDisable(true);
        comboBulanMaternal.setDisable(true);
        comboTahunMaternal.setDisable(true);
        buttonBrowseMaternal.setDisable(true);
        buttonExportMaternal.setDisable(true);
        progressMaternal.setVisible(true);
        
        maternalManager.getMaternalList(true);
        persalinanManager.getPersalinanList(true);
        
        new Thread() {
            @Override
            public void run() {
                int resultCode;
                
                if (comboJenisMaternal.getSelectionModel().isSelected(0)) {
                    // Laporan Bulanan
                    String year = comboTahunMaternal.getSelectionModel().getSelectedItem().toString();
                    int month = comboBulanMaternal.getSelectionModel().getSelectedIndex() + 1;
                    String date = year+"-"+(month<10? "0"+month : ""+month);
                    
                    int[][] data = new int[4][35];
                    for (int i=0; i<4; i++) Arrays.fill(data[i], 0);
                    data = maternalManager.processMonthlyReport(data, date);
                    data = persalinanManager.processMaternalMonthlyReport(data, date);
                    
                    resultCode = ExcelHelper.exportMaternalMonthlyReport(
                            fileMaternal, 
                            comboBulanMaternal.getSelectionModel().getSelectedItem().toString(), 
                            comboTahunMaternal.getSelectionModel().getSelectedItem().toString(), 
                            data
                    );
                } else {
                    // Laporan Tahunan
                    String year = comboTahunMaternal.getSelectionModel().getSelectedItem().toString();
                    int[][][] data = new int[4][12][35];
                    for (int i=0; i<4; i++) {
                        for (int j=0; j<12; j++) Arrays.fill(data[i][j], 0);
                    }
                    data = maternalManager.processAnnualReport(data, year);
                    data = persalinanManager.processMaternalAnnualReport(data, year);
                    
                    resultCode = ExcelHelper.exportMaternalAnnualReport(
                            fileMaternal, 
                            year, 
                            data
                    );
                }
                
                Platform.runLater(() -> {
                    onReportingFinished(MATERNAL, resultCode);
                });
            }            
        }.start();
    }
    @FXML private void exportNeonatalReport() {
        if (fileNeonatal == null) {
            DialogUtils.showError("Silahkan menentukan lokasi penyimpanan terlebih dahulu.");
            return;
        }
        
        comboJenisNeonatal.setDisable(true);
        comboBulanNeonatal.setDisable(true);
        comboTahunNeonatal.setDisable(true);
        buttonBrowseNeonatal.setDisable(true);
        buttonExportNeonatal.setDisable(true);
        progressNeonatal.setVisible(true);
        
        //maternalManager.getMaternalList(true);
        persalinanManager.getPersalinanList(true);
        
        new Thread() {
            @Override
            public void run() {
                int resultCode;
                String header, date;
                if (comboJenisNeonatal.getSelectionModel().isSelected(0)) {
                    // Laporan Bulanan
                    header = "BULAN "+comboBulanNeonatal.getSelectionModel()
                            .getSelectedItem().toString().toUpperCase()
                            +" TAHUN "+comboTahunNeonatal.getSelectionModel()
                            .getSelectedItem();     
                    
                    String year = comboTahunNeonatal.getSelectionModel()
                            .getSelectedItem().toString();
                    int month = comboBulanNeonatal.getSelectionModel()
                            .getSelectedIndex() + 1;
                    date = year+"-"+(month<10? "0"+month : ""+month);
                } else {
                    // Laporan Tahunan
                    header = "TAHUN "+comboTahunNeonatal.getSelectionModel()
                            .getSelectedItem();
                    date = comboTahunNeonatal.getSelectionModel()
                            .getSelectedItem().toString();
                }
                
                int[][] data = new int[5][30];
                for (int i=0; i<5; i++) Arrays.fill(data[i], 0);
                data = persalinanManager.processNeonatalReport(data, date);
                
                resultCode = ExcelHelper.exportNeonatalReport(
                        fileNeonatal, 
                        header,
                        data
                );
                    
                Platform.runLater(() -> {
                    onReportingFinished(NEONATAL, resultCode);
                });
            }            
        }.start();
    }
    @FXML private void exportSdidtkBayiReport() {
        if (fileSdidtkBayi == null) {
            DialogUtils.showError("Silahkan menentukan lokasi penyimpanan terlebih dahulu.");
            return;
        }
        
        comboJenisBayi.setDisable(true);
        comboBulanBayi.setDisable(true);
        comboTahunBayi.setDisable(true);
        buttonBrowseBayi.setDisable(true);
        buttonExportBayi.setDisable(true);
        progressBayi.setVisible(true);
        
        sdidtkManager.getSdidtkList(true);
        
        new Thread() {
            @Override
            public void run() {
                int resultCode;
                String date, month,
                        year = comboTahunBayi.getSelectionModel()
                            .getSelectedItem().toString();
                
                if (comboJenisBayi.getSelectionModel().isSelected(0)) {
                    // Laporan Bulanan
                    month = comboBulanBayi.getSelectionModel()
                            .getSelectedItem().toString();
                    
                    int indexMonth = comboBulanBayi.getSelectionModel()
                            .getSelectedIndex() + 1;
                    date = year+"-"+(indexMonth<10? "0"+indexMonth : ""+indexMonth);
                } else {
                    // Laporan Tahunan
                    month = "Semua Bulan";
                    
                    date = comboTahunBayi.getSelectionModel()
                            .getSelectedItem().toString();
                }
                
                int[][] data = new int[4][22];
                for (int i=0; i<4; i++) Arrays.fill(data[i], 0);
                data = sdidtkManager.processSdidtkReport(data, date, 0, 12);
                
                resultCode = ExcelHelper.exportSdidtkReport(
                        fileSdidtkBayi, 
                        "laporan_sdidtk_bayi.xlsx",
                        month, 
                        year, 
                        data
                );
                    
                Platform.runLater(() -> {
                    onReportingFinished(SDIDTK_BAYI, resultCode);
                });
            }            
        }.start();
    }
    @FXML private void exportSdidtkBalitaReport() {
        if (fileSdidtkBalita == null) {
            DialogUtils.showError("Silahkan menentukan lokasi penyimpanan terlebih dahulu.");
            return;
        }
        
        comboJenisBalita.setDisable(true);
        comboBulanBalita.setDisable(true);
        comboTahunBalita.setDisable(true);
        buttonBrowseBalita.setDisable(true);
        buttonExportBalita.setDisable(true);
        progressBalita.setVisible(true);
        
        sdidtkManager.getSdidtkList(true);
        
        new Thread() {
            @Override
            public void run() {
                int resultCode;
                String date, month,
                        year = comboTahunBalita.getSelectionModel()
                            .getSelectedItem().toString();
                
                if (comboJenisBalita.getSelectionModel().isSelected(0)) {
                    // Laporan Bulanan
                    month = comboBulanBalita.getSelectionModel()
                            .getSelectedItem().toString();
                    
                    int indexMonth = comboBulanBalita.getSelectionModel()
                            .getSelectedIndex() + 1;
                    date = year+"-"+(indexMonth<10? "0"+indexMonth : ""+indexMonth);
                } else {
                    // Laporan Tahunan
                    month = "Semua Bulan";
                    
                    date = comboTahunBalita.getSelectionModel()
                            .getSelectedItem().toString();
                }
                
                int[][] data = new int[4][22];
                for (int i=0; i<4; i++) Arrays.fill(data[i], 0);
                data = sdidtkManager.processSdidtkReport(data, date, 13, 60);
                
                resultCode = ExcelHelper.exportSdidtkReport(
                        fileSdidtkBalita, 
                        "laporan_sdidtk_balita.xlsx",
                        month, 
                        year, 
                        data
                );
                    
                Platform.runLater(() -> {
                    onReportingFinished(SDIDTK_BALITA, resultCode);
                });
            }            
        }.start();
    }
}