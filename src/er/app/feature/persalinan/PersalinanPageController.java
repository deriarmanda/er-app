/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.persalinan;

import er.app.config.AppConfig;
import er.app.data.manager.PasienDataManager;
import er.app.data.manager.PersalinanDataManager;
import er.app.data.model.Pasien;
import er.app.data.model.Persalinan;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import org.controlsfx.control.CheckComboBox;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class PersalinanPageController implements Initializable {

    @FXML private VBox root;
    
    // Formulir Views
    @FXML private TitledPane containerForms;
    @FXML private DatePicker datePickerTanggal;
    @FXML private Label labelKelurahan;
    @FXML private TextField formNomorRM;
    @FXML private TextField formNamaPasien;
    @FXML private TextField formNamaSuami;
    @FXML private TextField formDiagnosa;
    @FXML private ComboBox comboJenisPartus;
    @FXML private TextField spinnerUsiaKehamilan;
    @FXML private TextField spinnerBeratBadanLahir;
    @FXML private TextField spinnerPanjangBadanLahir;
    @FXML private ToggleGroup groupJenisKelamin;
    @FXML private RadioButton radioLaki;
    @FXML private RadioButton radioPerempuan;
    @FXML private TextField formKondisiIbu;
    @FXML private TextField formKondisiBayi;
    @FXML private ComboBox comboApgarSkor;
    @FXML private ToggleGroup groupVitamin;
    @FXML private RadioButton radioVitaminYa;
    @FXML private RadioButton radioVitaminTidak;
    @FXML private ToggleGroup groupVaksin;
    @FXML private RadioButton radioVaksinYa;
    @FXML private RadioButton radioVaksinTidak;
    @FXML private CheckComboBox choiceNeonatal;
    @FXML private ComboBox comboTindakan;
    @FXML private TextField formTempatLahir;
    @FXML private TextField formNamaPenolong;
    @FXML private Button buttonSearchNomorRM;
    @FXML private Button buttonSearchNamaPasien;
    @FXML private Button buttonTambah;
    @FXML private Button buttonUpdate;
    @FXML private Button buttonDelete;
    @FXML private Button buttonSimpan;
    @FXML private Button buttonBatal;
    
    // Table Views
    @FXML private TableView<Persalinan> tablePersalinan;
    @FXML private TableColumn<Persalinan, String> colTanggal;
    @FXML private TableColumn<Persalinan, String> colKelurahan;
    @FXML private TableColumn<Persalinan, String> colNomorRM;
    @FXML private TableColumn<Persalinan, String> colNamaPasien;
    @FXML private TableColumn<Persalinan, String> colNamaSuami;
    @FXML private TableColumn<Persalinan, String> colDiagnosa;
    @FXML private TableColumn<Persalinan, String> colJenisPartus;
    @FXML private TableColumn<Persalinan, Number> colUsiaKehamilan; 
    @FXML private TableColumn<Persalinan, Number> colBeratBadanLahir;
    @FXML private TableColumn<Persalinan, Number> colPanjangBadanLahir;
    @FXML private TableColumn<Persalinan, String> colJenisKelamin;
    @FXML private TableColumn<Persalinan, String> colKondisiIbu;
    @FXML private TableColumn<Persalinan, String> colKondisiBayi;
    @FXML private TableColumn<Persalinan, String> colVitamin;
    @FXML private TableColumn<Persalinan, String> colVaksin;
    @FXML private TableColumn<Persalinan, String> colNeonatal;
    @FXML private TableColumn<Persalinan, String> colTindakan;
    @FXML private TableColumn<Persalinan, String> colTempatLahir;
    @FXML private TextField formSearchTable;
    @FXML private Button buttonSearchTable;
    @FXML private Button buttonRefreshTable;
    
    private PasienDataManager pasienManager;
    private PersalinanDataManager persalinanManager;
    private Persalinan selectedPersalinan;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pasienManager = PasienDataManager.getInstance();
        persalinanManager = PersalinanDataManager.getInstance();
        selectedPersalinan = null;
        
        initForms();
        initTables();
    }     
    
    public void onPageShown() {
        System.out.println("Persalinan Controller shown");
    }
    
    private void initForms() {
        // DatePicker tanggal
        datePickerTanggal.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        
        // Form usia kehamilan
        spinnerUsiaKehamilan.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerUsiaKehamilan.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form berat badan lahir
        spinnerBeratBadanLahir.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerBeratBadanLahir.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form panjang badan lahir
        spinnerPanjangBadanLahir.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[+-]?([0-9]*[.])?[0-9]+")) {
                spinnerPanjangBadanLahir.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        
        // Form Apgar Skor
        ObservableList<Number> listApgarSkor = 
                FXCollections.observableArrayList(
                        0, 1, 2, 3, 4, 5,
                        6, 7, 8, 9, 10
                );
        comboApgarSkor.setItems(listApgarSkor);
        comboApgarSkor.getSelectionModel().selectFirst();
        
        // Form Jenis Partus
        ObservableList<String> listJenisPartus = 
                FXCollections.observableArrayList(
                        "Partus Normal", 
                        "Sectio Caesaria", 
                        "Abortus", 
                        "Persalinan Dukun"
                );
        comboJenisPartus.setItems(listJenisPartus);
        comboJenisPartus.getSelectionModel().selectFirst();
        
        // Form Kasus Neonatal
        ObservableList<String> listKasus = 
                FXCollections.observableArrayList(
                        "Bayi Lahir Prematur", 
                        "Trauma Lahir", 
                        "Asfiksia", 
                        "BBLR < 2500 gr", 
                        "Infeksi", 
                        "Tetanus Neonatorum", 
                        "Kelainan Bawaan", 
                        "Hipotiroid Kongenital (+)", 
                        "Icterus", 
                        "Masalah Pemberian ASI"
                );
        choiceNeonatal.getItems().clear();
        choiceNeonatal.getItems().addAll(listKasus);

        // Form tindakan
        ObservableList<String> listTindakan = 
                FXCollections.observableArrayList(
                        "Drip", 
                        "Vakum / Forcep", 
                        "Curetage", 
                        "Plasenta Manual",
                        "Tidak ada tindakan"
                );
        comboTindakan.setItems(listTindakan);
        comboTindakan.getSelectionModel().selectFirst();
    }
    private void initTables() {
        colTanggal.setCellValueFactory(value -> value.getValue().tanggalPartusProperty());
        colKelurahan.setCellValueFactory(value -> value.getValue().kelurahanProperty());
        colNomorRM.setCellValueFactory(value -> value.getValue().nomorRMProperty());
        colNamaPasien.setCellValueFactory(value -> value.getValue().namaPasienProperty());
        colNamaSuami.setCellValueFactory(value -> value.getValue().namaSuamiProperty());
        colDiagnosa.setCellValueFactory(value -> value.getValue().diagnosaProperty());
        colJenisPartus.setCellValueFactory(value -> value.getValue().jenisPartusProperty());
        colUsiaKehamilan.setCellValueFactory(value -> value.getValue().usiaKehamilanProperty());
        colBeratBadanLahir.setCellValueFactory(value -> value.getValue().beratBadanLahirProperty());
        colPanjangBadanLahir.setCellValueFactory(value -> value.getValue().panjangBadanLahirProperty());
        colJenisKelamin.setCellValueFactory(value -> value.getValue().jenisKelaminProperty());
        colKondisiIbu.setCellValueFactory(value -> value.getValue().kondisiIbuProperty());
        colKondisiBayi.setCellValueFactory(value -> value.getValue().kondisiBayiProperty());
        colVitamin.setCellValueFactory(value -> value.getValue().vitaminProperty());
        colVaksin.setCellValueFactory(value -> value.getValue().vaksinProperty());
        colNeonatal.setCellValueFactory(value -> value.getValue().neonatalProperty());
        colTindakan.setCellValueFactory(value -> value.getValue().tindakanProperty());
        colTempatLahir.setCellValueFactory(value -> value.getValue().tempatLahirProperty());
        
        tablePersalinan.setItems(persalinanManager.getPersalinanList());
        tablePersalinan.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Persalinan> observable, 
                        Persalinan oldValue, 
                        Persalinan newValue) -> {
            selectedPersalinan = newValue == null? oldValue : newValue;
            fetchSelectedPersalinan();
            buttonTambah.setDisable(false);
            buttonBatal.setDisable(false);
            formNomorRM.setDisable(true);
            buttonSearchNomorRM.setDisable(true);
            formNamaPasien.setDisable(true);
            buttonSearchNamaPasien.setDisable(true);
            containerForms.setExpanded(true);
        });
    }
    
    private void fetchSelectedPasien(Pasien pasien) {
        buttonTambah.setDisable(pasien == null);
        buttonBatal.setDisable(pasien == null);
        formNomorRM.setDisable(pasien != null);
        buttonSearchNomorRM.setDisable(pasien != null);
        formNamaPasien.setDisable(pasien != null);
        buttonSearchNamaPasien.setDisable(pasien != null);
        
        if (pasien != null) {
            labelKelurahan.setText("Kelurahan "+pasien.getKelurahan());
            formNomorRM.setText(pasien.getNomorRM());
            formNamaPasien.setText(pasien.getNamaLengkap());
            buttonTambah.requestFocus();
        } else {
            labelKelurahan.setText("Kelurahan -");
            formNomorRM.clear();
            formNamaPasien.clear();
            formNomorRM.requestFocus();
        }            
    }
    private void fetchSelectedPersalinan() {
        if (!buttonSimpan.isDisabled()) return;
        
        if (selectedPersalinan == null) {
            datePickerTanggal.setValue(null);
            formNamaSuami.clear();
            formDiagnosa.clear();
            formKondisiIbu.clear();
            formKondisiBayi.clear();
            formTempatLahir.clear();
            formNamaPenolong.clear();
            choiceNeonatal.getCheckModel().clearChecks();
        } else {
            datePickerTanggal.setValue(DateTimeUtils.parseDateOnly(selectedPersalinan.getTanggalPartus()));
            labelKelurahan.setText("Kelurahan "+selectedPersalinan.getKelurahan());
            formNomorRM.setText(selectedPersalinan.getNomorRM());
            formNamaPasien.setText(selectedPersalinan.getNamaPasien());
            formNamaSuami.setText(selectedPersalinan.getNamaSuami());
            formDiagnosa.setText(selectedPersalinan.getDiagnosa());
            comboJenisPartus.getSelectionModel().select(selectedPersalinan.getJenisPartus());
            spinnerUsiaKehamilan.setText(selectedPersalinan.getUsiaKehamilan()+"");
            spinnerBeratBadanLahir.setText(selectedPersalinan.getBeratBadanLahir()+"");
            spinnerPanjangBadanLahir.setText(selectedPersalinan.getPanjangBadanLahir()+"");
            groupJenisKelamin.selectToggle(
                    selectedPersalinan.getJenisKelamin().equals("Laki - laki")?
                            radioLaki : radioPerempuan
            );
            formKondisiIbu.setText(selectedPersalinan.getKondisiIbu());
            formKondisiBayi.setText(selectedPersalinan.getKondisiBayi());
            comboApgarSkor.getSelectionModel().select(selectedPersalinan.getApgarSkor());
            groupVitamin.selectToggle(
                    selectedPersalinan.getVitamin().equals("Ya")?
                            radioVitaminYa : radioVitaminTidak
            );
            groupVaksin.selectToggle(
                    selectedPersalinan.getVaksin().equals("Ya")?
                            radioVaksinYa : radioVaksinTidak
            );
            comboTindakan.getSelectionModel().select(selectedPersalinan.getTindakan());
            formTempatLahir.setText(selectedPersalinan.getTempatLahir());
            formNamaPenolong.setText(selectedPersalinan.getNamaPenolong());
            
            String temp = selectedPersalinan.getNeonatal();
            temp = temp.substring(1, temp.length()-1);
            String kasus[] = temp.split(", ");
            choiceNeonatal.getCheckModel().clearChecks();
            for (String k : kasus) choiceNeonatal.getCheckModel().check(k);            
        }
        
        buttonUpdate.setDisable(selectedPersalinan == null);
        buttonDelete.setDisable(selectedPersalinan == null);
    }
    private void disableForms(boolean isDisabled) {
        datePickerTanggal.setDisable(isDisabled);
        formNamaSuami.setDisable(isDisabled);
        formDiagnosa.setDisable(isDisabled);
        comboJenisPartus.setDisable(isDisabled);
        spinnerUsiaKehamilan.setDisable(isDisabled);
        spinnerBeratBadanLahir.setDisable(isDisabled);
        spinnerPanjangBadanLahir.setDisable(isDisabled);
        radioLaki.setDisable(isDisabled);
        radioPerempuan.setDisable(isDisabled);
        formKondisiIbu.setDisable(isDisabled);
        formKondisiBayi.setDisable(isDisabled);
        comboApgarSkor.setDisable(isDisabled);
        radioVitaminYa.setDisable(isDisabled);
        radioVitaminTidak.setDisable(isDisabled);
        radioVaksinYa.setDisable(isDisabled);
        radioVaksinTidak.setDisable(isDisabled);
        choiceNeonatal.setDisable(isDisabled);
        comboTindakan.setDisable(isDisabled);
        formTempatLahir.setDisable(isDisabled);
        formNamaPenolong.setDisable(isDisabled);
    }
    
    @FXML private void searchPasienByNomorRM() {
        Pasien pasien = pasienManager.searchPasienByNomorRM(formNomorRM.getText().trim());
        if (pasien == null) {            
            DialogUtils.showError("Data pasien tidak ditemukan.");
            labelKelurahan.setText("Kelurahan -");
        } else fetchSelectedPasien(pasien);
    }
    @FXML private void searchPasienByNamaLengkap() {
        Pasien pasien = pasienManager.searchPasienByNamaLengkap(formNamaPasien.getText().trim());
        if (pasien == null) {            
            DialogUtils.showError("Data pasien tidak ditemukan.");
            labelKelurahan.setText("Kelurahan -");
        } else fetchSelectedPasien(pasien);
    }
    @FXML private void onButtonTambahClicked() {
        selectedPersalinan = null;
        fetchSelectedPersalinan();
        disableForms(false);
        buttonTambah.setDisable(true);        
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);        
        buttonSimpan.setDisable(false);    
        buttonBatal.setDisable(false);
    }
    @FXML private void onButtonUpdateClicked() {
        disableForms(false);
        buttonTambah.setDisable(true);        
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);        
        buttonSimpan.setDisable(false);    
        buttonBatal.setDisable(false); 
    }
    @FXML private void savePersalinan() {
        if (formNomorRM.getText().trim().isEmpty() || 
                formNamaPasien.getText().trim().isEmpty()) {
            DialogUtils.showError("Data nomor RM dan nama pasien harus diisi.");
            disableForms(true);
            cancelForms();
            return;
        }
        
        root.setDisable(true);
        try {
            if (selectedPersalinan == null) {
                // insert new persalinan
                if (datePickerTanggal.getValue() == null) {
                    DialogUtils.showError("Data tidak boleh kosong.");
                    root.setDisable(false);
                    return;
                }

                // Collect data persalinan
                Persalinan persalinan = new Persalinan();
                persalinan.setNomorRM(formNomorRM.getText().trim());
                persalinan.setNamaPasien(formNamaPasien.getText().trim());
                persalinan.setTanggalPartus(datePickerTanggal.getValue().toString());
                persalinan.setKelurahan(labelKelurahan.getText().substring(10));
                persalinan.setNamaSuami(formNamaSuami.getText().trim());
                persalinan.setDiagnosa(formDiagnosa.getText().trim());
                persalinan.setJenisPartus(comboJenisPartus.getSelectionModel().getSelectedItem().toString());
                persalinan.setUsiaKehamilan(Integer.parseInt(spinnerUsiaKehamilan.getText().trim()));
                persalinan.setBeratBadanLahir(Integer.parseInt(spinnerBeratBadanLahir.getText().trim()));
                persalinan.setPanjangBadanLahir(Integer.parseInt(spinnerPanjangBadanLahir.getText().trim()));
                persalinan.setJenisKelamin(
                        groupJenisKelamin.selectedToggleProperty()
                                .get() == radioLaki? "Laki - laki" : "Perempuan");
                persalinan.setKondisiIbu(formKondisiIbu.getText().trim());
                persalinan.setKondisiBayi(formKondisiBayi.getText().trim());
                persalinan.setApgarSkor(comboApgarSkor.getSelectionModel().getSelectedIndex());
                persalinan.setVitamin(
                        groupVitamin.selectedToggleProperty()
                                .get() == radioVitaminYa? "Ya" : "Tidak");
                persalinan.setVaksin(
                        groupVaksin.selectedToggleProperty()
                                .get() == radioVaksinYa? "Ya" : "Tidak");
                persalinan.setNeonatal(choiceNeonatal.getCheckModel().getCheckedItems().toString());
                persalinan.setTindakan(comboTindakan.getSelectionModel().getSelectedItem().toString());
                persalinan.setTempatLahir(formTempatLahir.getText().trim());
                persalinan.setNamaPenolong(formNamaPenolong.getText().trim());

                // insert and check result
                if (persalinanManager.addPersalinan(persalinan) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil menambahkan data persalinan baru.");
                    selectedPersalinan = persalinan;
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(false);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal menambahkan data persalinan baru, silahkan coba lagi.");
                }
            } else {
                // update selected persalinan
                if (datePickerTanggal.getValue() == null) {
                    DialogUtils.showError("Data tidak boleh kosong.");
                    root.setDisable(false);
                    return;
                }

                // Collect data persalinan
                selectedPersalinan.setTanggalPartus(datePickerTanggal.getValue().toString());
                selectedPersalinan.setNamaSuami(formNamaSuami.getText().trim());
                selectedPersalinan.setDiagnosa(formDiagnosa.getText().trim());
                selectedPersalinan.setJenisPartus(comboJenisPartus.getSelectionModel().getSelectedItem().toString());
                selectedPersalinan.setUsiaKehamilan(Integer.parseInt(spinnerUsiaKehamilan.getText().trim()));
                selectedPersalinan.setBeratBadanLahir(Integer.parseInt(spinnerBeratBadanLahir.getText().trim()));
                selectedPersalinan.setPanjangBadanLahir(Integer.parseInt(spinnerPanjangBadanLahir.getText().trim()));
                selectedPersalinan.setJenisKelamin(
                        groupJenisKelamin.selectedToggleProperty()
                                .get() == radioLaki? "Laki - laki" : "Perempuan");
                selectedPersalinan.setKondisiIbu(formKondisiIbu.getText().trim());
                selectedPersalinan.setKondisiBayi(formKondisiBayi.getText().trim());
                selectedPersalinan.setApgarSkor(comboApgarSkor.getSelectionModel().getSelectedIndex());
                selectedPersalinan.setVitamin(
                        groupVitamin.selectedToggleProperty()
                                .get() == radioVitaminYa? "Ya" : "Tidak");
                selectedPersalinan.setVaksin(
                        groupVaksin.selectedToggleProperty()
                                .get() == radioVaksinYa? "Ya" : "Tidak");
                selectedPersalinan.setNeonatal(choiceNeonatal.getCheckModel().getCheckedItems().toString());
                selectedPersalinan.setTindakan(comboTindakan.getSelectionModel().getSelectedItem().toString());
                selectedPersalinan.setTempatLahir(formTempatLahir.getText().trim());
                selectedPersalinan.setNamaPenolong(formNamaPenolong.getText().trim());

                // update and check result
                if (persalinanManager.updatePersalinan(selectedPersalinan) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil merubah data persalinan lama.");
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(false);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal merubah data persalinan lama, silahkan coba lagi.");
                }
            }
        } catch (NumberFormatException ex) {
            DialogUtils.showError("Format data yang diinputkan salah.\n"
                    + "Gunakan titik ( . ) sebagai pengganti koma ( , ) untuk data bilangan desimal.");
        } catch (Exception ex) {
            DialogUtils.showError("Gagal memproses data persalinan, silahkan coba lagi.");
        }
        root.setDisable(false);
    }
    @FXML private void deletePersalinan() {
        if (selectedPersalinan == null) DialogUtils.showInfo("Silahkan pilih salah satu data Persalinan terlebih dahulu di tabel.");
        else if (DialogUtils.showConfirm("Yakin akan menghapus data persalinan dari "
                +selectedPersalinan.getNamaPasien()+" \npada tanggal "
                +selectedPersalinan.getTanggalPartus()+" ?") == DialogUtils.RESULT_OK
        ) {
            root.setDisable(true);
            String nama = selectedPersalinan.getNamaPasien(),
                    tanggal = selectedPersalinan.getTanggalPartus();
            if (persalinanManager.deletePersalinan(selectedPersalinan) == AppConfig.SUCCESS_CODE) {
                DialogUtils.showInfo("Berhasil menghapus data persalinan dari "
                        +nama+" \npada tanggal "+tanggal);
                if (tablePersalinan.getItems().isEmpty()) {
                    selectedPersalinan = null;
                    fetchSelectedPersalinan();
                }
                disableForms(true);
                buttonSimpan.setDisable(true);
                buttonBatal.setDisable(false);
                buttonTambah.setDisable(false);
            } else {
                DialogUtils.showError("Gagal menghapus data persalinan dari "
                        +nama+" \npada tanggal "+tanggal
                        +", silahkan coba lagi.");
            }
            root.setDisable(false);
        }
    }
    @FXML private void cancelForms() {
        if (datePickerTanggal.isDisabled()) {  
            tablePersalinan.getSelectionModel().clearSelection();
            fetchSelectedPasien(null);
            selectedPersalinan = null;
            fetchSelectedPersalinan();
            disableForms(true);
        } else {            
            if (DialogUtils.showConfirm("Batalkan semua perubahan?")
                    == DialogUtils.RESULT_OK) {
                buttonSimpan.setDisable(true);
                buttonBatal.setDisable(false);
                buttonTambah.setDisable(false);
                fetchSelectedPersalinan();
                disableForms(true);
            }
        }
    }
    
    @FXML private void searchPersalinan() {
        tablePersalinan.setItems(persalinanManager.filterPersalinanList(formSearchTable.getText()));
    }
    @FXML private void refreshTable() {
        formSearchTable.clear();
        tablePersalinan.setItems(persalinanManager.getPersalinanList(true));    
    }
    
    private void handleSpin(Spinner spinner, Number oldValue, Object newValue) {
        try {
            if (newValue == null) {
                spinner.getValueFactory().setValue((int)oldValue);
            } else {
                spinner.getValueFactory().setValue((int)newValue);
            }
        } catch (Exception e) {
            System.out.println("spinner exception");
            spinner.getValueFactory().setValue(0);
        }
    }
}
