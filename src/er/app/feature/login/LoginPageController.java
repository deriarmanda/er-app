/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.login;

import er.app.config.AppConfig;
import er.app.data.manager.UserDataManager;
import er.app.data.model.User;
import er.app.feature.home.ERApp;
import er.app.util.DialogUtils;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Deri Armanda
 */
public class LoginPageController implements Initializable {

    @FXML private TextField fieldUsername;
    @FXML private PasswordField fieldPassword;
    
    private ERApp mainApp;
    private UserDataManager userManager;
    private User user;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserDataManager.getInstance();
        user = new User();
    }    

    public void setMainApp(ERApp mainApp) {
        this.mainApp = mainApp;
    }
    
    @FXML private void doLogin() {
        user.setUsername(fieldUsername.getText());
        user.setPassword(fieldPassword.getText());
        int result = userManager.signingInUser(user);
        if (result == AppConfig.SUCCESS_CODE) {            
            if (mainApp != null) {
                DialogUtils.showInfo("Login berhasil. Selamat datang kembali, "+user.getUsername());
                onAuthenticated();
            }
            else {
                System.err.println("Error opening HomePage, mainApp is null.");
                DialogUtils.showError("Terjadi Kesalahan ketika membuka halaman Home.");
            }
        } else {
            DialogUtils.showError("Gagal melakukan Login, silahkan periksa username dan password anda.");
        }
    }
    
    @FXML private void doRegister() {
        user.setUsername(fieldUsername.getText());
        user.setPassword(fieldPassword.getText());
        int result = userManager.registerUser(user);
        if (result == AppConfig.SUCCESS_CODE) {            
            if (mainApp != null) {
                DialogUtils.showInfo("Berhasil mendaftarkan akun. Selamat datang di ER App, "+user.getUsername());
                onAuthenticated();
            }
            else {
                System.err.println("Error opening HomePage, mainApp is null.");
                DialogUtils.showError("Terjadi Kesalahan ketika membuka halaman Home.");
            }
        } else {
            DialogUtils.showError("Gagal melakukan proses Register, silahkan coba lagi.");
        }
    }
    
    private void onAuthenticated() {
        fieldUsername.clear();
        fieldPassword.clear();
        mainApp.showPage(ERApp.HOME_PAGE);
    }
}
