/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.feature.pasien;

import er.app.config.AppConfig;
import er.app.data.manager.PasienDataManager;
import er.app.data.model.Pasien;
import er.app.util.DateTimeUtils;
import er.app.util.DialogUtils;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class PasienPageController implements Initializable {
    
    @FXML private VBox root;
    
    // Formulir Views
    @FXML private TitledPane containerForms;
    @FXML private TextField formNoRm;
    @FXML private TextField formNamaPasien;
    @FXML private ToggleGroup groupJenisKelamin;
    @FXML private RadioButton radioLaki;
    @FXML private RadioButton radioPerempuan;
    @FXML private TextField formTempatLahir;
    @FXML private DatePicker datePickerTanggalLahir;
    @FXML private TextField spinnerUsia;
    @FXML private ComboBox typeUsia;
    @FXML private TextField formNamaKK;
    @FXML private TextField formNIK;
    @FXML private TextField formNomorTelepon;
    @FXML private TextArea formAlamat;
    @FXML private ComboBox comboKelurahan;
    @FXML private TextField formKelurahanLainnya;
    @FXML private ComboBox comboPembayaran;
    @FXML private TextField formNoBpjs;
    @FXML private Button buttonSimpan;
    @FXML private Button buttonBatal;
    @FXML private Button buttonTambah;
    @FXML private Button buttonUpdate;
    @FXML private Button buttonDelete;
    
    // Table Views
    @FXML private TableView<Pasien> tablePasien;
    @FXML private TableColumn<Pasien, String> colNoRm;
    @FXML private TableColumn<Pasien, String> colNama;
    @FXML private TableColumn<Pasien, String> colJenisKelamin;
    @FXML private TableColumn<Pasien, String> colTtl;
    @FXML private TableColumn<Pasien, String> colUsia;
    @FXML private TableColumn<Pasien, String> colAlamat;
    @FXML private TableColumn<Pasien, String> colKelurahan;
    @FXML private TableColumn<Pasien, String> colPembayaran;
    @FXML private TextField formSearch;
    @FXML private Button buttonSearch;
    @FXML private Button buttonRefresh;
    
    private PasienDataManager pasienManager;
    private Pasien selectedPasien;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO        
        pasienManager = PasienDataManager.getInstance();
        selectedPasien = null;
        
        initForms();
        initTables();
    }     
    
    public void onPageShown() {
        System.out.println("Pasien Controller shown");
    }
    
    private void initForms() {
        // DatePicker tanggal lahir
        datePickerTanggalLahir.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        
        // Form usia
        spinnerUsia.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                spinnerUsia.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        typeUsia.getItems().add("Tahun");
        typeUsia.getItems().add("Bulan");
        typeUsia.getItems().add("Hari");
        typeUsia.getSelectionModel().selectFirst();
        
        // Form kelurahan
        ObservableList<String> listKelurahan = 
                FXCollections.observableArrayList(
                        "Bunulrejo", 
                        "Kesatrian", 
                        "Jodipan", 
                        "Polehan", 
                        "Lainnya"
                );
        comboKelurahan.setItems(listKelurahan);
        comboKelurahan.getSelectionModel().selectFirst();
        
        // Form pembayaran
        comboPembayaran.getItems().add("BPJS");
        comboPembayaran.getItems().add("Non BPJS");
        comboPembayaran.getSelectionModel().selectFirst();
        comboPembayaran.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (!buttonSimpan.isDisabled()) 
                formNoBpjs.setDisable(comboPembayaran.getSelectionModel().isSelected(1));
        });
        
        // Form kelurahan
        comboKelurahan.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (!buttonSimpan.isDisabled()) 
                formKelurahanLainnya.setDisable(!comboKelurahan.getSelectionModel().isSelected(4));
        });
    }  
    private void initTables() {
        colNoRm.setCellValueFactory(value -> value.getValue().nomorRMProperty());
        colNama.setCellValueFactory(value -> value.getValue().namaLengkapProperty());
        colJenisKelamin.setCellValueFactory(value -> value.getValue().jenisKelaminProperty());
        colTtl.setCellValueFactory(value -> value.getValue().tempatTanggalLahirProperty());
        colUsia.setCellValueFactory(value -> value.getValue().usiaProperty());
        colAlamat.setCellValueFactory(value -> value.getValue().alamatProperty());
        colKelurahan.setCellValueFactory(value -> value.getValue().kelurahanProperty());
        colPembayaran.setCellValueFactory(value -> value.getValue().pembayaranProperty());
        
        tablePasien.setItems(pasienManager.getPasienList());
        tablePasien.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Pasien> observable, 
                        Pasien oldValue, 
                        Pasien newValue) -> {
            selectedPasien = newValue;
            fetchSelectedPasien();
            containerForms.setExpanded(true);
        });
    }

    private void fetchSelectedPasien() {
        if (!buttonSimpan.isDisabled()) return;
        
        if (selectedPasien == null) {
            formNoRm.setText("");
            formNamaPasien.clear();
            groupJenisKelamin.selectToggle(radioLaki);
            formTempatLahir.clear();
            datePickerTanggalLahir.setValue(null);
            formKelurahanLainnya.clear();
            formNamaKK.clear();
            formNIK.clear();
            formNomorTelepon.clear();
            formAlamat.clear();
            formNoBpjs.clear();
        } else {
            formNoRm.setText(selectedPasien.getNomorRM());
            formNamaPasien.setText(selectedPasien.getNamaLengkap());
            groupJenisKelamin.selectToggle(
                    selectedPasien.getJenisKelamin().equals("Laki - laki")?
                            radioLaki : radioPerempuan
            );
            String temp[] = selectedPasien.getTempatTanggalLahir().split(", ");
            if (temp.length > 1) {
                formTempatLahir.setText(temp[0]);
                datePickerTanggalLahir.setValue(DateTimeUtils.parseDateOnly(temp[1]));
            }
            temp = selectedPasien.getUsia().split(" ");
            if (temp.length > 1) {
                spinnerUsia.setText(temp[0]);
                typeUsia.getSelectionModel().select(temp[1]);
            }
            formNamaKK.setText(selectedPasien.getNamaKK());
            formNIK.setText(selectedPasien.getNIK());
            formNomorTelepon.setText(selectedPasien.getNomorTelepon());
            formAlamat.setText(selectedPasien.getAlamat());
            switch (selectedPasien.getKelurahan()) {
                case "Bunulrejo":
                    comboKelurahan.getSelectionModel().select(0);
                    formKelurahanLainnya.clear();
                    break;
                case "Kesatrian":
                    comboKelurahan.getSelectionModel().select(1);
                    formKelurahanLainnya.clear();
                    break;
                case "Jodipan":
                    comboKelurahan.getSelectionModel().select(2);
                    formKelurahanLainnya.clear();
                    break;
                case "Polehan":
                    comboKelurahan.getSelectionModel().select(3);
                    formKelurahanLainnya.clear();
                    break;
                default:
                    comboKelurahan.getSelectionModel().select(4);
                    formKelurahanLainnya.setText(selectedPasien.getKelurahan());
                    break;
            }
            comboPembayaran.getSelectionModel().select(selectedPasien.getPembayaran());
            formNoBpjs.setText(selectedPasien.getNomorBpjs());
        }
        buttonTambah.setDisable(false);
        buttonUpdate.setDisable(selectedPasien == null);
        buttonDelete.setDisable(selectedPasien == null);
    }
    private void disableForms(boolean isDisabled) {
        formNoRm.setDisable(isDisabled);
        formNamaPasien.setDisable(isDisabled);
        radioLaki.setDisable(isDisabled);
        radioPerempuan.setDisable(isDisabled);
        formTempatLahir.setDisable(isDisabled);
        datePickerTanggalLahir.setDisable(isDisabled);
        spinnerUsia.setDisable(isDisabled);
        typeUsia.setDisable(isDisabled);
        formNamaKK.setDisable(isDisabled);
        formNIK.setDisable(isDisabled);
        formNomorTelepon.setDisable(isDisabled);
        formAlamat.setDisable(isDisabled);
        comboKelurahan.setDisable(isDisabled);
        comboPembayaran.setDisable(isDisabled);
        
        if (!isDisabled) {
            formKelurahanLainnya.setDisable(!comboKelurahan.getSelectionModel().isSelected(4));
            formNoBpjs.setDisable(comboPembayaran.getSelectionModel().isSelected(1));
            formNoRm.requestFocus();
        } else {
            formKelurahanLainnya.setDisable(true);
            formNoBpjs.setDisable(true);            
        }
    }
    
    @FXML private void savePasien() {
        root.setDisable(true);
        try {
            if (selectedPasien == null) {
                // insert new pasien
                if (formNoRm.getText().trim().isEmpty() || 
                        formNamaPasien.getText().trim().isEmpty()) {
                    DialogUtils.showError("Data nomor RM dan nama lengkap pasien harus diisi.");
                    root.setDisable(false);
                    return;
                }

                // Collect data pasien
                Pasien pasien = new Pasien();
                pasien.setNomorRM(formNoRm.getText().trim());
                pasien.setNamaLengkap(formNamaPasien.getText().trim());
                pasien.setJenisKelamin(
                        groupJenisKelamin.selectedToggleProperty()
                                .get() == radioLaki? "Laki - laki" : "Perempuan"
                );
                pasien.setTempatTanggalLahir(
                        formTempatLahir.getText().trim() + ", " +
                        datePickerTanggalLahir.getValue()
                );
                System.out.println(datePickerTanggalLahir.getValue());
                pasien.setUsia(
                        spinnerUsia.getText().trim() + " " +
                        typeUsia.getValue().toString()
                );
                pasien.setNamaKK(formNamaKK.getText().trim());
                pasien.setNIK(formNIK.getText().trim());
                pasien.setNomorTelepon(formNomorTelepon.getText().trim());
                pasien.setAlamat(formAlamat.getText().trim());
                if (comboKelurahan.getSelectionModel().isSelected(4)) pasien.setKelurahan(formKelurahanLainnya.getText().trim());
                else pasien.setKelurahan(comboKelurahan.getValue().toString());
                pasien.setPembayaran(comboPembayaran.getValue().toString());
                pasien.setNomorBpjs(formNoBpjs.getText().trim());

                // insert and check result
                if (pasienManager.addPasien(pasien) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil menambahkan data pasien baru.");
                    selectedPasien = pasien;
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(true);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal menambahkan data pasien baru, silahkan coba lagi.");
                }
            } else {
                // update selected pasien
                if (formNoRm.getText().trim().isEmpty() || 
                        formNamaPasien.getText().trim().isEmpty()) {
                    DialogUtils.showError("Data nomor RM dan nama lengkap pasien harus diisi.");
                    return;
                }

                // Collect data pasien
                selectedPasien.setNomorRM(formNoRm.getText().trim());
                selectedPasien.setNamaLengkap(formNamaPasien.getText().trim());
                selectedPasien.setJenisKelamin(
                        groupJenisKelamin.selectedToggleProperty()
                                .get() == radioLaki? "Laki - laki" : "Perempuan"
                );
                selectedPasien.setTempatTanggalLahir(
                        formTempatLahir.getText().trim() + ", " +
                        datePickerTanggalLahir.getValue()
                );
                selectedPasien.setUsia(
                        spinnerUsia.getText().trim() + " " +
                        typeUsia.getValue().toString()
                );
                selectedPasien.setNamaKK(formNamaKK.getText().trim());
                selectedPasien.setNIK(formNIK.getText().trim());
                selectedPasien.setNomorTelepon(formNomorTelepon.getText().trim());
                selectedPasien.setAlamat(formAlamat.getText().trim());
                if (comboKelurahan.getSelectionModel().isSelected(4)) selectedPasien.setKelurahan(formKelurahanLainnya.getText().trim());
                else selectedPasien.setKelurahan(comboKelurahan.getValue().toString());
                selectedPasien.setPembayaran(comboPembayaran.getValue().toString());
                selectedPasien.setNomorBpjs(formNoBpjs.getText().trim());

                // update and check result
                if (pasienManager.updatePasien(selectedPasien) == AppConfig.SUCCESS_CODE) {
                    DialogUtils.showInfo("Berhasil merubah data pasien lama.");
                    disableForms(true);
                    buttonSimpan.setDisable(true);
                    buttonBatal.setDisable(true);
                    buttonTambah.setDisable(false);
                    buttonUpdate.setDisable(false);
                    buttonDelete.setDisable(false);
                } else {
                    DialogUtils.showError("Gagal merubah data pasien lama, silahkan coba lagi.");
                }
            }
        } catch (Exception ex) {
            DialogUtils.showError("Gagal menambahkan data maternal baru, silahkan periksa format data apakah sudah benar.");
        }
        root.setDisable(false);
    }
    @FXML private void deletePasien() {
        if (selectedPasien == null) DialogUtils.showInfo("Silahkan pilih data salah satu pasien terlebih dahulu di tabel.");
        else if (DialogUtils.showConfirm("Yakin akan menghapus pasien "
                +selectedPasien.getNamaLengkap() +" ?") == DialogUtils.RESULT_OK
//                +"? Semua data termasuk data rekam medis pasien akan "
//                + "ikut terhapus.") == DialogUtils.RESULT_OK
        ) {
            root.setDisable(true);
            String nama = selectedPasien.getNamaLengkap();
            if (pasienManager.deletePasien(selectedPasien) == AppConfig.SUCCESS_CODE) {
                DialogUtils.showInfo("Berhasil menghapus data pasien "
                        +nama);
                selectedPasien = null;
                fetchSelectedPasien();
                disableForms(true);
                buttonSimpan.setDisable(true);
                buttonBatal.setDisable(true);
                buttonTambah.setDisable(false);
                buttonUpdate.setDisable(selectedPasien == null);
                buttonDelete.setDisable(selectedPasien == null);
            } else {
                DialogUtils.showError("Gagal menghapus data pasien "
                        +nama
                        +", silahkan coba lagi.");
            }
            root.setDisable(false);
        }
    }
    @FXML private void cancelForms() {
        if (DialogUtils.showConfirm("Batalkan semua perubahan?")
                == DialogUtils.RESULT_OK) {
            fetchSelectedPasien();
            disableForms(true);
            buttonSimpan.setDisable(true);
            buttonBatal.setDisable(true);
            buttonTambah.setDisable(false);
            buttonUpdate.setDisable(selectedPasien == null);
            buttonDelete.setDisable(selectedPasien == null);
        }
    }
    @FXML private void onButtonTambahClicked() {
        selectedPasien = null;
        fetchSelectedPasien();
        disableForms(false);
        buttonSimpan.setDisable(false);
        buttonBatal.setDisable(false);
        buttonTambah.setDisable(true);
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);
    }
    @FXML private void onButtonUpdateClicked() {
        disableForms(false);
        buttonSimpan.setDisable(false);
        buttonBatal.setDisable(false);
        buttonTambah.setDisable(true);
        buttonUpdate.setDisable(true);
        buttonDelete.setDisable(true);
    }
    @FXML private void searchPasien() {
        String query = formSearch.getText();
        tablePasien.getItems().stream()
            .filter(item -> item.getNomorRM()
                    .toLowerCase().contains(query.toLowerCase()) || 
                    item.getNamaLengkap().contains(query))
            .findAny()
            .ifPresent(item -> {
                tablePasien.getSelectionModel().select(item);
                tablePasien.scrollTo(item);
            });
    }
    @FXML private void refreshTable() {
        tablePasien.setItems(pasienManager.getPasienList(true));
    }
    
    private void handleSpin(Spinner spinner, Number oldValue, Object newValue) {
        try {
            if (newValue == null) {
                spinner.getValueFactory().setValue((int)oldValue);
            } else {
                spinner.getValueFactory().setValue((int)newValue);
            }
        } catch (Exception e) {
            System.out.println("spinner exception");
            spinner.getValueFactory().setValue(0);
        }
    }
}
