/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.config;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import er.app.util.DialogUtils;
import java.io.File;
import java.sql.SQLException;

/**
 *
 * @author DERI
 */
public class DatabaseConfig {
    
    private static final String TAG_NAME = DatabaseConfig.class.getSimpleName();
    private static final String DB_NAME = "er_app.db";
    private static final String DB_PATH = 
            System.getProperty("user.dir")+File.separator+DB_NAME;
    private static final String JDBC_CONFIG = "jdbc:sqlite:" + DB_PATH;
    
    private static ConnectionSource conn;
    
    public static ConnectionSource getConnection() {
        if (conn == null) try {
            conn = new JdbcConnectionSource(JDBC_CONFIG);
        } catch (SQLException ex) {
            conn = null;
            System.err.println("Error while creating db source: "+ex);
            DialogUtils.showError("Gagal menyambungkan ke database.");
        }
        return conn;
    }
}
