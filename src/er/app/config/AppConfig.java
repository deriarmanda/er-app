/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package er.app.config;

import er.app.data.manager.MaternalDataManager;
import er.app.data.manager.PasienDataManager;
import er.app.data.manager.PersalinanDataManager;
import er.app.data.manager.SdidtkDataManager;
import er.app.data.manager.UserDataManager;
import er.app.util.DialogUtils;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author DERI
 */
public class AppConfig {
    
    public static String appName;
    public static String appAuthor;
    public static Image appLogo;
    
    public static int SUCCESS_CODE = 110;
    public static int ERROR_CODE = 111;
    
    public static void init(Stage stage) {
        appName = "ER - App";
        appLogo = new Image("/er/app/assets/image/logo_app_512p.png");
        appAuthor = "Waktu Rilis : \n"
                + "Desember 2018 \n\n"
                + "Dikembangkan Oleh : \n"
                + "Tita Septiane Anggarsari \n"
                + "D-III Perekam Medis dan Informasi Kesehatan 2016 \n"
                + "Poltekkes Kemenkes Malang";
        
        DialogUtils.init(stage);
        
        DialogUtils.showSplash();
        UserDataManager.init();
        PasienDataManager.init();
        MaternalDataManager.init();
        PersalinanDataManager.init();
        SdidtkDataManager.init();
        DialogUtils.closeSplash();
    }
}
